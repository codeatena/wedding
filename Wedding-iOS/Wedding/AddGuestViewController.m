//
//  AddGuestViewController.m
//  Wedding
//
//  Created by apple on 09/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import "AddGuestViewController.h"

@interface AddGuestViewController ()

@end

@implementation AddGuestViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    userDefault                 =   [NSUserDefaults standardUserDefaults];
    
    pickerViewArrayObj          =   [[NSMutableArray alloc ]init];
    familyArrObj                =   [[NSMutableArray alloc]initWithObjects:@"Family",@"Friend", nil];
    guestTypeArrObj             =   [[NSMutableArray alloc]initWithObjects:@"Day Guest",@"Evening Guest",@"Both", nil];
    
    UIImageView *bgImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    bgImageView.image           =   [UIImage imageNamed:ADD_GIFT_BG_IMG];
    [self.view addSubview:bgImageView];
    
    UIImageView *topImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT*0.08)];
    topImageView.image           =   [UIImage imageNamed:TOP_IMG];
    [self.view addSubview:topImageView];
    
    UIImageView *titleImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.08, SCREEN_WIDTH, SCREEN_HEIGHT*0.065)];
    [titleImageView setBackgroundColor:[UIColor colorWithRed:197.0f/255.0f green:105.0f/255.0f blue:130.0f/255.0f alpha:1.0f]];
    titleImageView.alpha            =   0.7;
    [self.view addSubview:titleImageView];
    
    UILabel *titleLbl           =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.10, SCREEN_HEIGHT*0.08, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.06)];
    titleLbl.backgroundColor    =   [UIColor clearColor];
    titleLbl.text               =   @"Add Guest";
    titleLbl.textColor          =   [UIColor whiteColor];
    titleLbl.textAlignment      =   NSTextAlignmentLeft;
    titleLbl.font               =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.06];
    [self.view addSubview:titleLbl];
    
    UIButton *backBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.03, SCREEN_HEIGHT*0.09,SCREEN_WIDTH*0.05,SCREEN_HEIGHT*0.04)];
    [backBtn setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMG] forState:UIControlStateNormal];
    [backBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    
    UIButton *logoutBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.80, 0,SCREEN_WIDTH*0.20,SCREEN_HEIGHT*0.08)];
    [logoutBtn setBackgroundColor:[UIColor clearColor]];
    [logoutBtn setTitle:@"Logout" forState:UIControlStateNormal];
    [logoutBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    logoutBtn.titleLabel.font     =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.05];
    [logoutBtn addTarget:self action:@selector(logoutButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:logoutBtn];
    
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    toolBar.barStyle = UIBarStyleBlackOpaque;
    UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneBtnClicked)];
    
    [toolBar setItems:[NSArray arrayWithObjects:flexibleSpaceLeft,btn, nil]];
    
    contentScroll                   =   [[UIScrollView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.16,SCREEN_WIDTH , SCREEN_HEIGHT*0.82)];
    contentScroll.backgroundColor   =   [UIColor clearColor];
    [self.view addSubview:contentScroll];
    
    profileImgV    =   [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.05, SCREEN_HEIGHT*0.02, SCREEN_WIDTH*0.25, SCREEN_WIDTH*0.25)];
    profileImgV.image           =   [UIImage imageNamed:DEFAULT_GUEST_IMG];
    [contentScroll addSubview:profileImgV];
    
    UIButton *addImgBtn =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.05, SCREEN_HEIGHT*0.02, SCREEN_WIDTH*0.25, SCREEN_WIDTH*0.25)];
    [addImgBtn setBackgroundColor:[UIColor clearColor]];
    addImgBtn.alpha     =   0.6;
    [addImgBtn addTarget:self action:@selector(openGalary) forControlEvents:UIControlEventTouchUpInside];
    [contentScroll addSubview:addImgBtn];
    
    favBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.60, SCREEN_HEIGHT*0.02,SCREEN_WIDTH*0.12,SCREEN_WIDTH*0.12)];
    [favBtn setBackgroundImage:[UIImage imageNamed:STAR_WHITE_IMG] forState:UIControlStateNormal];
    [favBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [favBtn addTarget:self action:@selector(favBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [contentScroll addSubview:favBtn];
    
    UILabel *favLbl           =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.50, SCREEN_HEIGHT*0.10, SCREEN_WIDTH*0.40, SCREEN_HEIGHT*0.06)];
    favLbl.backgroundColor    =   [UIColor clearColor];
    favLbl.text               =   @"Add to Favourite";
    favLbl.textColor          =   [UIColor whiteColor];
    favLbl.textAlignment      =   NSTextAlignmentLeft;
    favLbl.font               =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.04];
    [contentScroll addSubview:favLbl];
    
    
    float leftMargin    =   SCREEN_WIDTH*0.10;
    float topMargin     =   SCREEN_HEIGHT*0.20;
    NSArray *placeholderArr =   [[NSArray alloc]initWithObjects:@"First Name",@"Last Name",@"Family",@"Day Guest",@"Contact No",@"Email",@"Address", nil];
    for (int i=0; i<7; i++) {
        
        UITextField *txtField       =   [[UITextField alloc]initWithFrame:CGRectMake(leftMargin, topMargin, SCREEN_WIDTH*0.80, SCREEN_HEIGHT*0.06)];
        txtField.placeholder        =   [placeholderArr objectAtIndex:i];
        txtField.delegate           =   self;
        txtField.backgroundColor    =   [UIColor blackColor];
        txtField.layer.cornerRadius =   22;
        txtField.tag                =   i+1;
        txtField.font               =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.05];
        txtField.alpha              =   0.6;
        txtField.textColor          =   [UIColor whiteColor];
        // For adding padding in text field
        UIView *paddingView1        =   [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH*0.04, 20)];
        txtField.leftView           =   paddingView1;
        txtField.leftViewMode       =   UITextFieldViewModeAlways;
        [txtField setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
        [contentScroll addSubview:txtField];
        
        
        if (i == 2 || i == 3) {
            UIImageView *dropDownImg    =   [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.80, topMargin+SCREEN_HEIGHT*0.02, SCREEN_WIDTH*0.05, SCREEN_WIDTH*0.04)];
            dropDownImg.image           =   [UIImage imageNamed:DROP_DOWN_PNG];
            [contentScroll addSubview:dropDownImg];
        }
        
        topMargin += SCREEN_HEIGHT*0.07;
    }
    
    pickerViewObj = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 300, 200)];
    pickerViewObj.showsSelectionIndicator = YES;
    pickerViewObj.hidden = YES;
    pickerViewObj.delegate = self;
    pickerViewObj.dataSource = self;
    [self.view addSubview:pickerViewObj];
    
    UIButton *addBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.60, topMargin, SCREEN_WIDTH*0.30, SCREEN_HEIGHT*0.08)];
    [addBtn setBackgroundImage:[UIImage imageNamed:BUTTON_IMAGE] forState:UIControlStateNormal];
    [addBtn setTitle:@"Add Guest" forState:UIControlStateNormal];
    [addBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    addBtn.titleLabel.font   =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.055];
    [addBtn addTarget:self action:@selector(addGuest) forControlEvents:UIControlEventTouchUpInside];
    [contentScroll addSubview:addBtn];
    
    [contentScroll setContentSize:CGSizeMake(SCREEN_WIDTH, topMargin)];
}

#pragma mark Back Button Method
-(void)backButtonClicked{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark Method to be called when tap on logout button
-(void)logoutButtonClicked{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

#pragma mark Method to be called when tap on Done button of tool bar
-(void)doneBtnClicked{
    [self.view endEditing:YES];
}

#pragma mark Method to be called when click on favourite button
-(void)favBtnClicked{
    if (isFav == 1) {
        [favBtn setBackgroundImage:[UIImage imageNamed:STAR_WHITE_IMG] forState:UIControlStateNormal];
        isFav   =   0;
    }else{
        [favBtn setBackgroundImage:[UIImage imageNamed:STAR_GOLDEN_IMG] forState:UIControlStateNormal];
        isFav   =   1;
    }
}

#pragma mark method to open gallary
-(void)openGalary{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *picker =   [[UIImagePickerController alloc]init];
        picker.delegate                 =   self;
        picker.allowsEditing            =   NO;
        picker.sourceType               =   UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:nil];
    }else{
        NSLog(@"ERROR");
    }
}

#pragma mark Method to add guest
-(void)addGuest{
    
    if([common reachabilityFunction]) {
        
        UITextField *temp1   =   (UITextField *)[self.view viewWithTag:1];
        UITextField *temp2   =   (UITextField *)[self.view viewWithTag:2];
        UITextField *temp3   =   (UITextField *)[self.view viewWithTag:3];
        UITextField *temp4   =   (UITextField *)[self.view viewWithTag:4];
        UITextField *temp5   =   (UITextField *)[self.view viewWithTag:5];
        UITextField *temp6   =   (UITextField *)[self.view viewWithTag:6];
        UITextField *temp7   =   (UITextField *)[self.view viewWithTag:7];
        
        NSString    *fav    =   @"";
        if (isFav) {
            fav =       @"favourite";
        }
        
        NSString *urlStr = [NSString stringWithFormat:@"%@guest_user.php?firstname=%@&lastname=%@&relation=%@&contact_no=%@&email=%@&address=%@&profile_pic=%@&favouite=%@&guest_option=%@&user_id=%d",API_URL,temp1.text,temp2.text,temp3.text,temp5.text,temp6.text,temp7.text,@"xyz.png",fav,temp4.text,[[userDefault valueForKey:@"userId"] integerValue]];
        
        urlStr  =   [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        responseData   =   [[NSMutableData alloc]init];
        
        request        =   [NSURLRequest requestWithURL:[NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] cachePolicy:NSURLCacheStorageAllowed timeoutInterval:5];
        
        connect        =   [[NSURLConnection alloc] initWithRequest:request delegate:self];
        
        [connect start];
    }else{
        [common showAlert:@"Warning" _message:@"No internet connection."];
    }
}

#pragma mark Url connection delegate methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    //    NSLog(@"didReceiveResponse");
    
    if (connection==connect){
        // Login
        [responseData setLength:0];
    }
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    //    NSLog(@"didFailWithError");
    
    if(responseData==nil){
        [common showAlert:@"Error" _message:@"Server Problem"];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    if(connection==connect){
        [responseData appendData:data];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    if (connection==connect){
        NSError* error;
        
        NSMutableDictionary *reviewsDict = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        
        //        NSLog(@"DICT :: %@",reviewsDict);
        if ([[reviewsDict valueForKey:@"success"] integerValue] == 1){
            [common showAlert:@"Info" _message:@"Guest Added"];
        }else{
            [common showAlert:@"Warning" _message:@"Oops! An error occurred."];
        }
        
    }
}


#pragma mark UIImagePicker delegate methods
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo
{
    
    UIImage *uploadedImg    =   [UIImage imageWithCGImage:image.CGImage scale:0.1 orientation:image.imageOrientation];
    profileImgV.image       =   uploadedImg;
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark TextField delegate methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField.tag == 3 ) {
        textField.inputAccessoryView    =   toolBar;
        textField.inputView     =   pickerViewObj;
        pickerViewArrayObj      =   familyArrObj;
        pickerViewObj.hidden    =   NO;
        [pickerViewObj reloadAllComponents];
    }else if (textField.tag == 4){
        textField.inputView     =   pickerViewObj;
        textField.inputAccessoryView    =   toolBar;
        pickerViewArrayObj      =   guestTypeArrObj;
        pickerViewObj.hidden    =   NO;
        [pickerViewObj reloadAllComponents];
    }
    
    
    CGRect Frame    =   contentScroll.frame;
    if (textField.tag >= 4 ) {
        Frame   = CGRectMake(contentScroll.frame.origin.x, -SCREEN_HEIGHT*0.15, contentScroll.frame.size.width, contentScroll.frame.size.height);
    }else{
        Frame   =  CGRectMake(0, SCREEN_HEIGHT*0.16,SCREEN_WIDTH , SCREEN_HEIGHT*0.82);
    }
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    
    contentScroll.frame =   Frame;
    
    [UIView commitAnimations];
    
}

#pragma mark Picker view delegate method
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return pickerViewArrayObj.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [pickerViewArrayObj objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    UITextField *temp   =   (UITextField *)[self.view viewWithTag:3];
    if (temp.isEditing == true) {
        temp.text   =   [pickerViewArrayObj objectAtIndex:row];
    }else{
        temp        =   (UITextField *)[self.view viewWithTag:4];
        temp.text   =   [pickerViewArrayObj objectAtIndex:row];
    }
}

#pragma mark Method to be called when keyboard hide
-(void)keyboardDidHide:(NSNotification *)notification{
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    
    contentScroll.frame    =   CGRectMake(0, SCREEN_HEIGHT*0.16,SCREEN_WIDTH , SCREEN_HEIGHT*0.82);
    
    [UIView commitAnimations];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
