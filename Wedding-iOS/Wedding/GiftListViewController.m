//
//  GiftListViewController.m
//  Wedding
//
//  Created by apple on 09/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import "GiftListViewController.h"
#import "AddGiftViewController.h"

@interface GiftListViewController ()

@end

@implementation GiftListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    userDefaults                =   [NSUserDefaults standardUserDefaults];
    
    UIImageView *bgImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    bgImageView.image           =   [UIImage imageNamed:GIFT_LIST_BG_IMG];
    [self.view addSubview:bgImageView];
    
    UIImageView *topImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT*0.08)];
    topImageView.image           =   [UIImage imageNamed:TOP_IMG];
    [self.view addSubview:topImageView];
    
    UIImageView *titleImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.08, SCREEN_WIDTH, SCREEN_HEIGHT*0.065)];
    [titleImageView setBackgroundColor:[UIColor colorWithRed:197.0f/255.0f green:105.0f/255.0f blue:130.0f/255.0f alpha:1.0f]];
    titleImageView.alpha            =   0.7;
    [self.view addSubview:titleImageView];
    
    UILabel *titleLbl           =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.10, SCREEN_HEIGHT*0.08, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.06)];
    titleLbl.backgroundColor    =   [UIColor clearColor];
    titleLbl.text               =   @"Gift List";
    titleLbl.textColor          =   [UIColor whiteColor];
    titleLbl.textAlignment      =   NSTextAlignmentLeft;
    titleLbl.font               =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.06];
    [self.view addSubview:titleLbl];
    
    UIButton *backBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.03, SCREEN_HEIGHT*0.09,SCREEN_WIDTH*0.05,SCREEN_HEIGHT*0.04)];
    [backBtn setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMG] forState:UIControlStateNormal];
    [backBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    
    UIButton *logoutBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.80, 0,SCREEN_WIDTH*0.20,SCREEN_HEIGHT*0.08)];
    [logoutBtn setBackgroundColor:[UIColor clearColor]];
    [logoutBtn setTitle:@"Logout" forState:UIControlStateNormal];
    [logoutBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    logoutBtn.titleLabel.font     =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.05];
    [logoutBtn addTarget:self action:@selector(logoutButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:logoutBtn];
    
    UIButton *addGiftBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.75, SCREEN_HEIGHT*0.08,SCREEN_WIDTH*0.25,SCREEN_HEIGHT*0.065)];
    [addGiftBtn setBackgroundColor:[UIColor clearColor]];
    [addGiftBtn setBackgroundImage:[UIImage imageNamed:ADD_BUTTON_IMAGE] forState:UIControlStateNormal];
    [addGiftBtn setTitle:@"Add Gift" forState:UIControlStateNormal];
    addGiftBtn.titleLabel.font     =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.05];
    [addGiftBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [addGiftBtn addTarget:self action:@selector(doAddGift) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:addGiftBtn];
    
    UIImageView *blackBG        =   [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.03, SCREEN_HEIGHT*0.16, SCREEN_WIDTH*0.94, SCREEN_HEIGHT*0.82)];
    [blackBG setBackgroundColor:[UIColor blackColor]];
    blackBG.alpha               =   0.6;
    [self.view addSubview:blackBG];
    
    tableV                  =   [[UITableView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.03, SCREEN_HEIGHT*0.16, SCREEN_WIDTH*0.94, SCREEN_HEIGHT*0.82)];
    tableV.dataSource       =   self;
    tableV.delegate         =   self;
    tableV.backgroundColor  =   [UIColor clearColor];
    //    tableV.separatorStyle   =   UITableViewCellSeparatorStyleNone;
    tableV.showsVerticalScrollIndicator =   NO;
    [self.view addSubview:tableV];
    
    [self loadGiftList];
}

#pragma mark Back Button Method
-(void)backButtonClicked{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark Method to be called when tap on logout button
-(void)logoutButtonClicked{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

#pragma mark Method to add gift
-(void)doAddGift{
    AddGiftViewController *giftAddVC  =   [[AddGiftViewController alloc]init];
    [self.navigationController pushViewController:giftAddVC animated:NO];
}

#pragma mark Method to load Gift list
-(void)loadGiftList{
    if([common reachabilityFunction]) {
        NSString *urlStr = [NSString stringWithFormat:@"%@get_gift.php?user_id=%d",API_URL,[[userDefaults valueForKey:@"userId"] integerValue]];
        
        urlStr  =   [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        responseData   =   [[NSMutableData alloc]init];
        
        request        =   [NSURLRequest requestWithURL:[NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] cachePolicy:NSURLCacheStorageAllowed timeoutInterval:5];
        
        connect        =   [[NSURLConnection alloc] initWithRequest:request delegate:self];
        
        [connect start];
    }else{
        [common showAlert:@"Warning" _message:@"No internet connection."];
    }
}

#pragma mark Table view Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //    NSLog(@"SECTION :: %d",section);
    return dataArray.count;
    //    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return SCREEN_HEIGHT*0.12;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *unifiedID = [NSString stringWithFormat:@"aCellIDPh_%d_%d",indexPath.section,indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:unifiedID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:unifiedID];
        
        cell.selectionStyle =   UITableViewCellSelectionStyleNone;
        cell.backgroundColor    =   [UIColor clearColor];
        
        UIImageView *imgV   =   [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.01, SCREEN_HEIGHT*0.10, SCREEN_HEIGHT*0.10)];
        imgV.contentMode = UIViewContentModeScaleAspectFill;
        [imgV sd_setImageWithURL:[NSURL URLWithString:[[dataArray objectAtIndex:indexPath.row] objectForKey:@"profile_pic"]] placeholderImage:[UIImage imageNamed:@"male.png"]];
        //        [imgV setImage:[UIImage imageNamed:@"male.png"]];
        [cell.contentView addSubview:imgV];
        
        UILabel *lbl    =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_HEIGHT*0.15, 0, SCREEN_WIDTH*0.60, SCREEN_HEIGHT*0.05)];
        [lbl setBackgroundColor:[UIColor clearColor]];
        [lbl setTextColor:[UIColor whiteColor]];
        [lbl setTextAlignment:NSTextAlignmentLeft];
        lbl.numberOfLines   =   0;
        lbl.lineBreakMode   =   NSLineBreakByWordWrapping;
        lbl.font    =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.04];
        lbl.text =   [NSString stringWithFormat:@"%@",[[dataArray objectAtIndex:indexPath.row] objectForKey:@"firstname"]];
        //        lbl.text    =   @"xyz";
        [cell.contentView addSubview:lbl];
        
        UILabel *lbl2    =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_HEIGHT*0.15, SCREEN_HEIGHT*0.05, SCREEN_WIDTH*0.60, SCREEN_HEIGHT*0.05)];
        [lbl2 setBackgroundColor:[UIColor clearColor]];
        [lbl2 setTextColor:[UIColor whiteColor]];
        [lbl2 setTextAlignment:NSTextAlignmentLeft];
        lbl2.numberOfLines   =   0;
        lbl2.lineBreakMode   =   NSLineBreakByWordWrapping;
        lbl2.font    =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.04];
        lbl2.text =   [NSString stringWithFormat:@"%@ - %@",[[dataArray objectAtIndex:indexPath.row] objectForKey:@"grift"],[[dataArray objectAtIndex:indexPath.row] objectForKey:@"relation"]];
        //        lbl2.text    =   @"ABC";
        [cell.contentView addSubview:lbl2];
        
        UIImageView *giftImgV   =   [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.70, SCREEN_HEIGHT*0.01, SCREEN_HEIGHT*0.10, SCREEN_HEIGHT*0.10)];
        giftImgV.contentMode = UIViewContentModeScaleAspectFill;
        giftImgV.image  =   [UIImage imageNamed:@"gift_list_icon.png"];
        [cell.contentView addSubview:giftImgV];
        
    }
    return cell;
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    giftIdToDelete  =   [[[dataArray objectAtIndex:indexPath.row] valueForKey:@"girft_id"] integerValue];
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Delete Gift ?" message:@"Do you want to delete this Gift?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    alert.delegate  =   self;
    [alert show];
}

#pragma mark alertview delegate method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==1) {
        NSLog(@"Yo Delete");
        [self deleteGift];
    }
}

#pragma mark Method to delete gift
-(void)deleteGift{
    if([common reachabilityFunction]) {
        
        NSString *urlStr = [NSString stringWithFormat:@"%@delete_gift.php?id=%d&user_id=%d",API_URL,giftIdToDelete,[[userDefaults valueForKey:@"userId"] integerValue]];
        
        urlStr  =   [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        responseData2   =   [[NSMutableData alloc]init];
        
        request2        =   [NSURLRequest requestWithURL:[NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] cachePolicy:NSURLCacheStorageAllowed timeoutInterval:5];
        
        connect2        =   [[NSURLConnection alloc] initWithRequest:request2 delegate:self];
        
        [connect2 start];
    }else{
        [common showAlert:@"Warning" _message:@"No internet connection."];
    }
}


#pragma mark Url connection delegate methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    //    NSLog(@"didReceiveResponse");
    
    if (connection==connect){
        // Login
        [responseData setLength:0];
    }else{
        [responseData2 setLength:0];
    }
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    //    NSLog(@"didFailWithError");
    
    if(responseData==nil){
        [common showAlert:@"Error" _message:@"Server Problem"];
    }else{
        [common showAlert:@"Error" _message:@"Server Problem"];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    if(connection==connect){
        [responseData appendData:data];
    }else{
        [responseData2 appendData:data];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    if (connection==connect){
        NSError* error;
        
        NSMutableDictionary *reviewsDict = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        
        dataArray    =   [[reviewsDict valueForKey:@"guest"] mutableCopy];
        
        NSLog(@"DICT :: %@",reviewsDict);
        [tableV reloadData];
    }else if (connection==connect2){
        NSError* error;
        
        NSMutableDictionary *reviewsDict = [NSJSONSerialization JSONObjectWithData:responseData2 options:kNilOptions error:&error];
        if ([[reviewsDict valueForKey:@"success"] integerValue] == 1){
            [common showAlert:@"Info" _message:@"Gift Deleted"];
        }else{
            [common showAlert:@"Warning" _message:@"Oops! An error occurred."];
        }
        [self loadGiftList];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
