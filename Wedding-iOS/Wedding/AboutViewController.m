//
//  AboutViewController.m
//  Wedding
//
//  Created by apple on 07/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController ()

@end

@implementation AboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIImageView *bgImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    //    bgImageView.image           =   [UIImage imageNamed:WEDDING_TAB_BG];
    [bgImageView setBackgroundColor:[UIColor colorWithRed:241.0f/255.0f green:155.0f/255.0f blue:204.0f/255.0f alpha:1.0f]];
    [self.view addSubview:bgImageView];
    
    UIImageView *topImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT*0.08)];
    topImageView.image           =   [UIImage imageNamed:TOP_IMG];
    [self.view addSubview:topImageView];
    
    UIImageView *titleImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.08, SCREEN_WIDTH, SCREEN_HEIGHT*0.065)];
    [titleImageView setBackgroundColor:[UIColor colorWithRed:197.0f/255.0f green:105.0f/255.0f blue:130.0f/255.0f alpha:1.0f]];
    titleImageView.alpha            =   0.7;
    [self.view addSubview:titleImageView];
    
    UILabel *titleLbl           =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.05, SCREEN_HEIGHT*0.08, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.06)];
    titleLbl.backgroundColor    =   [UIColor clearColor];
    titleLbl.text               =   @"About Us";
    titleLbl.textColor          =   [UIColor whiteColor];
    titleLbl.textAlignment      =   NSTextAlignmentLeft;
    titleLbl.font               =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.06];
    [self.view addSubview:titleLbl];
    
    UIButton *logoutBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.80, 0,SCREEN_WIDTH*0.20,SCREEN_HEIGHT*0.08)];
    [logoutBtn setBackgroundColor:[UIColor clearColor]];
    [logoutBtn setTitle:@"Logout" forState:UIControlStateNormal];
    [logoutBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    logoutBtn.titleLabel.font     =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.05];
    [logoutBtn addTarget:self action:@selector(logoutButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:logoutBtn];
    
    UIScrollView *contentScroll =   [[UIScrollView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.18, SCREEN_WIDTH, SCREEN_HEIGHT*0.70)];
    [contentScroll setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:contentScroll];
    
    UILabel *aboutTxt           =  [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.05, SCREEN_HEIGHT*0.02, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.20)];
    aboutTxt.backgroundColor    =   [UIColor clearColor];
    aboutTxt.text               =   @"Lorem ipsum dolor sit amet, per falli imperdiet te. No his quodsi impedit graecis, at nam case magna. Id est harum movet dicit, eum zril officiis intellegat ex, et tollit omnesque his. His no harum appetere. No dicta liber sed, eam animal fabulas perpetua et.\n\nEu mel tractatos necessitatibus, eu civibus elaboraret sea. Cu eos unum intellegam. Nec cu tollit partiendo incorrupte, idque doming mel ne. Tation utroque theophrastus sit eu, cu vix invidunt oportere referrentur, accusam rationibus pri no. Ei sea vidit aperiri assentior, quo in habemus legendos, nihil regione senserit eos in. Ei eam meis graeco, eu cetero vocibus pro.";
    aboutTxt.textColor          =   [UIColor whiteColor];
    aboutTxt.textAlignment      =   NSTextAlignmentLeft;
    aboutTxt.font               =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.05];
    aboutTxt.numberOfLines      =   0;
    aboutTxt.lineBreakMode      =   NSLineBreakByWordWrapping;
    [aboutTxt sizeToFit];
    [contentScroll addSubview:aboutTxt];
    
    UIButton *shareBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.25, aboutTxt.frame.size.height+SCREEN_HEIGHT*0.05,SCREEN_WIDTH*0.50,SCREEN_HEIGHT*0.07)];
    [shareBtn setBackgroundImage:[UIImage imageNamed:SHARE_BUTTON_IMG] forState:UIControlStateNormal];
    [shareBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [shareBtn addTarget:self action:@selector(shareButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [contentScroll addSubview:shareBtn];
    
    [contentScroll setContentSize:CGSizeMake(SCREEN_WIDTH, aboutTxt.frame.size.height+SCREEN_HEIGHT*0.15)];
    
}

#pragma mark Method to be called when tap on logout button
-(void)logoutButtonClicked{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark Method to be called when tap on share button
-(void)shareButtonClicked{
    //actionsheet initialisation........
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Facebook",@"Twitter",@"Email", nil];
    [actionSheet showInView:self.view];
}


-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
            NSLog(@"You have pressed the %@ button", [actionSheet buttonTitleAtIndex:buttonIndex]);
            [self fbBtnClick:self];
            break;
        case 1:
            NSLog(@"You have pressed the %@ button", [actionSheet buttonTitleAtIndex:buttonIndex]);
            [self TWBtnClick:self];
            break;
        case 2:
            NSLog(@"You have pressed the %@ button", [actionSheet buttonTitleAtIndex:buttonIndex]);
            [self mailBtn];
            break;
        default:
            // NSLog(@"You have pressed the %d button", buttonIndex);
            break;
            
            
    }
    
}


-(void)fbBtnClick : (id)sender
{
    // UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlStr]]];
    SLComposeViewController *facebookController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    SLComposeViewControllerCompletionHandler __block completionHandler=^(SLComposeViewControllerResult result){
        
        [facebookController dismissViewControllerAnimated:YES completion:nil];
        switch(result){
            case SLComposeViewControllerResultCancelled:
            default:
            {
                NSLog(@"Cancelled.....");
            }
                break;
            case SLComposeViewControllerResultDone:
            {
                NSLog(@"Posted....");
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Status" message:@"Successfully Posted on facebook." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }
                break;
        }};
    //--- [facebookController addImage:facebookImage];
    [facebookController setInitialText:SHARE_MESSAGE_ABOUT_US];
    // [facebookController addURL:[NSURL URLWithString:@"http://www.babybuckler.com/"]];
    [facebookController setCompletionHandler:completionHandler];
    [self presentViewController:facebookController animated:YES completion:nil];
    
}

-(void)TWBtnClick : (id)sender
{
    SLComposeViewController *twitterController=[SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    
    
    //    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    //    {
    SLComposeViewControllerCompletionHandler __block completionHandler=^(SLComposeViewControllerResult result){
        
        [twitterController dismissViewControllerAnimated:YES completion:nil];
        
        switch(result){
            case SLComposeViewControllerResultCancelled:
            default:
            {
                NSLog(@"Cancelled.....");
                
            }
                break;
            case SLComposeViewControllerResultDone:
            {
                
            }
                break;
        }};
    
    //-----[twitterController addImage:twitterImg];
    [twitterController setInitialText:SHARE_MESSAGE_ABOUT_US];
    //  [twitterController addURL:[NSURL URLWithString:[[globalJoge.globalDataAccess objectAtIndex:0] valueForKey:@"website_home_url"]]];
    [twitterController setCompletionHandler:completionHandler];
    [self presentViewController:twitterController animated:YES completion:nil];}


-(void)mailBtn
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        
        mailer.mailComposeDelegate = self;
        
        [mailer setSubject:@"subject"];
        
        NSString *emailBody = SHARE_MESSAGE_ABOUT_US;
        [mailer setMessageBody:emailBody isHTML:NO];
        
        [self presentViewController:mailer animated:YES completion:Nil];
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                        message:@"Your device doesn't support the composer sheet"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
    }
}


- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	switch (result)
	{
		case MFMailComposeResultCancelled:
			NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued");
			break;
		case MFMailComposeResultSaved:
			NSLog(@"Mail saved: you saved the email message in the Drafts folder");
			break;
		case MFMailComposeResultSent:
			NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send the next time the user connects to email");
			break;
		case MFMailComposeResultFailed:
			NSLog(@"Mail failed: the email message was nog saved or queued, possibly due to an error");
			break;
		default:
			NSLog(@"Mail not sent");
			break;
	}
    
	[self dismissViewControllerAnimated:YES completion:Nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
