//
//  WeddingViewController.m
//  Wedding
//
//  Created by apple on 07/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import "WeddingViewController.h"
#import "MyBudgetViewController.h"
#import "GiftListViewController.h"
#import "GuestListViewController.h"
#import "DateTimeViewController.h"
#import "TimetableViewController.h"
#import "FavoritesViewController.h"

@interface WeddingViewController ()

@end

@implementation WeddingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [dateLbl setText:@"Date/month/year"];
    NSString *weddingDate   =   [userDefaults valueForKey:KEY_WEDDING_DAY];
    
    if (weddingDate.length > 0) {
        [dateLbl setText:weddingDate];
    }
    
    [daysLeftLbl setText:@"Today"];
    if (weddingDate.length > 0 ) {
        [daysLeftLbl setText:[common dayCountBetweenDateAndCurrentDate:weddingDate]];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    userDefaults                =   [NSUserDefaults standardUserDefaults];
    
    UIImageView *bgImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    bgImageView.image           =   [UIImage imageNamed:WEDDING_TAB_BG];
    [self.view addSubview:bgImageView];
    
    UIImageView *topImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT*0.08)];
    topImageView.image           =   [UIImage imageNamed:TOP_IMG];
    [self.view addSubview:topImageView];
    
    UIImageView *titleImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.08, SCREEN_WIDTH, SCREEN_HEIGHT*0.065)];
    [titleImageView setBackgroundColor:[UIColor colorWithRed:197.0f/255.0f green:105.0f/255.0f blue:130.0f/255.0f alpha:1.0f]];
    titleImageView.alpha            =   0.7;
    [self.view addSubview:titleImageView];
    
    UILabel *titleLbl           =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.05, SCREEN_HEIGHT*0.08, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.06)];
    titleLbl.backgroundColor    =   [UIColor clearColor];
    titleLbl.text               =   @"My Wedding";
    titleLbl.textColor          =   [UIColor whiteColor];
    titleLbl.textAlignment      =   NSTextAlignmentLeft;
    titleLbl.font               =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.06];
    [self.view addSubview:titleLbl];
    
    UIButton *logoutBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.80, 0,SCREEN_WIDTH*0.20,SCREEN_HEIGHT*0.08)];
    [logoutBtn setBackgroundColor:[UIColor clearColor]];
    [logoutBtn setTitle:@"Logout" forState:UIControlStateNormal];
    [logoutBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    logoutBtn.titleLabel.font     =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.05];
    [logoutBtn addTarget:self action:@selector(logoutButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:logoutBtn];
    
    UIImageView *blackBG        =   [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.03, SCREEN_HEIGHT*0.16, SCREEN_WIDTH*0.94, SCREEN_HEIGHT*0.84)];
    [blackBG setBackgroundColor:[UIColor blackColor]];
    blackBG.alpha               =   0.6;
    [self.view addSubview:blackBG];
    
    float leftMargin    =   SCREEN_WIDTH*0.10;
    float topMargin     =   SCREEN_HEIGHT*0.20;
    
    NSArray *optionTitleArr =   [[NSArray alloc]initWithObjects:@"My budget",@"Gift List",@"Date Time",@"Guest List",@"Timetable",@"Favourites", nil];
    NSArray *optionImgArr =   [[NSArray alloc]initWithObjects:@"budget_icon.png",@"gift_list_icon.png",@"note_icon.png",@"guest_icon.png",@"pro_icon.png",@"fev_icon.png", nil];
    
    for (int i=0; i<6; i++) {
        if (i%3==0 && i!=0) {
            leftMargin  =   SCREEN_WIDTH*0.10;
            topMargin   +=  SCREEN_HEIGHT*0.20;
        }
        UIButton *optionBtn =   [[UIButton alloc]initWithFrame:CGRectMake(leftMargin, topMargin, SCREEN_WIDTH*0.25, SCREEN_HEIGHT*0.12)];
        [optionBtn setImage:[UIImage imageNamed:[optionImgArr objectAtIndex:i]] forState:UIControlStateNormal];
        optionBtn.tag       =   i+1;
        [optionBtn addTarget:self action:@selector(optionBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:optionBtn];
        
        UILabel *optionTitle    =   [[UILabel alloc]initWithFrame:CGRectMake(leftMargin, topMargin+SCREEN_HEIGHT*0.12, SCREEN_WIDTH*0.25, SCREEN_HEIGHT*0.06)];
        [optionTitle setText:[optionTitleArr objectAtIndex:i]];
        [optionTitle setBackgroundColor:[UIColor clearColor]];
        [optionTitle setTextColor:[UIColor whiteColor]];
        [optionTitle setTextAlignment:NSTextAlignmentCenter];
        optionTitle.font    =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.045];
        [self.view addSubview:optionTitle];
        
        leftMargin  +=  SCREEN_WIDTH*0.28;
    }
    
    UIImageView *whiteBg    =   [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.10, SCREEN_HEIGHT*0.60, SCREEN_WIDTH*0.80, SCREEN_HEIGHT*0.25)];
    [whiteBg setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:whiteBg];
    
    UILabel *myWeddingTitle    =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.15, SCREEN_HEIGHT*0.62, SCREEN_WIDTH*0.70, SCREEN_HEIGHT*0.06)];
    [myWeddingTitle setText:@"My Wedding Day"];
    [myWeddingTitle setBackgroundColor:[UIColor clearColor]];
    [myWeddingTitle setTextColor:[UIColor blackColor]];
    [myWeddingTitle setTextAlignment:NSTextAlignmentCenter];
    myWeddingTitle.font    =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.05];
    [self.view addSubview:myWeddingTitle];
    
    UILabel *dateTitle    =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.15, SCREEN_HEIGHT*0.68, SCREEN_WIDTH*0.35, SCREEN_HEIGHT*0.06)];
    [dateTitle setText:@"Date:"];
    [dateTitle setBackgroundColor:[UIColor clearColor]];
    [dateTitle setTextColor:[UIColor blackColor]];
    [dateTitle setTextAlignment:NSTextAlignmentLeft];
    dateTitle.font    =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.05];
    [self.view addSubview:dateTitle];
    
    dateLbl    =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.45, SCREEN_HEIGHT*0.68, SCREEN_WIDTH*0.40, SCREEN_HEIGHT*0.06)];
    [dateLbl setText:@"Date/month/year"];
    NSString *weddingDate   =   [userDefaults valueForKey:KEY_WEDDING_DAY];
    
    if (weddingDate.length > 0) {
        [dateLbl setText:weddingDate];
    }
    [dateLbl setBackgroundColor:[UIColor clearColor]];
    [dateLbl setTextColor:[UIColor blackColor]];
    [dateLbl setTextAlignment:NSTextAlignmentLeft];
    dateLbl.font    =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.05];
    [self.view addSubview:dateLbl];
    
    UILabel *daysLeftTitle    =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.15, SCREEN_HEIGHT*0.74, SCREEN_WIDTH*0.35, SCREEN_HEIGHT*0.06)];
    [daysLeftTitle setText:@"Days Left:"];
    [daysLeftTitle setBackgroundColor:[UIColor clearColor]];
    [daysLeftTitle setTextColor:[UIColor blackColor]];
    [daysLeftTitle setTextAlignment:NSTextAlignmentLeft];
    daysLeftTitle.font    =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.05];
    [self.view addSubview:daysLeftTitle];
    
    daysLeftLbl    =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.45, SCREEN_HEIGHT*0.74, SCREEN_WIDTH*0.40, SCREEN_HEIGHT*0.06)];
    [daysLeftLbl setText:@"Today"];
    if (weddingDate.length > 0 ) {
        [daysLeftLbl setText:[common dayCountBetweenDateAndCurrentDate:weddingDate]];
    }
    [daysLeftLbl setBackgroundColor:[UIColor clearColor]];
    [daysLeftLbl setTextColor:[UIColor blackColor]];
    [daysLeftLbl setTextAlignment:NSTextAlignmentLeft];
    daysLeftLbl.font    =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.05];
    [self.view addSubview:daysLeftLbl];
    
}

#pragma mark Method to be called when tap on logout button
-(void)logoutButtonClicked{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark Method to be called when option button is clicked
-(void)optionBtnClicked:(UIButton *)sender{
    NSLog(@"SENDER TAG :: %d",sender.tag);
    if (sender.tag == 1) {
        MyBudgetViewController *myBudgetVC  =   [[MyBudgetViewController alloc]init];
        [self.navigationController pushViewController:myBudgetVC animated:NO];
    }else if (sender.tag == 2) {
        GiftListViewController *giftListVC  =   [[GiftListViewController alloc]init];
        [self.navigationController pushViewController:giftListVC animated:NO];
    }else if (sender.tag == 3) {
        DateTimeViewController *dateVC  =   [[DateTimeViewController alloc]init];
        [self.navigationController pushViewController:dateVC animated:NO];
    }else if (sender.tag == 4) {
        GuestListViewController *guestListVC  =   [[GuestListViewController alloc]init];
        [self.navigationController pushViewController:guestListVC animated:NO];
    }else if (sender.tag == 5) {
        TimetableViewController *VC  =   [[TimetableViewController alloc]init];
        [self.navigationController pushViewController:VC animated:NO];
    }else if (sender.tag == 6) {
        FavoritesViewController *VC  =   [[FavoritesViewController alloc]init];
        [self.navigationController pushViewController:VC animated:NO];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
