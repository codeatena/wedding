//
//  DetailsViewController.m
//  Wedding
//
//  Created by apple on 08/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import "DetailsViewController.h"
#import "MapViewController.h"

@interface DetailsViewController ()

@end

@implementation DetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initWithCustomData:(NSDictionary *)_dataDict{
    self = [super init];
    if (self) {
        // Custom initialization
        dataDict   =   [[NSDictionary alloc]init];
        dataDict   =   [_dataDict copy];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    suppDataDict   =   [[NSDictionary alloc]init];
    
    imgArr         =   [[NSMutableArray alloc]init];
    
    userDefaults   =   [NSUserDefaults standardUserDefaults];
    
    [self loadSingleSupplierDetails:[dataDict objectForKey:@"id"]];
    
    UIImageView *bgImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    //    bgImageView.image           =   [UIImage imageNamed:WEDDING_TAB_BG];
    [bgImageView setBackgroundColor:[UIColor colorWithRed:241.0f/255.0f green:155.0f/255.0f blue:204.0f/255.0f alpha:1.0f]];
    [self.view addSubview:bgImageView];
    
    UIImageView *topImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT*0.08)];
    topImageView.image           =   [UIImage imageNamed:TOP_IMG];
    [self.view addSubview:topImageView];
    
    UIImageView *titleImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.08, SCREEN_WIDTH, SCREEN_HEIGHT*0.065)];
    [titleImageView setBackgroundColor:[UIColor colorWithRed:197.0f/255.0f green:105.0f/255.0f blue:130.0f/255.0f alpha:1.0f]];
    titleImageView.alpha            =   0.7;
    [self.view addSubview:titleImageView];
    
    UILabel *titleLbl           =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.10, SCREEN_HEIGHT*0.08, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.06)];
    titleLbl.backgroundColor    =   [UIColor clearColor];
    titleLbl.text               =   @"Supplier Details";
    titleLbl.textColor          =   [UIColor whiteColor];
    titleLbl.textAlignment      =   NSTextAlignmentLeft;
    titleLbl.font               =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.06];
    [self.view addSubview:titleLbl];
    
    UIButton *backBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.03, SCREEN_HEIGHT*0.09,SCREEN_WIDTH*0.05,SCREEN_HEIGHT*0.04)];
    [backBtn setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMG] forState:UIControlStateNormal];
    [backBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    
    NSLog(@"DETAIL DATA ARRAY :: %@",dataDict);
    
    contentScroll   =   [[UIScrollView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.20, SCREEN_WIDTH, SCREEN_HEIGHT*0.66)];
    [contentScroll setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:contentScroll];
    
    float topMargin =   SCREEN_HEIGHT*0.01;
    
    UIButton *addFavBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.55, topMargin+SCREEN_HEIGHT*0.02, SCREEN_WIDTH*0.04, SCREEN_WIDTH*0.04)];
    [addFavBtn setImage:[UIImage imageNamed:CHECKBOX_UNSELECTED_IMG] forState:UIControlStateNormal];
    [addFavBtn addTarget:self action:@selector(favBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [contentScroll addSubview:addFavBtn];
    
    UILabel *addFavLbl         =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.60, topMargin, SCREEN_WIDTH*0.40, SCREEN_HEIGHT*0.06)];
    addFavLbl.backgroundColor  =   [UIColor clearColor];
    addFavLbl.text             =   @"Add to Favorite";
    addFavLbl.textColor        =   [UIColor whiteColor];
    addFavLbl.textAlignment    =   NSTextAlignmentLeft;
    addFavLbl.font             =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.045];
    [contentScroll addSubview:addFavLbl];
    
    topMargin   +=  SCREEN_HEIGHT*0.07;
    
    preBtn    =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.05, topMargin+SCREEN_HEIGHT*0.07, SCREEN_WIDTH*0.05, SCREEN_HEIGHT*0.04)];
    [preBtn setBackgroundImage:[UIImage imageNamed:PREVIOUS_BUTTON_IMG] forState:UIControlStateNormal];
    [preBtn addTarget:self action:@selector(preBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [preBtn setHidden:YES];
    [contentScroll addSubview:preBtn];
    
    supplierImgView    =   [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.375, topMargin, SCREEN_WIDTH*0.25, SCREEN_WIDTH*0.25)];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
    [supplierImgView setUserInteractionEnabled:YES];
    [supplierImgView addGestureRecognizer:tapGestureRecognizer];
    [contentScroll addSubview:supplierImgView];
    
    nxtBtn    =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.90, topMargin+SCREEN_HEIGHT*0.07, SCREEN_WIDTH*0.05, SCREEN_HEIGHT*0.04)];
    [nxtBtn setBackgroundImage:[UIImage imageNamed:NEXT_BUTTON_IMG] forState:UIControlStateNormal];
    [nxtBtn addTarget:self action:@selector(nextBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [contentScroll addSubview:nxtBtn];
    
    topMargin   +=  SCREEN_HEIGHT*0.23;
    
    UIImageView *nameBG     =   [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.05, topMargin, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.06)];
    [nameBG setBackgroundColor:[UIColor whiteColor]];
    [nameBG setAlpha:0.7];
    [contentScroll addSubview:nameBG];
    
    UILabel *supplierNameLbl         =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.10, topMargin, SCREEN_WIDTH*0.80, SCREEN_HEIGHT*0.06)];
    supplierNameLbl.backgroundColor  =   [UIColor clearColor];
    supplierNameLbl.text             =   [dataDict objectForKey:@"suppler_name"];
    supplierNameLbl.textColor        =   [UIColor blackColor];
    supplierNameLbl.textAlignment    =   NSTextAlignmentCenter;
    supplierNameLbl.font             =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.045];
    [contentScroll addSubview:supplierNameLbl];
    
    topMargin   +=  SCREEN_HEIGHT*0.08;
    
    float leftMargin    =   SCREEN_WIDTH*0.10;
    //    float topMargin2    =   topMargin;
    NSArray *keyArray   =   [[NSArray alloc]initWithObjects:@"suppler_name",@"address",@"suppler_phone",@"email",@"web",@"description", nil];
    NSArray *prefixArr  =   [[NSArray alloc]initWithObjects:@"",@"",@"Tel: ",@"Email: ",@"Web: ",@"", nil];
    for (int i=0; i<6; i++) {
        UILabel *detailLbl  =   [[UILabel alloc]initWithFrame:CGRectMake(leftMargin, topMargin, SCREEN_WIDTH*0.80, SCREEN_HEIGHT*0.05)];
        detailLbl.backgroundColor  =   [UIColor clearColor];
        detailLbl.text             =   [NSString stringWithFormat:@"%@%@",[prefixArr objectAtIndex:i],[dataDict valueForKey:[keyArray objectAtIndex:i]]];
        detailLbl.textColor        =   [UIColor whiteColor];
        detailLbl.textAlignment    =   NSTextAlignmentLeft;
        detailLbl.font             =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.037];
        detailLbl.numberOfLines    =   0;
        detailLbl.lineBreakMode    =   NSLineBreakByWordWrapping;
        [detailLbl sizeToFit];
        [contentScroll addSubview:detailLbl];
        
        topMargin  +=  detailLbl.frame.size.height+SCREEN_HEIGHT*0.01;
    }
    
    [contentScroll setContentSize:CGSizeMake(contentScroll.frame.size.width, topMargin)];
    
    UIImageView *seperatorBG     =   [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.86, SCREEN_WIDTH, SCREEN_HEIGHT*0.04)];
    [seperatorBG setBackgroundColor:[UIColor whiteColor]];
    [seperatorBG setAlpha:0.7];
    [self.view addSubview:seperatorBG];
    
    // To draw bottom tab bar
    NSArray *tabImgNameArr   =  [[NSArray alloc]initWithObjects:@"email.png",@"website.png",@"call.png",@"map.png", nil];
    NSArray *tabNameArr   =  [[NSArray alloc]initWithObjects:@"Email",@"Website",@"Call",@"Map", nil];
    leftMargin    =   SCREEN_WIDTH*0.05;
    for (int i=0; i<4; i++) {
        UIButton *tabBtn    =   [[UIButton alloc]initWithFrame:CGRectMake(leftMargin, SCREEN_HEIGHT*0.905, SCREEN_WIDTH*0.12, SCREEN_HEIGHT*.06)];
        [tabBtn setBackgroundImage:[UIImage imageNamed:[tabImgNameArr objectAtIndex:i]] forState:UIControlStateNormal];
        tabBtn.tag  =   i+10;
        [tabBtn addTarget:self action:@selector(tabBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:tabBtn];
        
        UILabel *tabTitleLbl  =   [[UILabel alloc]initWithFrame:CGRectMake(leftMargin, SCREEN_HEIGHT*0.96, SCREEN_WIDTH*0.12, SCREEN_HEIGHT*0.04)];
        tabTitleLbl.backgroundColor  =   [UIColor clearColor];
        tabTitleLbl.text             =   [tabNameArr objectAtIndex:i];
        tabTitleLbl.textColor        =   [UIColor whiteColor];
        tabTitleLbl.textAlignment    =   NSTextAlignmentCenter;
        tabTitleLbl.font             =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.03];
        [self.view addSubview:tabTitleLbl];
        
        leftMargin  +=  SCREEN_WIDTH*0.26;
    }
    
    
    
}

#pragma mark Back Button Method
-(void)backButtonClicked{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark Method to be called when tap on supplier imageview
-(void)handleTapFrom: (UITapGestureRecognizer *)recognizer{
    [contentScroll bringSubviewToFront:supplierImgView];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    if (isZoomedIn != 1) {
        isZoomedIn  =   1;
        supplierImgView.frame    =   CGRectMake(SCREEN_WIDTH*0.15, supplierImgView.frame.origin.y, SCREEN_WIDTH*0.70, SCREEN_WIDTH*0.70);
    }else{
        isZoomedIn  =   0;
        supplierImgView.frame    =   CGRectMake(SCREEN_WIDTH*0.375, supplierImgView.frame.origin.y, SCREEN_WIDTH*0.25, SCREEN_WIDTH*0.25);
    }
    
    
    [UIView commitAnimations];
}

#pragma mark Method to be called when add to favorite button is taped
-(void)favBtnClicked:(UIButton *)sender{
    if (isFavorite) {
        [sender setImage:[UIImage imageNamed:CHECKBOX_UNSELECTED_IMG] forState:UIControlStateNormal];
        isFavorite  =   0;
    }else{
        [sender setImage:[UIImage imageNamed:CHECKBOX_SELECTED_IMG] forState:UIControlStateNormal];
        isFavorite  =   1;
    }
    [self addToFav];
}

#pragma mark method to add supplier to favourites
-(void)addToFav{
    if([common reachabilityFunction]) {
        
        NSString *favStatus =   @"";
        if (isFavorite) {
            favStatus =   @"favourite";
        }
        
        NSString *urlStr = [NSString stringWithFormat:@"%@add_favorite.php?user_id=%d&supplier_id=%@&status=%@",API_URL,[[userDefaults valueForKey:@"userId"] integerValue],[dataDict valueForKey:@"id"],favStatus];
        
//        NSLog(@"%@",urlStr);
        
        urlStr  =   [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        responseData2   =   [[NSMutableData alloc]init];
        
        request2        =   [NSURLRequest requestWithURL:[NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] cachePolicy:NSURLCacheStorageAllowed timeoutInterval:5];
        
        connect2        =   [[NSURLConnection alloc] initWithRequest:request2 delegate:self];
        
        [connect2 start];
    }else{
        [common showAlert:@"Warning" _message:@"No internet connection."];
    }
}

#pragma mark Next button click method
-(void)preBtnClicked{
    if (imgIndex > 0) {
        imgIndex--;
        [supplierImgView sd_setImageWithURL:[NSURL URLWithString:[[imgArr objectAtIndex:imgIndex] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_NAME]];
        [nxtBtn setHidden:NO];
        if (imgIndex == 0) {
            [preBtn setHidden:YES];
        }
    }
}

#pragma mark Previous button click method
-(void)nextBtnClicked{
    if (imgArr.count > 1 && imgIndex < imgArr.count-1) {
        imgIndex++;
        [supplierImgView sd_setImageWithURL:[NSURL URLWithString:[[imgArr objectAtIndex:imgIndex] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_NAME]];
        [preBtn setHidden:NO];
        if (imgIndex >= imgArr.count-1){
            [nxtBtn setHidden:YES];
        }
        NSLog(@"DEBUG IMAGE ARR :: %@",imgArr);
    }
}

#pragma mark Method to be called when tab button is tapped
-(void)tabBtnClicked:(UIButton *)sender{
    //    NSLog(@"tabBtnClicked %d ",sender.tag-10);
    if (sender.tag-10 == 0) {
        MFMailComposeViewController *mc =   [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate          =   self;
        [mc setSubject:@""];
        [mc setMessageBody:@"" isHTML:NO];
        NSLog(@"DEBUG :::::: %@",[dataDict valueForKey:@"email"]);
        [mc setToRecipients:[[NSArray alloc] initWithObjects:[dataDict valueForKey:@"email"], nil ]];
        
        // Present mail view controller on screen
        //    [self addSubview:mc.view];
        [self presentViewController:mc animated:YES completion:NULL];
    }else if (sender.tag-10 == 1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[dataDict valueForKey:@"web"]]];
    }else if (sender.tag-10 == 2) {
        NSURL *phoneURL = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"Tel:%@",[dataDict valueForKey:@"suppler_phone"]]];
        [[UIApplication sharedApplication] openURL:phoneURL];
    }else if (sender.tag-10 == 3) {
        MapViewController *mapVC    =   [[MapViewController alloc]initWithCustomData:[dataDict valueForKey:@"address"]];
        [self.navigationController pushViewController:mapVC animated:NO];
    }
}

#pragma mark MFMAilComposer Delegate Methods
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark Method to load single supplier data
-(void)loadSingleSupplierDetails:(NSString *)_id{
    if([common reachabilityFunction]) {
        NSString *urlStr = [NSString stringWithFormat:@"%@single_suplier.php?id=%d",API_URL,[_id integerValue]];
        
        urlStr  =   [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        responseData   =   [[NSMutableData alloc]init];
        
        request        =   [NSURLRequest requestWithURL:[NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] cachePolicy:NSURLCacheStorageAllowed timeoutInterval:5];
        
        connect        =   [[NSURLConnection alloc] initWithRequest:request delegate:self];
        
        [connect start];
    }else{
        [common showAlert:@"Warning" _message:@"No internet connection."];
    }
}

#pragma mark Url connection delegate methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    //    NSLog(@"didReceiveResponse");
    
    if (connection==connect){
        // Login
        [responseData setLength:0];
    }else if (connection==connect2){
        // Login
        [responseData2 setLength:0];
    }
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    //    NSLog(@"didFailWithError");
    
    if(responseData==nil){
        [common showAlert:@"Error" _message:@"Server Problem"];
    }else if(responseData2==nil){
        [common showAlert:@"Error" _message:@"Server Problem"];
    }
    
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    if(connection==connect){
        [responseData appendData:data];
    }else if(connection==connect2){
        [responseData2 appendData:data];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    if (connection==connect){
        NSError* error;
        
        //        NSString *responseString = [[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
        
        //        NSLog(@"Response :::: %@",responseString);
        
        NSMutableDictionary *reviewsDict = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        
        //        NSLog(@"DICT :: %@",[[reviewsDict valueForKey:@"get_suppler_category"] objectAtIndex:0]);
        //        dataArray    =   [[reviewsDict valueForKey:@"suppliers"] mutableCopy];
        NSArray *imgTemp    =   [[reviewsDict valueForKey:@"inventorys"] mutableCopy];
        suppDataDict    =   [reviewsDict copy];
        NSLog(@"DICT :::::: %@",reviewsDict);
        
        if (imgTemp.count > 0) {
            for (int i=0 ; i<imgTemp.count; i++) {
                [imgArr addObject:[[imgTemp objectAtIndex:i] valueForKey:@"image"]];
            }
            imgIndex    =   0;
            [supplierImgView sd_setImageWithURL:[NSURL URLWithString:[[imgArr objectAtIndex:imgIndex] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_NAME]];
            if (imgArr.count == 1) {
                [nxtBtn setHidden:YES];
            }
        }
        
        
    }else if (connection==connect2){
        NSError* error;
        NSMutableDictionary *reviewsDict = [NSJSONSerialization JSONObjectWithData:responseData2 options:kNilOptions error:&error];
        if ([[reviewsDict valueForKey:@"success"] integerValue] == 1){
            [common showAlert:@"Info" _message:@"Status Changed"];
        }else{
            [common showAlert:@"Warning" _message:@"Oops! An error occurred."];
        }
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
