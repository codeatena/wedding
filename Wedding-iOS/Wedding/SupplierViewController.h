//
//  SupplierViewController.h
//  Wedding
//
//  Created by apple on 07/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SupplierViewController : UIViewController<NSURLConnectionDelegate,NSURLConnectionDataDelegate,UICollectionViewDataSource,UICollectionViewDelegate>{
    UICollectionView        *collectionV;
    NSMutableData           *responseData;
    NSURLRequest            *request;
    NSURLConnection         *connect;
    NSMutableArray          *dataArray;
}

@end
