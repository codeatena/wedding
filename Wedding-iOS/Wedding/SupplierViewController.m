//
//  SupplierViewController.m
//  Wedding
//
//  Created by apple on 07/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import "SupplierViewController.h"
#import "CustomCollectionCell.h"
#import "SupplierDetailsViewController.h"

@interface SupplierViewController ()

@end

@implementation SupplierViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    dataArray                   =   [[NSMutableArray alloc]init];
    
    UIImageView *bgImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    //    bgImageView.image           =   [UIImage imageNamed:WEDDING_TAB_BG];
    [bgImageView setBackgroundColor:[UIColor colorWithRed:241.0f/255.0f green:155.0f/255.0f blue:204.0f/255.0f alpha:1.0f]];
    [self.view addSubview:bgImageView];
    
    UIImageView *topImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT*0.08)];
    topImageView.image           =   [UIImage imageNamed:TOP_IMG];
    [self.view addSubview:topImageView];
    
    UIImageView *titleImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.08, SCREEN_WIDTH, SCREEN_HEIGHT*0.065)];
    [titleImageView setBackgroundColor:[UIColor colorWithRed:197.0f/255.0f green:105.0f/255.0f blue:130.0f/255.0f alpha:1.0f]];
    titleImageView.alpha            =   0.7;
    [self.view addSubview:titleImageView];
    
    UILabel *titleLbl           =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.05, SCREEN_HEIGHT*0.08, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.06)];
    titleLbl.backgroundColor    =   [UIColor clearColor];
    titleLbl.text               =   @"Suppliers";
    titleLbl.textColor          =   [UIColor whiteColor];
    titleLbl.textAlignment      =   NSTextAlignmentLeft;
    titleLbl.font               =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.06];
    [self.view addSubview:titleLbl];
    
    UIButton *logoutBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.80, 0,SCREEN_WIDTH*0.20,SCREEN_HEIGHT*0.08)];
    [logoutBtn setBackgroundColor:[UIColor clearColor]];
    [logoutBtn setTitle:@"Logout" forState:UIControlStateNormal];
    [logoutBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    logoutBtn.titleLabel.font     =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.05];
    [logoutBtn addTarget:self action:@selector(logoutButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:logoutBtn];
    
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc]init];
    [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [layout setItemSize:CGSizeMake(SCREEN_WIDTH*0.20,SCREEN_HEIGHT*0.20)];
    
    collectionV     =   [[UICollectionView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.16, SCREEN_WIDTH, SCREEN_HEIGHT*0.75) collectionViewLayout:layout];
    
    collectionV.backgroundColor =   [UIColor clearColor];
    collectionV.delegate        =   self;
    collectionV.dataSource      =   self;
    //    [collectionV setBackgroundColor:[UIColor grayColor]];
    // [collectionV setPagingEnabled:YES];
    [collectionV registerClass:[CustomCollectionCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [self.view addSubview:collectionV];
    
    [self loadSuppliersCategoryList];
}

#pragma mark Method to be called when tap on logout button
-(void)logoutButtonClicked{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark Method to load Supplier's category list
-(void)loadSuppliersCategoryList{
    if([common reachabilityFunction]) {
        NSString *urlStr = [NSString stringWithFormat:@"%@get_suppler_category.php",API_URL];
        
        urlStr  =   [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        responseData   =   [[NSMutableData alloc]init];
        
        request        =   [NSURLRequest requestWithURL:[NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] cachePolicy:NSURLCacheStorageAllowed timeoutInterval:5];
        
        connect        =   [[NSURLConnection alloc] initWithRequest:request delegate:self];
        
        [connect start];
    }else{
        [common showAlert:@"Warning" _message:@"No internet connection."];
    }
}

#pragma mark Url connection delegate methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    //    NSLog(@"didReceiveResponse");
    
    if (connection==connect){
        // Login
        [responseData setLength:0];
    }
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    //    NSLog(@"didFailWithError");
    
    if(responseData==nil){
        [common showAlert:@"Error" _message:@"Server Problem"];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    if(connection==connect){
        [responseData appendData:data];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    if (connection==connect){
        NSError* error;
        
        //        NSString *responseString = [[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
        
        //        NSLog(@"Response :::: %@",responseString);
        
        NSMutableDictionary *reviewsDict = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        
        //        NSLog(@"DICT :: %@",[[reviewsDict valueForKey:@"get_suppler_category"] objectAtIndex:0]);
        dataArray    =   [[reviewsDict valueForKey:@"get_suppler_category"] mutableCopy];
        
        //        NSLog(@"DICT :: %@",dataArray);
        [collectionV reloadData];
    }
}

#pragma mark Collection view Datasource and delegates

-(UIEdgeInsets) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(SCREEN_WIDTH*0.04,SCREEN_WIDTH*0.04,SCREEN_WIDTH*0.04,SCREEN_WIDTH*0.04);
}

-(NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return dataArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *identifier=@"cellIdentifier";
    
    CustomCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    if (!cell) {
        cell = [[CustomCollectionCell alloc] initWithFrame:CGRectMake(0,0,cell.frame.size.width , cell.frame.size.height)];
    }
    
    NSString* url = [[dataArray objectAtIndex:indexPath.row] objectForKey:@"category_image"];
    NSString* filename = [url substringFromIndex:54];
//    NSLog(@"categery image filename = %@", filename);
    NSString* filePath = [NSString stringWithFormat:@"%@%@", UPLOAD_URL, filename];
    [cell.imageV sd_setImageWithURL:[NSURL URLWithString:filePath] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_NAME]];
    cell.nameLbl.text   =   [[dataArray objectAtIndex:indexPath.row] objectForKey:@"category_name"];
    
    return cell;
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    SupplierDetailsViewController *supplierDetailVC =   [[SupplierDetailsViewController alloc]initWithCustomData:[[[dataArray objectAtIndex:indexPath.row] objectForKey:@"id"] integerValue]];
    [self.navigationController pushViewController:supplierDetailVC animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
