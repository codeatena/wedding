//
//  MyBudgetViewController.m
//  Wedding
//
//  Created by apple on 08/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import "MyBudgetViewController.h"

@interface MyBudgetViewController ()

@end

@implementation MyBudgetViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    userDefault                 =   [NSUserDefaults standardUserDefaults];
    
    UIImageView *bgImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    bgImageView.image           =   [UIImage imageNamed:MY_BUDGET_BG_IMG];
    [self.view addSubview:bgImageView];
    
    UIImageView *topImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT*0.08)];
    topImageView.image           =   [UIImage imageNamed:TOP_IMG];
    [self.view addSubview:topImageView];
    
    UIImageView *titleImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.08, SCREEN_WIDTH, SCREEN_HEIGHT*0.065)];
    [titleImageView setBackgroundColor:[UIColor colorWithRed:197.0f/255.0f green:105.0f/255.0f blue:130.0f/255.0f alpha:1.0f]];
    titleImageView.alpha            =   0.7;
    [self.view addSubview:titleImageView];
    
    UILabel *titleLbl           =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.10, SCREEN_HEIGHT*0.08, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.06)];
    titleLbl.backgroundColor    =   [UIColor clearColor];
    titleLbl.text               =   @"Wedding Budget";
    titleLbl.textColor          =   [UIColor whiteColor];
    titleLbl.textAlignment      =   NSTextAlignmentLeft;
    titleLbl.font               =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.06];
    [self.view addSubview:titleLbl];
    
    UIButton *backBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.03, SCREEN_HEIGHT*0.09,SCREEN_WIDTH*0.05,SCREEN_HEIGHT*0.04)];
    [backBtn setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMG] forState:UIControlStateNormal];
    [backBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    
    UIButton *logoutBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.80, 0,SCREEN_WIDTH*0.20,SCREEN_HEIGHT*0.08)];
    [logoutBtn setBackgroundColor:[UIColor clearColor]];
    [logoutBtn setTitle:@"Logout" forState:UIControlStateNormal];
    [logoutBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    logoutBtn.titleLabel.font     =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.05];
    [logoutBtn addTarget:self action:@selector(logoutButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:logoutBtn];
    
    toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    toolBar.barStyle = UIBarStyleBlackOpaque;
    UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneBtnClicked)];
    
    [toolBar setItems:[NSArray arrayWithObjects:flexibleSpaceLeft,btn, nil]];
    
    
    
    int isBudgetSet =   [userDefault integerForKey:KEY_IS_BUDGET_SET];
    if (isBudgetSet == 0) {
        // Show alert view to enter budget
        UIAlertView *alerView = [[UIAlertView alloc]initWithTitle:@"Add Wedding Budget" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Cancel",@"Add", nil];
        alerView.alertViewStyle =   UIAlertViewStylePlainTextInput;
        UITextField *textField  =   [alerView textFieldAtIndex:0];
        textField.delegate      =   self;
        textField.keyboardType  =   UIKeyboardTypeNumberPad;
        textField.placeholder   =   @"Budget";
        [alerView show];
    }
    
    contentScroll =   [[UIScrollView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.16, SCREEN_WIDTH, SCREEN_HEIGHT*0.84)];
    [contentScroll setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:contentScroll];
    
    [self manuallViewMaking];
}

#pragma mark Back Button Method
-(void)backButtonClicked{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark Method to be called when tap on logout button
-(void)logoutButtonClicked{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

#pragma mark Method to save added expence
-(void)doTotal{
    
    [userDefault setInteger:[((UITextField *)[self.view viewWithTag:1]).text integerValue] forKey:KEY_TRANSPORTATION_EXPENCE];
    [userDefault setInteger:[((UITextField *)[self.view viewWithTag:2]).text integerValue] forKey:KEY_CEREMONY_EXPENCE];
    [userDefault setInteger:[((UITextField *)[self.view viewWithTag:3]).text integerValue] forKey:KEY_FLOWER_EXPENCE];
    [userDefault setInteger:[((UITextField *)[self.view viewWithTag:4]).text integerValue] forKey:KEY_PHOTOGRAPHY_EXPENCE];
    [userDefault setInteger:[((UITextField *)[self.view viewWithTag:5]).text integerValue] forKey:KEY_ATTIRE_EXPENCE];
    [userDefault setInteger:[((UITextField *)[self.view viewWithTag:6]).text integerValue] forKey:KEY_OTHER_EXPENCE];
    [userDefault setInteger:[((UITextField *)[self.view viewWithTag:7]).text integerValue] forKey:KEY_RECEPTION_EXPENCE];
    
    [userDefault synchronize];
    
    [self manuallViewMaking];
}

#pragma mark Method to make view
-(void)manuallViewMaking{
    [[contentScroll subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    weddingBudget           =   [userDefault integerForKey:KEY_WEDDING_BUDGET];
    transportationExpence   =   [userDefault integerForKey:KEY_TRANSPORTATION_EXPENCE];
    ceremonyExpence         =   [userDefault integerForKey:KEY_CEREMONY_EXPENCE];
    flowerExpence           =   [userDefault integerForKey:KEY_FLOWER_EXPENCE];
    photographyExpence      =   [userDefault integerForKey:KEY_PHOTOGRAPHY_EXPENCE];
    attireExpence           =   [userDefault integerForKey:KEY_ATTIRE_EXPENCE];
    otherExpence            =   [userDefault integerForKey:KEY_OTHER_EXPENCE];
    receptionExpence        =   [userDefault integerForKey:KEY_RECEPTION_EXPENCE];
    
    
    spentToDate             =   transportationExpence+ceremonyExpence+flowerExpence+photographyExpence+attireExpence+otherExpence+receptionExpence;
    remainingBudget         =   weddingBudget-spentToDate;
    
    float leftMargin    =   SCREEN_WIDTH*0.05;
    float topMargin     =   SCREEN_HEIGHT*0.02;
    NSArray *titleArr   =   [[NSArray alloc]initWithObjects:@"Total Budget:",@"Spent to date:",@"Remaining Budget:",@"Remaining Days:", nil];
    NSMutableArray *valueArr    =   [[NSMutableArray alloc]initWithObjects:[NSString stringWithFormat:@"%d",weddingBudget],[NSString stringWithFormat:@"%d",spentToDate],[NSString stringWithFormat:@"%d",remainingBudget],[NSString stringWithFormat:@"%d",1], nil];
    for (int i=0; i<4; i++) {
        UILabel *aTitleLbl   =   [[UILabel alloc]initWithFrame:CGRectMake(leftMargin, topMargin, SCREEN_WIDTH*0.40, SCREEN_HEIGHT*0.06)];
        aTitleLbl.backgroundColor    =   [UIColor clearColor];
        aTitleLbl.text               =   [titleArr objectAtIndex:i];
        aTitleLbl.textColor          =   [UIColor whiteColor];
        aTitleLbl.textAlignment      =   NSTextAlignmentLeft;
        aTitleLbl.font               =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.04];
        [contentScroll addSubview:aTitleLbl];
        
        UILabel *aValueLbl   =   [[UILabel alloc]initWithFrame:CGRectMake(leftMargin+SCREEN_WIDTH*0.45, topMargin, SCREEN_WIDTH*0.40, SCREEN_HEIGHT*0.06)];
        aValueLbl.backgroundColor    =   [UIColor clearColor];
        aValueLbl.text               =   [NSString stringWithFormat:@"£ %@",[valueArr objectAtIndex:i]];
        if (i==3) {
            [aValueLbl setText:@"Today"];
            NSString *weddingDate       =   [userDefault valueForKey:KEY_WEDDING_DAY];
            if (weddingDate.length > 0 ) {
                [aValueLbl setText:[common dayCountBetweenDateAndCurrentDate:weddingDate]];
            }
        }
        aValueLbl.textColor          =   [UIColor whiteColor];
        aValueLbl.textAlignment      =   NSTextAlignmentLeft;
        aValueLbl.font               =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.04];
        [contentScroll addSubview:aValueLbl];
        topMargin   +=  SCREEN_HEIGHT*0.06;
    }
    
    NSArray *expenceTitleArr   =   [[NSArray alloc]initWithObjects:@"Transportation",@"Ceremony",@"Flower",@"Photography",@"Wedding Attire",@"Everything Else",@"Reception", nil];
    
    NSArray *expenceValueArr   =   [[NSArray alloc]initWithObjects:[NSString stringWithFormat:@"%d",transportationExpence],[NSString stringWithFormat:@"%d",ceremonyExpence],[NSString stringWithFormat:@"%d",flowerExpence],[NSString stringWithFormat:@"%d",photographyExpence],[NSString stringWithFormat:@"%d",attireExpence],[NSString stringWithFormat:@"%d",otherExpence],[NSString stringWithFormat:@"%d",receptionExpence], nil];
    
    for (int i=0; i<7; i++) {
        
        UIImageView *bgV    =   [[UIImageView alloc]initWithFrame:CGRectMake(leftMargin, topMargin, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.05)];
        bgV.backgroundColor =   [UIColor blackColor];
        bgV.alpha           =   0.6;
        [contentScroll addSubview:bgV];
        
        UILabel *aTitleLbl   =   [[UILabel alloc]initWithFrame:CGRectMake(leftMargin, topMargin, SCREEN_WIDTH*0.40, SCREEN_HEIGHT*0.05)];
        aTitleLbl.backgroundColor       =   [UIColor clearColor];
        aTitleLbl.text                  =   [expenceTitleArr objectAtIndex:i];
        aTitleLbl.textColor             =   [UIColor whiteColor];
        aTitleLbl.textAlignment         =   NSTextAlignmentLeft;
        aTitleLbl.font                  =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.04];
        [contentScroll addSubview:aTitleLbl];
        
        UILabel *currencyLbl            =   [[UILabel alloc]initWithFrame:CGRectMake(leftMargin+SCREEN_WIDTH*0.45, topMargin, SCREEN_WIDTH*0.02, SCREEN_HEIGHT*0.05)];
        currencyLbl.backgroundColor     =   [UIColor clearColor];
        currencyLbl.text                =   @"£ ";
        currencyLbl.textColor           =   [UIColor whiteColor];
        currencyLbl.textAlignment       =   NSTextAlignmentLeft;
        currencyLbl.font                =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.04];
        [contentScroll addSubview:currencyLbl];
        
        UITextField *aValueTxt          =   [[UITextField alloc]initWithFrame:CGRectMake(leftMargin+SCREEN_WIDTH*0.48, topMargin, SCREEN_WIDTH*0.40, SCREEN_HEIGHT*0.05)];
        aValueTxt.backgroundColor       =   [UIColor clearColor];
        aValueTxt.text                  =   [NSString stringWithFormat:@"%@",[expenceValueArr objectAtIndex:i]];
        aValueTxt.textColor             =   [UIColor whiteColor];
        aValueTxt.textAlignment         =   NSTextAlignmentLeft;
        aValueTxt.inputAccessoryView    =   toolBar;
        aValueTxt.delegate              =   self;
        aValueTxt.tag                   =   i+1;
        aValueTxt.keyboardType          =   UIKeyboardTypeNumberPad;
        aValueTxt.font                  =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.04];
        [contentScroll addSubview:aValueTxt];
        
        topMargin   +=  SCREEN_HEIGHT*0.06;
    }
    
    UIButton *totalBtn  =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.40 , topMargin , SCREEN_WIDTH*0.55, SCREEN_HEIGHT*0.07)];
    [totalBtn setBackgroundImage:[UIImage imageNamed:BUTTON_IMAGE] forState:UIControlStateNormal];
    [totalBtn setTitle:@"Total" forState:UIControlStateNormal];
    [totalBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    totalBtn.titleLabel.font    =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.05];
    [totalBtn addTarget:self action:@selector(doTotal) forControlEvents:UIControlEventTouchUpInside];
    [contentScroll addSubview:totalBtn];
}

#pragma mark Method to be called when done button is tapped on tool bar of keyboard
-(void)doneBtnClicked{
    [self.view endEditing:YES];
}

#pragma mark AlertView Delegate method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==1) {
        NSString *budget = [alertView textFieldAtIndex:0].text;
        [userDefault setInteger:[budget integerValue] forKey:KEY_WEDDING_BUDGET];
        [userDefault setInteger:1 forKey:KEY_IS_BUDGET_SET];
        [userDefault synchronize];
        [self manuallViewMaking];
    }
}

#pragma mark TextField delegate methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string  {
    
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARECTERS] invertedSet];
    
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
    return [string isEqualToString:filtered];
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    CGRect Frame    =   contentScroll.frame;
    if(textField.tag >= 5){
        Frame= CGRectMake(contentScroll.frame.origin.x, -SCREEN_HEIGHT*0.25, contentScroll.frame.size.width, contentScroll.frame.size.height);
    }
    else if(textField.tag >=2){
        Frame= CGRectMake(contentScroll.frame.origin.x, -SCREEN_HEIGHT*0.15, contentScroll.frame.size.width, contentScroll.frame.size.height);
    }
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    
    contentScroll.frame =   Frame;
    
    [UIView commitAnimations];
}

#pragma mark Method to be called when keyboard hide
-(void)keyboardDidHide:(NSNotification *)notification{
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    
    contentScroll.frame    =   CGRectMake(0, SCREEN_HEIGHT*0.16, SCREEN_WIDTH, SCREEN_HEIGHT*0.84);
    
    [UIView commitAnimations];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
