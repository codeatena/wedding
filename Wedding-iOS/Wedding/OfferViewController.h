//
//  OfferViewController.h
//  Wedding
//
//  Created by apple on 07/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>

@interface OfferViewController : UIViewController<UIActionSheetDelegate,MFMailComposeViewControllerDelegate>

@end
