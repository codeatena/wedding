//
//  GiftListViewController.h
//  Wedding
//
//  Created by apple on 09/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GiftListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,NSURLConnectionDelegate,NSURLConnectionDataDelegate>{
    UITableView             *tableV;
    NSMutableData           *responseData;
    NSURLRequest            *request;
    NSURLConnection         *connect;
    NSMutableData           *responseData2;
    NSURLRequest            *request2;
    NSURLConnection         *connect2;
    NSMutableArray          *dataArray;
    NSUserDefaults          *userDefaults;
    int                     giftIdToDelete;
}

@end
