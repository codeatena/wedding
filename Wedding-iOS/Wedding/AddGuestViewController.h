//
//  AddGuestViewController.h
//  Wedding
//
//  Created by apple on 09/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddGuestViewController : UIViewController<NSURLConnectionDelegate,NSURLConnectionDataDelegate,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate>{
    
    UIScrollView            *contentScroll;
    NSMutableData           *responseData;
    NSURLRequest            *request;
    NSURLConnection         *connect;
    NSUserDefaults          *userDefault;
    UIPickerView            *pickerViewObj;
    NSMutableArray          *pickerViewArrayObj;
    NSMutableArray          *familyArrObj;
    NSMutableArray          *guestTypeArrObj;
    UIToolbar               *toolBar;
    UIImageView             *profileImgV;
    UIButton                *favBtn;
    int                     isFav;
}

@end
