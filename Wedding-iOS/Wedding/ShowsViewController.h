//
//  ShowsViewController.h
//  Wedding
//
//  Created by apple on 07/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,NSURLConnectionDelegate,NSURLConnectionDataDelegate>{
    UITableView             *tableV;
    NSMutableData           *responseData;
    NSURLRequest            *request;
    NSURLConnection         *connect;
    NSMutableArray          *dataArray;
}

@end
