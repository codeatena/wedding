//
//  GuestListViewController.h
//  Wedding
//
//  Created by apple on 09/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GuestListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,NSURLConnectionDelegate,NSURLConnectionDataDelegate,UIPickerViewDelegate,UIPickerViewDataSource>{
    
    UILabel                 *countLbl;
    UIButton                *filterBtn;
    UITableView             *tableV;
    NSMutableData           *responseData;
    NSURLRequest            *request;
    NSURLConnection         *connect;
    NSMutableData           *responseData2;
    NSURLRequest            *request2;
    NSURLConnection         *connect2;
    NSMutableArray          *dataArray;
    UIPickerView            *pickerViewObj;
    NSMutableArray          *pickerViewArrayObj;
    NSUserDefaults          *userDefaults;
    NSString                *filterText;
    NSMutableArray          *completeArr;
    int                     guestIdToAddGift;

}

@end
