//
//  TimetableViewController.m
//  Wedding
//
//  Created by apple on 09/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import "TimetableViewController.h"
#import "timeTblDetailsViewController.h"

@interface TimetableViewController ()

@end

@implementation TimetableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    dataArray = [[NSMutableArray alloc]init];
    
    NSMutableDictionary *dataDicOne = [[NSMutableDictionary alloc]init];
    [dataDicOne setValue:@"twelve_mnths.png" forKey:@"image"];
    [dataDicOne setValue:@"12-9 Months Before" forKey:@"title"];
    [dataDicOne setValue:@"Things to consider 12-9 months before the wedding." forKey:@"sub_title"];
    [dataArray addObject:dataDicOne];
    
    NSMutableDictionary *dataDicTwo = [[NSMutableDictionary alloc]init];
    [dataDicTwo setValue:@"nine_mnths.png" forKey:@"image"];
    [dataDicTwo setValue:@"9-6 Months Before" forKey:@"title"];
    [dataDicTwo setValue:@"From music to photographers, there is a lot to plan 6-9 months before the big day." forKey:@"sub_title"];
    [dataArray addObject:dataDicTwo];
    
    NSMutableDictionary *dataDicThree = [[NSMutableDictionary alloc]init];
    [dataDicThree setValue:@"six_mnths.png" forKey:@"image"];
    [dataDicThree setValue:@"6-3 Months Before" forKey:@"title"];
    [dataDicThree setValue:@"The closer the wedding the more things there are to think about." forKey:@"sub_title"];
    [dataArray addObject:dataDicThree];
    
    NSMutableDictionary *dataDicFour = [[NSMutableDictionary alloc]init];
    [dataDicFour setValue:@"three_mnths.png" forKey:@"image"];
    [dataDicFour setValue:@"3-2 Months Before" forKey:@"title"];
    [dataDicFour setValue:@"From seating plans to hen nights, there is a lot going on 3 months before the wedding." forKey:@"sub_title"];
    [dataArray addObject:dataDicFour];
    
    NSMutableDictionary *dataDicFive = [[NSMutableDictionary alloc]init];
    [dataDicFive setValue:@"one_mnths.png" forKey:@"image"];
    [dataDicFive setValue:@"1 Months Before" forKey:@"title"];
    [dataDicFive setValue:@"A list of things to plan one month before the wedding." forKey:@"sub_title"];
    [dataArray addObject:dataDicFive];
    
    NSMutableDictionary *dataDicSix = [[NSMutableDictionary alloc]init];
    [dataDicSix setValue:@"night_before_wedd.png" forKey:@"image"];
    [dataDicSix setValue:@"Night before the Wedding" forKey:@"title"];
    [dataDicSix setValue:@"The night before the wedding is a time a relax with friends and family." forKey:@"sub_title"];
    [dataArray addObject:dataDicSix];
    
    NSMutableDictionary *dataDicSeven = [[NSMutableDictionary alloc]init];
    [dataDicSeven setValue:@"wedding_day.png" forKey:@"image"];
    [dataDicSeven setValue:@"Wedding Day" forKey:@"title"];
    [dataDicSeven setValue:@"Follow this rough guide for your wedding day and everyting should go smoothly. " forKey:@"sub_title"];
    [dataArray addObject:dataDicSeven];
    
    
    
    UIImageView *bgImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    //    bgImageView.image           =   [UIImage imageNamed:WEDDING_TAB_BG];
    [bgImageView setBackgroundColor:[UIColor colorWithRed:241.0f/255.0f green:155.0f/255.0f blue:204.0f/255.0f alpha:1.0f]];
    [self.view addSubview:bgImageView];
    
    UIImageView *topImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT*0.08)];
    topImageView.image           =   [UIImage imageNamed:TOP_IMG];
    [self.view addSubview:topImageView];
    
    UIImageView *titleImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.08, SCREEN_WIDTH, SCREEN_HEIGHT*0.065)];
    [titleImageView setBackgroundColor:[UIColor colorWithRed:197.0f/255.0f green:105.0f/255.0f blue:130.0f/255.0f alpha:1.0f]];
    titleImageView.alpha            =   0.7;
    [self.view addSubview:titleImageView];
    
    UILabel *titleLbl           =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.10, SCREEN_HEIGHT*0.08, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.06)];
    titleLbl.backgroundColor    =   [UIColor clearColor];
    titleLbl.text               =   @"Wedding Timetable";
    titleLbl.textColor          =   [UIColor whiteColor];
    titleLbl.textAlignment      =   NSTextAlignmentLeft;
    titleLbl.font               =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.06];
    [self.view addSubview:titleLbl];
    
    UIButton *backBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.03, SCREEN_HEIGHT*0.09,SCREEN_WIDTH*0.05,SCREEN_HEIGHT*0.04)];
    [backBtn setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMG] forState:UIControlStateNormal];
    [backBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    
    UIButton *logoutBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.80, 0,SCREEN_WIDTH*0.20,SCREEN_HEIGHT*0.08)];
    [logoutBtn setBackgroundColor:[UIColor clearColor]];
    [logoutBtn setTitle:@"Logout" forState:UIControlStateNormal];
    [logoutBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    logoutBtn.titleLabel.font     =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.05];
    [logoutBtn addTarget:self action:@selector(logoutButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:logoutBtn];
    
    tableV                  =   [[UITableView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.167, SCREEN_WIDTH , SCREEN_HEIGHT*0.79)];
    tableV.dataSource       =   self;
    tableV.delegate         =   self;
    tableV.backgroundColor  =   [UIColor clearColor];
    //    tableV.separatorStyle   =   UITableViewCellSeparatorStyleNone;
    tableV.showsVerticalScrollIndicator =   NO;
    [tableV setSeparatorInset:UIEdgeInsetsZero];
    [tableV setSeparatorColor:[UIColor colorWithRed:(170/255.0f) green:(134/255.0f) blue:(96/255.0f) alpha:1]];
    [self.view addSubview:tableV];
    
    
}

#pragma mark Back Button Method
-(void)backButtonClicked{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark Method to be called when tap on logout button
-(void)logoutButtonClicked{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark Table view Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //    NSLog(@"SECTION :: %d",section);
    //    return dataArray.count;
    return dataArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return SCREEN_HEIGHT*0.12;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *unifiedID = [NSString stringWithFormat:@"aCellIDPh_%d_%d",indexPath.section,indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:unifiedID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:unifiedID];
        
        cell.selectionStyle =   UITableViewCellSelectionStyleNone;
        cell.backgroundColor    =   [UIColor clearColor];
        NSMutableDictionary *dataDic = [dataArray objectAtIndex:indexPath.row];
        UIImage *oldImage = [UIImage imageNamed:[dataDic valueForKey:@"image"]];
        CGSize newSize = CGSizeMake(SCREEN_WIDTH/5.33, SCREEN_HEIGHT/9.6);
        UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
        [oldImage drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        
        cell.imageView.image        =   newImage;
        cell.textLabel.text         =   [dataDic valueForKey:@"title"];
        cell.textLabel.font         =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.05];
        cell.detailTextLabel.text   =   [dataDic valueForKey:@"sub_title"];
        cell.detailTextLabel.font   =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.035];
        cell.detailTextLabel.numberOfLines  =   2;
    }
    return cell;
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //    [common showAlert:@"Info" _message:@"Comming Soon"];
    timeTblDetailsViewController    *vc =   [[timeTblDetailsViewController alloc]initWithData:indexPath.row];
    [self.navigationController pushViewController:vc animated:NO];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
