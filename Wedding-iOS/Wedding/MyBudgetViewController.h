//
//  MyBudgetViewController.h
//  Wedding
//
//  Created by apple on 08/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyBudgetViewController : UIViewController<UITextFieldDelegate>{
    NSUserDefaults      *userDefault;
    int                 weddingBudget;
    int                 transportationExpence;
    int                 ceremonyExpence;
    int                 flowerExpence;
    int                 photographyExpence;
    int                 attireExpence;
    int                 otherExpence;
    int                 receptionExpence;
    int                 spentToDate;
    int                 remainingBudget;
    UIToolbar           *toolBar;
    UIScrollView        *contentScroll;
}

@end
