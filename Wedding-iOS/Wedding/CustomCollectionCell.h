//
//  CustomCollectionCell.h
//  Wedding
//
//  Created by apple on 07/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCollectionCell : UICollectionViewCell{
}

@property(nonatomic, retain) UIImageView *imageV;
@property(nonatomic, retain) UILabel *nameLbl;

@end
