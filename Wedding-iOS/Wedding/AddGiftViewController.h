//
//  AddGiftViewController.h
//  Wedding
//
//  Created by apple on 09/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddGiftViewController : UIViewController<NSURLConnectionDelegate,NSURLConnectionDataDelegate,UITextFieldDelegate>{
    NSMutableData           *responseData;
    NSURLRequest            *request;
    NSURLConnection         *connect;
    UITextField             *nameTxt;
    UITextField             *giftTxt;
}

@end
