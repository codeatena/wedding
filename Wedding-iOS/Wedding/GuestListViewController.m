//
//  GuestListViewController.m
//  Wedding
//
//  Created by apple on 09/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import "GuestListViewController.h"
#import "AddGuestViewController.h"

@interface GuestListViewController ()

@end

@implementation GuestListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self loadGuestList];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    userDefaults                =   [NSUserDefaults standardUserDefaults];
    
    completeArr                 =   [[NSMutableArray alloc]init];
    dataArray                   =   [[NSMutableArray alloc]init];
    
    guestIdToAddGift            =   0;
    
    UIImageView *bgImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    bgImageView.image           =   [UIImage imageNamed:GUEST_LIST_BG_IMG];
    [self.view addSubview:bgImageView];
    
    UIImageView *topImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT*0.08)];
    topImageView.image           =   [UIImage imageNamed:TOP_IMG];
    [self.view addSubview:topImageView];
    
    UIImageView *titleImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.08, SCREEN_WIDTH, SCREEN_HEIGHT*0.065)];
    [titleImageView setBackgroundColor:[UIColor colorWithRed:197.0f/255.0f green:105.0f/255.0f blue:130.0f/255.0f alpha:1.0f]];
    titleImageView.alpha            =   0.7;
    [self.view addSubview:titleImageView];
    
    UILabel *titleLbl           =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.10, SCREEN_HEIGHT*0.08, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.06)];
    titleLbl.backgroundColor    =   [UIColor clearColor];
    titleLbl.text               =   @"Guest List";
    titleLbl.textColor          =   [UIColor whiteColor];
    titleLbl.textAlignment      =   NSTextAlignmentLeft;
    titleLbl.font               =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.06];
    [self.view addSubview:titleLbl];
    
    UIButton *backBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.03, SCREEN_HEIGHT*0.09,SCREEN_WIDTH*0.05,SCREEN_HEIGHT*0.04)];
    [backBtn setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMG] forState:UIControlStateNormal];
    [backBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    
    UIButton *logoutBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.80, 0,SCREEN_WIDTH*0.20,SCREEN_HEIGHT*0.08)];
    [logoutBtn setBackgroundColor:[UIColor clearColor]];
    [logoutBtn setTitle:@"Logout" forState:UIControlStateNormal];
    [logoutBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    logoutBtn.titleLabel.font     =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.05];
    [logoutBtn addTarget:self action:@selector(logoutButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:logoutBtn];
    
    UIButton *addGuestBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.70, SCREEN_HEIGHT*0.08,SCREEN_WIDTH*0.30,SCREEN_HEIGHT*0.065)];
    [addGuestBtn setBackgroundColor:[UIColor clearColor]];
    [addGuestBtn setBackgroundImage:[UIImage imageNamed:ADD_BUTTON_IMAGE] forState:UIControlStateNormal];
    [addGuestBtn setTitle:@"Add Guest" forState:UIControlStateNormal];
    addGuestBtn.titleLabel.font     =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.05];
    [addGuestBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [addGuestBtn addTarget:self action:@selector(doAddGuest) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:addGuestBtn];
    
    UIImageView *blackBG        =   [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.03, SCREEN_HEIGHT*0.16, SCREEN_WIDTH*0.94, SCREEN_HEIGHT*0.82)];
    [blackBG setBackgroundColor:[UIColor blackColor]];
    blackBG.alpha               =   0.6;
    [self.view addSubview:blackBG];
    
    countLbl           =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.15, SCREEN_HEIGHT*0.16, SCREEN_WIDTH*0.25, SCREEN_HEIGHT*0.05)];
    countLbl.backgroundColor    =   [UIColor clearColor];
    countLbl.text               =   @"(0/3)";
    countLbl.textColor          =   [UIColor whiteColor];
    countLbl.textAlignment      =   NSTextAlignmentLeft;
    countLbl.font               =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.045];
    [self.view addSubview:countLbl];
    
    filterBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.50, SCREEN_HEIGHT*0.16,SCREEN_WIDTH*0.45,SCREEN_HEIGHT*0.065)];
    [filterBtn setBackgroundColor:[UIColor clearColor]];
    [filterBtn setBackgroundImage:[UIImage imageNamed:@"button_pink4.png"] forState:UIControlStateNormal];
    [filterBtn setTitle:@"Day Guest" forState:UIControlStateNormal];
    filterBtn.titleLabel.font     =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.05];
    [filterBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [filterBtn addTarget:self action:@selector(showFilterOption) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:filterBtn];
    
    UIImageView *dropDownImg    =   [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.38, SCREEN_HEIGHT*0.02, SCREEN_WIDTH*0.05, SCREEN_WIDTH*0.04)];
    dropDownImg.image           =   [UIImage imageNamed:DROP_DOWN_PNG];
    [filterBtn addSubview:dropDownImg];
    
    
    tableV                  =   [[UITableView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.03, SCREEN_HEIGHT*0.22, SCREEN_WIDTH*0.94, SCREEN_HEIGHT*0.76)];
    tableV.dataSource       =   self;
    tableV.delegate         =   self;
    tableV.backgroundColor  =   [UIColor clearColor];
    //    tableV.separatorStyle   =   UITableViewCellSeparatorStyleNone;
    tableV.showsVerticalScrollIndicator =   NO;
    [self.view addSubview:tableV];
    
    pickerViewArrayObj  =   [[NSMutableArray alloc]initWithObjects:@"Day Guest",@"Evening Guest",@"Both", nil];
    
    pickerViewObj = [[UIPickerView alloc] initWithFrame:CGRectMake(0,SCREEN_HEIGHT*0.70,SCREEN_WIDTH,SCREEN_HEIGHT*0.30)];
    pickerViewObj.showsSelectionIndicator = YES;
    pickerViewObj.backgroundColor   =   [UIColor whiteColor];
    pickerViewObj.hidden = YES;
    pickerViewObj.delegate = self;
    pickerViewObj.dataSource = self;
    [self.view addSubview:pickerViewObj];
    
    filterText  =   [pickerViewArrayObj objectAtIndex:0];
    
    [self loadGuestList];
}

#pragma mark Back Button Method
-(void)backButtonClicked{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark Method to be called when tap on logout button
-(void)logoutButtonClicked{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

#pragma mark Method to add guest
-(void)doAddGuest{
    AddGuestViewController *guestAddVC  =   [[AddGuestViewController alloc]init];
    [self.navigationController pushViewController:guestAddVC animated:NO];
    
}

#pragma mark Method to show filter option
-(void)showFilterOption{
    [pickerViewObj setHidden:NO];
}

#pragma mark Method to load Guest list
-(void)loadGuestList{
    if([common reachabilityFunction]) {
        NSString *urlStr = [NSString stringWithFormat:@"%@get_guest.php?user_id=%d",API_URL,[[userDefaults valueForKey:@"userId"] integerValue]];
        
        urlStr  =   [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        responseData   =   [[NSMutableData alloc]init];
        
        request        =   [NSURLRequest requestWithURL:[NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] cachePolicy:NSURLCacheStorageAllowed timeoutInterval:5];
        
        connect        =   [[NSURLConnection alloc] initWithRequest:request delegate:self];
        
        [connect start];
    }else{
        [common showAlert:@"Warning" _message:@"No internet connection."];
    }
}

#pragma mark Method to add gift to guest
-(void)addGift:(NSString *)giftName{
    if([common reachabilityFunction]) {
        NSString *urlStr = [NSString stringWithFormat:@"%@add_grift_user.php?user_id=%d&guest_id=%d&grift=%@",API_URL,[[userDefaults valueForKey:@"userId"] integerValue],guestIdToAddGift,giftName];
        
        urlStr  =   [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        responseData2   =   [[NSMutableData alloc]init];
        
        request2        =   [NSURLRequest requestWithURL:[NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] cachePolicy:NSURLCacheStorageAllowed timeoutInterval:5];
        
        connect2        =   [[NSURLConnection alloc] initWithRequest:request2 delegate:self];
        
        [connect2 start];
    }else{
        [common showAlert:@"Warning" _message:@"No internet connection."];
    }
}

#pragma mark Table view Methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //    NSLog(@"SECTION :: %d",section);
    return dataArray.count;
    //    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return SCREEN_HEIGHT*0.12;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *unifiedID = [NSString stringWithFormat:@"aCellIDPh_%d_%d",indexPath.section,indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:unifiedID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:unifiedID];
        
        cell.selectionStyle =   UITableViewCellSelectionStyleNone;
        cell.backgroundColor    =   [UIColor clearColor];
        
        UIImageView *imgV   =   [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.01, SCREEN_HEIGHT*0.10, SCREEN_HEIGHT*0.10)];
        imgV.contentMode = UIViewContentModeScaleAspectFit;
        //        [imgV sd_setImageWithURL:[NSURL URLWithString:[[dataArray objectAtIndex:indexPath.row] objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_NAME]];
        [imgV setImage:[UIImage imageNamed:STAR_WHITE_IMG]];
        if ([[[dataArray objectAtIndex:indexPath.row] objectForKey:@"favouite"] isEqualToString:@"favouite"]) {
            [imgV setImage:[UIImage imageNamed:STAR_GOLDEN_IMG]];
        }
        [cell.contentView addSubview:imgV];
        
        UILabel *lbl    =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_HEIGHT*0.12, SCREEN_HEIGHT*0.02, SCREEN_WIDTH*0.30, SCREEN_HEIGHT*0.08)];
        [lbl setBackgroundColor:[UIColor clearColor]];
        [lbl setTextColor:[UIColor whiteColor]];
        [lbl setTextAlignment:NSTextAlignmentLeft];
        lbl.numberOfLines   =   0;
        lbl.lineBreakMode   =   NSLineBreakByWordWrapping;
        lbl.font    =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.04];
        //        lbl.text =   [NSString stringWithFormat:@"%@\n%@",[[dataArray objectAtIndex:indexPath.row] objectForKey:@"suppler_name"],[[dataArray objectAtIndex:indexPath.row] objectForKey:@"city"]];
        lbl.text    =   @"Waiting";
        [cell.contentView addSubview:lbl];
        
        UILabel *lbl2    =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_HEIGHT*0.12+SCREEN_WIDTH*0.30, SCREEN_HEIGHT*0.02, SCREEN_WIDTH*0.30, SCREEN_HEIGHT*0.08)];
        [lbl2 setBackgroundColor:[UIColor clearColor]];
        [lbl2 setTextColor:[UIColor whiteColor]];
        [lbl2 setTextAlignment:NSTextAlignmentLeft];
        lbl2.numberOfLines   =   0;
        lbl2.lineBreakMode   =   NSLineBreakByWordWrapping;
        lbl2.font    =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.04];
        lbl2.text =   [NSString stringWithFormat:@"%@",[[dataArray objectAtIndex:indexPath.row] objectForKey:@"fistname"]];
        //        lbl2.text    =   @"ABC";
        [cell.contentView addSubview:lbl2];
        
        UIImageView *guestImgV   =   [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.70, SCREEN_HEIGHT*0.01, SCREEN_HEIGHT*0.10, SCREEN_HEIGHT*0.10)];
        guestImgV.contentMode = UIViewContentModeScaleAspectFill;
        //        guestImgV.image  =   [UIImage imageNamed:@"male.png"];
        [guestImgV sd_setImageWithURL:[NSURL URLWithString:[[dataArray objectAtIndex:indexPath.row] objectForKey:@"profile_pic"]] placeholderImage:[UIImage imageNamed:@"male.png"]];
        [cell.contentView addSubview:guestImgV];
        
        UIImageView *arrowImgV   =   [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.85, SCREEN_HEIGHT*0.045, SCREEN_HEIGHT*0.03, SCREEN_HEIGHT*0.04)];
        arrowImgV.image  =   [UIImage imageNamed:NEXT_BUTTON_IMG];
        [cell.contentView addSubview:arrowImgV];
        
    }
    return cell;
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //    [common showAlert:@"Info" _message:@"Comming Soon"];
    // Show alert view to enter budget
    
    guestIdToAddGift    =   [[[dataArray objectAtIndex:indexPath.row] valueForKey:@"id"] integerValue];
    
    UIAlertView *alerView = [[UIAlertView alloc]initWithTitle:@"Add Gift" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Cancel",@"Add", nil];
    alerView.alertViewStyle =   UIAlertViewStylePlainTextInput;
    UITextField *textField  =   [alerView textFieldAtIndex:0];
    //    textField.delegate      =   self;
    textField.placeholder   =   @"Gift";
    [alerView show];
}

#pragma mark AlertView Delegate method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==1) {
        NSString *giftName = [alertView textFieldAtIndex:0].text;
        [self addGift:giftName];
    }
}


#pragma mark Url connection delegate methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    //    NSLog(@"didReceiveResponse");
    
    if (connection==connect){
        [responseData setLength:0];
    }else if (connection==connect2){
        [responseData2 setLength:0];
    }
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    if(responseData==nil){
        [common showAlert:@"Error" _message:@"Server Problem"];
    }else if(responseData2==nil){
        [common showAlert:@"Error" _message:@"Server Problem"];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    if(connection==connect){
        [responseData appendData:data];
    }else if(connection==connect2){
        [responseData2 appendData:data];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    if (connection==connect){
        NSError* error;
        
        NSMutableDictionary *reviewsDict = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        
        completeArr    =   [[reviewsDict valueForKey:@"get_guest"] mutableCopy];
        
        [self addFilter];
        
        NSLog(@"DICT :: %@",reviewsDict);
        [tableV reloadData];
    }else if (connection==connect2){
        NSError* error;
        
        NSMutableDictionary *reviewsDict = [NSJSONSerialization JSONObjectWithData:responseData2 options:kNilOptions error:&error];
        
        if ([[reviewsDict valueForKey:@"success"] integerValue] == 1){
            [common showAlert:@"Info" _message:@"Gift Added"];
        }else{
            [common showAlert:@"Warning" _message:@"Oops! An error occurred."];
        }
        
    }
}

#pragma mark To filter data
-(void)addFilter{
    [dataArray removeAllObjects];
    if ([filterText isEqualToString:@"Both"]) {
        dataArray   =   [completeArr mutableCopy];
    }else if ([filterText isEqualToString:@"Day Guest"]) {
        for (int i=0; i<completeArr.count; i++) {
            NSLog(@"::: %@",[[completeArr objectAtIndex:i] valueForKey:@"guest_option"]);
            if ([@"DayGuest" isEqualToString:[[completeArr objectAtIndex:i] valueForKey:@"guest_option"] ]) {
                [dataArray addObject:[completeArr objectAtIndex:i]];
            }
        }
    }else{
        for (int i=0; i<completeArr.count; i++) {
            if (![@"DayGuest" isEqualToString:[[completeArr objectAtIndex:i] valueForKey:@"guest_option"] ]) {
                [dataArray addObject:[completeArr objectAtIndex:i]];
            }
        }
    }
    
    [tableV reloadData];
}

#pragma mark Picker view delegate method
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return pickerViewArrayObj.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    return [pickerViewArrayObj objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    [filterBtn setTitle:[pickerViewArrayObj objectAtIndex:row] forState:UIControlStateNormal];
    filterText  =   [pickerViewArrayObj objectAtIndex:row];
    [self addFilter];
    [pickerViewObj setHidden:YES];
    //    [tableV reloadData];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
