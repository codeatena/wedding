//
//  AddGiftViewController.m
//  Wedding
//
//  Created by apple on 09/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import "AddGiftViewController.h"

@interface AddGiftViewController ()

@end

@implementation AddGiftViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIImageView *bgImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    bgImageView.image           =   [UIImage imageNamed:ADD_GIFT_BG_IMG];
    [self.view addSubview:bgImageView];
    
    UIImageView *topImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT*0.08)];
    topImageView.image           =   [UIImage imageNamed:TOP_IMG];
    [self.view addSubview:topImageView];
    
    UIImageView *titleImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.08, SCREEN_WIDTH, SCREEN_HEIGHT*0.065)];
    [titleImageView setBackgroundColor:[UIColor colorWithRed:197.0f/255.0f green:105.0f/255.0f blue:130.0f/255.0f alpha:1.0f]];
    titleImageView.alpha            =   0.7;
    [self.view addSubview:titleImageView];
    
    UILabel *titleLbl           =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.10, SCREEN_HEIGHT*0.08, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.06)];
    titleLbl.backgroundColor    =   [UIColor clearColor];
    titleLbl.text               =   @"Add Gift";
    titleLbl.textColor          =   [UIColor whiteColor];
    titleLbl.textAlignment      =   NSTextAlignmentLeft;
    titleLbl.font               =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.06];
    [self.view addSubview:titleLbl];
    
    UIButton *backBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.03, SCREEN_HEIGHT*0.09,SCREEN_WIDTH*0.05,SCREEN_HEIGHT*0.04)];
    [backBtn setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMG] forState:UIControlStateNormal];
    [backBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    
    UIButton *logoutBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.80, 0,SCREEN_WIDTH*0.20,SCREEN_HEIGHT*0.08)];
    [logoutBtn setBackgroundColor:[UIColor clearColor]];
    [logoutBtn setTitle:@"Logout" forState:UIControlStateNormal];
    [logoutBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    logoutBtn.titleLabel.font     =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.05];
    [logoutBtn addTarget:self action:@selector(logoutButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:logoutBtn];
    
    //    UIImageView *blackBG        =   [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.03, SCREEN_HEIGHT*0.16, SCREEN_WIDTH*0.94, SCREEN_HEIGHT*0.82)];
    //    [blackBG setBackgroundColor:[UIColor blackColor]];
    //    blackBG.alpha               =   0.6;
    //    [self.view addSubview:blackBG];
    
    nameTxt       =   [[UITextField alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.10, SCREEN_HEIGHT*0.22, SCREEN_WIDTH*0.80, SCREEN_HEIGHT*0.06)];
    nameTxt.placeholder        =   @"First Name";
    nameTxt.delegate           =   self;
    nameTxt.backgroundColor    =   [UIColor blackColor];
    nameTxt.layer.cornerRadius =   22;
    nameTxt.font               =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.05];
    nameTxt.alpha              =   0.6;
    nameTxt.textColor          =   [UIColor whiteColor];
    // For adding padding in text field
    UIView *paddingView1        =   [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH*0.04, 20)];
    nameTxt.leftView           =   paddingView1;
    nameTxt.leftViewMode       =   UITextFieldViewModeAlways;
    [nameTxt setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.view addSubview:nameTxt];
    
    giftTxt       =   [[UITextField alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.10, SCREEN_HEIGHT*0.30, SCREEN_WIDTH*0.80, SCREEN_HEIGHT*0.06)];
    giftTxt.placeholder        =   @"Gift";
    giftTxt.delegate           =   self;
    giftTxt.backgroundColor    =   [UIColor blackColor];
    giftTxt.layer.cornerRadius =   22;
    giftTxt.font               =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.05];
    giftTxt.alpha              =   0.6;
    giftTxt.textColor          =   [UIColor whiteColor];
    // For adding padding in text field
    UIView *paddingView        =   [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH*0.04, 20)];
    giftTxt.leftView           =   paddingView;
    giftTxt.leftViewMode       =   UITextFieldViewModeAlways;
    [giftTxt setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    [self.view addSubview:giftTxt];
    
    
    
    UIButton *addBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.60, SCREEN_HEIGHT*0.40, SCREEN_WIDTH*0.30, SCREEN_HEIGHT*0.08)];
    [addBtn setBackgroundImage:[UIImage imageNamed:BUTTON_IMAGE] forState:UIControlStateNormal];
    [addBtn setTitle:@"Add Gift" forState:UIControlStateNormal];
    [addBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    addBtn.titleLabel.font   =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.055];
    [addBtn addTarget:self action:@selector(addGift) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:addBtn];
    
    
}

#pragma mark Back Button Method
-(void)backButtonClicked{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark Method to be called when tap on logout button
-(void)logoutButtonClicked{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

#pragma mark Method to add gift
-(void)addGift{
    [common showAlert:@"Info" _message:@"Comming Soon"];
    //    NSString *urlStr = [NSString stringWithFormat:@"%@get_suppler.php?cate_id=%d",API_URL,1];
    //    urlStr  =   [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //    responseData   =   [[NSMutableData alloc]init];
    //
    //    request        =   [NSURLRequest requestWithURL:[NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] cachePolicy:NSURLCacheStorageAllowed timeoutInterval:5];
    //
    //    connect        =   [[NSURLConnection alloc] initWithRequest:request delegate:self];
    //
    //    [connect start];
}

#pragma mark TextField delegate methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark Url connection delegate methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    //    NSLog(@"didReceiveResponse");
    
    if (connection==connect){
        // Login
        [responseData setLength:0];
    }
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    //    NSLog(@"didFailWithError");
    
    if(responseData==nil){
        [common showAlert:@"Error" _message:@"Server Problem"];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    if(connection==connect){
        [responseData appendData:data];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    if (connection==connect){
        NSError* error;
        
        NSMutableDictionary *reviewsDict = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        
        //        NSLog(@"DICT :: %@",reviewsDict);
        if ([[reviewsDict valueForKey:@"success"] integerValue] == 1){
            [common showAlert:@"Info" _message:@"Your Password Has Been Sent To Your Email Address."];
        }else{
            [common showAlert:@"Warning" _message:@"Oops! An error occurred."];
        }
        
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
