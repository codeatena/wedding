//
//  common.m
//  Wedding
//
//  Created by apple on 07/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import "common.h"

@implementation common

#pragma mark Reachability Method
+(BOOL)reachabilityFunction{
    
    Reachability* wifiReach = [Reachability reachabilityWithHostname:@"www.google.com"] ;
    NetworkStatus netStatus = [wifiReach currentReachabilityStatus];
    
    switch (netStatus)
    {
        case NotReachable:
        {
            NSLog(@"Access Not Available");
            return FALSE;
            
            break;
        }
            
        case ReachableViaWWAN:
        {
            NSLog(@"Reachable WWAN");
            return TRUE;
            break;
        }
        case ReachableViaWiFi:
        {
            return TRUE;
            NSLog(@"Reachable WiFi");
            break;
        }
    }
    
    
}

+(void)showAlert:(NSString *)title _message:(NSString *)message{
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

+(BOOL) validateEmail: (NSString *) email{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL isValid = [emailTest evaluateWithObject:email];
    return isValid;
}

+(NSString *)dayCountBetweenDateAndCurrentDate:(NSString *)date{
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"dd-MM-yyyy"];
    NSDateFormatter *f2 = [[NSDateFormatter alloc] init];
    [f2 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *startDate   =   [f dateFromString:date];
    NSDate *currentDate =   [NSDate date];
    NSString *end       =   [f stringFromDate:currentDate];
    NSDate *endDate     =   [f dateFromString:end];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
    int dayCnt  =   [components day];
    if (dayCnt < 0) {
        dayCnt  =   -dayCnt;
    }
    NSString *dayCount  =   [NSString stringWithFormat:@"%d",dayCnt];
    
    return dayCount;
    
}

@end
