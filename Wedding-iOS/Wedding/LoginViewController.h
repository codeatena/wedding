//
//  LoginViewController.h
//  Wedding
//
//  Created by apple on 07/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<UITextFieldDelegate,NSURLConnectionDelegate,NSURLConnectionDataDelegate>{
    UITextField             *emailTxt;
    UITextField             *passwordTxt;
    NSMutableData           *responseData;
    NSURLRequest            *request;
    NSURLConnection         *connect;
    // For forgot password
    NSMutableData           *responseData2;
    NSURLRequest            *request2;
    NSURLConnection         *connect2;
    //
    NSUserDefaults          *userDefault;
    int                     isRememberMe;
}

@property (strong, nonatomic) UITabBarController *tabBarController;

@end
