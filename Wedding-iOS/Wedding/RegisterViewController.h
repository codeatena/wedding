//
//  RegisterViewController.h
//  Wedding
//
//  Created by apple on 07/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController<UITextFieldDelegate,NSURLConnectionDelegate,NSURLConnectionDataDelegate>{
    NSArray                 *optionTitleArr;
    NSMutableData           *responseData;
    NSURLRequest            *request;
    NSURLConnection         *connect;
    NSUserDefaults          *userDefault;
    int                     isTnCAccepted;
    UIScrollView            *contentScroll;
}

@end
