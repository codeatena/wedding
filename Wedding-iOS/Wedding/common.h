//
//  common.h
//  Wedding
//
//  Created by apple on 07/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface common : NSObject

+(BOOL)reachabilityFunction;
+(void)showAlert:(NSString *)title _message:(NSString *)message;
+(BOOL) validateEmail: (NSString *) email;
+(NSString *)dayCountBetweenDateAndCurrentDate:(NSString *)date;

@end
