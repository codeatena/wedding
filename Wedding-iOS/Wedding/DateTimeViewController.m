//
//  DateTimeViewController.m
//  Wedding
//
//  Created by apple on 09/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import "DateTimeViewController.h"

@interface DateTimeViewController (){
    UIDatePicker *datePickerObj;
}

@end

@implementation DateTimeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    userDefaults                =   [NSUserDefaults standardUserDefaults];
    
    UIImageView *bgImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    //    bgImageView.image           =   [UIImage imageNamed:WEDDING_TAB_BG];
    [bgImageView setBackgroundColor:[UIColor colorWithRed:241.0f/255.0f green:155.0f/255.0f blue:204.0f/255.0f alpha:1.0f]];
    [self.view addSubview:bgImageView];
    
    UIImageView *topImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT*0.08)];
    topImageView.image           =   [UIImage imageNamed:TOP_IMG];
    [self.view addSubview:topImageView];
    
    UIImageView *titleImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.08, SCREEN_WIDTH, SCREEN_HEIGHT*0.065)];
    [titleImageView setBackgroundColor:[UIColor colorWithRed:197.0f/255.0f green:105.0f/255.0f blue:130.0f/255.0f alpha:1.0f]];
    titleImageView.alpha            =   0.7;
    [self.view addSubview:titleImageView];
    
    UILabel *titleLbl           =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.10, SCREEN_HEIGHT*0.08, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.06)];
    titleLbl.backgroundColor    =   [UIColor clearColor];
    titleLbl.text               =   @"Wedding Dates & Time";
    titleLbl.textColor          =   [UIColor whiteColor];
    titleLbl.textAlignment      =   NSTextAlignmentLeft;
    titleLbl.font               =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.06];
    [self.view addSubview:titleLbl];
    
    UIButton *backBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.03, SCREEN_HEIGHT*0.09,SCREEN_WIDTH*0.05,SCREEN_HEIGHT*0.04)];
    [backBtn setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMG] forState:UIControlStateNormal];
    [backBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    
    UIButton *logoutBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.80, 0,SCREEN_WIDTH*0.20,SCREEN_HEIGHT*0.08)];
    [logoutBtn setBackgroundColor:[UIColor clearColor]];
    [logoutBtn setTitle:@"Logout" forState:UIControlStateNormal];
    [logoutBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    logoutBtn.titleLabel.font     =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.05];
    [logoutBtn addTarget:self action:@selector(logoutButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:logoutBtn];
    
    [self manuallyViewMaking];
}

-(void)manuallyViewMaking{
    [self weddingViewMaking:SCREEN_HEIGHT/6];
    [self receptionViewMaking:SCREEN_HEIGHT/2.4];
    
    datePickerObj = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, 300, 200)];
    datePickerObj.hidden = YES;
    [datePickerObj addTarget:self action:@selector(datePickerChangeDate:) forControlEvents:UIControlEventValueChanged];
    datePickerObj.datePickerMode = UIDatePickerModeDate;
    datePickerObj.minimumDate = [NSDate date];
    [self.view addSubview:datePickerObj];
    
    UIButton *submitButton = [[UIButton alloc]init];
    submitButton.frame = CGRectMake(SCREEN_WIDTH/2.56, SCREEN_HEIGHT/1.5, SCREEN_WIDTH/4.5714, SCREEN_HEIGHT/19.2);
    [submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [submitButton setTitle:@"Submit" forState:UIControlStateNormal];
    [submitButton addTarget:self action:@selector(submitButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    submitButton.backgroundColor = [UIColor colorWithRed:(217/255.0f) green:(165/255.0f) blue:(188/255.0f) alpha:1];
    submitButton.titleLabel.font  =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.04];
    [self.view addSubview:submitButton];
    
    
}
-(void)weddingViewMaking : (float)height {
    
    UIView *firstBackView = [[UIView alloc]init];
    firstBackView.frame = CGRectMake(0, height, SCREEN_WIDTH, SCREEN_HEIGHT/4);
    firstBackView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:firstBackView];
    
    
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT/20);
    [self lablecustimizing:titleLabel title:@"Wedding Day" alignment: NSTextAlignmentCenter FontSize :14];
    [firstBackView addSubview:titleLabel];
    
    UILabel *weddingTitleDate = [[UILabel alloc]init];
    weddingTitleDate.frame = CGRectMake(SCREEN_WIDTH/10.67, SCREEN_HEIGHT/16, SCREEN_WIDTH/3.2, SCREEN_HEIGHT/20);
    [self lablecustimizing:weddingTitleDate title:@"Date:" alignment: NSTextAlignmentLeft   FontSize :12];
    [firstBackView addSubview:weddingTitleDate];
    
    UITextField *weddingDateTextF = [[UITextField alloc]init];
    weddingDateTextF.frame = CGRectMake(SCREEN_WIDTH/2.46, SCREEN_HEIGHT/16, SCREEN_WIDTH/2.13, SCREEN_HEIGHT/20);
    [self textFieldCustomize:weddingDateTextF placeHolder: @"Date/month/year"];
    weddingDateTextF.tag =  51;
    weddingDateTextF.text   =   [userDefaults valueForKey:KEY_WEDDING_DAY];
    [firstBackView addSubview:weddingDateTextF];
    
    UILabel *weddingTitleTime = [[UILabel alloc]init];
    weddingTitleTime.frame = CGRectMake(SCREEN_WIDTH/10.67, SCREEN_HEIGHT/8.72, SCREEN_WIDTH/3.2, SCREEN_HEIGHT/20);
    [self lablecustimizing:weddingTitleTime title:@"Time:" alignment: NSTextAlignmentLeft   FontSize :12];
    [firstBackView addSubview:weddingTitleTime];
    
    UITextField *weddingTimeTextF = [[UITextField alloc]init];
    weddingTimeTextF.frame = CGRectMake(SCREEN_WIDTH/2.46, SCREEN_HEIGHT/8.72, SCREEN_WIDTH/2.13, SCREEN_HEIGHT/20);
    [self textFieldCustomize:weddingTimeTextF placeHolder: @"Time"];
    weddingTimeTextF.tag =52;
    weddingTimeTextF.text   =   [userDefaults valueForKey:KEY_WEDDING_TIME];
    [firstBackView addSubview:weddingTimeTextF];
    
    UILabel *weddingTitleVenue = [[UILabel alloc]init];
    weddingTitleVenue.frame = CGRectMake(SCREEN_WIDTH/10.67, SCREEN_HEIGHT/6, SCREEN_WIDTH/3.2, SCREEN_HEIGHT/20);
    [self lablecustimizing:weddingTitleVenue title:@"Venue:" alignment: NSTextAlignmentLeft   FontSize :12];
    [firstBackView addSubview:weddingTitleVenue];
    
    UITextField *weddingVenueTextF = [[UITextField alloc]init];
    weddingVenueTextF.frame = CGRectMake(SCREEN_WIDTH/2.46, SCREEN_HEIGHT/6, SCREEN_WIDTH/2.13, SCREEN_HEIGHT/20);
    [self textFieldCustomize:weddingVenueTextF placeHolder: @""];
    weddingVenueTextF.tag =53;
    weddingVenueTextF.layer.cornerRadius=8.0f;
    weddingVenueTextF.layer.borderColor= [UIColor whiteColor].CGColor;
    weddingVenueTextF.layer.borderWidth = 0.5;
    weddingVenueTextF.layer.masksToBounds=YES;
    weddingVenueTextF.text              =   [userDefaults valueForKey:KEY_WEDDING_VENUE];
    UIView *paddingView1                =   [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH*0.02, 20)];
    weddingVenueTextF.leftView          =   paddingView1;
    weddingVenueTextF.leftViewMode      =   UITextFieldViewModeAlways;
    weddingVenueTextF.backgroundColor   =   [UIColor clearColor];
    
    [firstBackView addSubview:weddingVenueTextF];
    
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    toolBar.barStyle = UIBarStyleBlackOpaque;
    UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneBtnClicked)];
    
    [toolBar setItems:[NSArray arrayWithObjects:flexibleSpaceLeft,btn, nil]];
    
    
    weddingTimeTextF.inputAccessoryView = toolBar;
    weddingDateTextF.inputAccessoryView = toolBar;
    
    
}

-(void)receptionViewMaking  : (float)height{
    UIView *firstBackView = [[UIView alloc]init];
    firstBackView.frame = CGRectMake(0, height, SCREEN_WIDTH, SCREEN_HEIGHT/4);
    firstBackView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:firstBackView];
    
    
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT/20);
    [self lablecustimizing:titleLabel title:@"Reception Day" alignment: NSTextAlignmentCenter FontSize :14];
    [firstBackView addSubview:titleLabel];
    
    UILabel *weddingTitleDate = [[UILabel alloc]init];
    weddingTitleDate.frame = CGRectMake(SCREEN_WIDTH/10.67, SCREEN_HEIGHT/16, SCREEN_WIDTH/3.2, SCREEN_HEIGHT/20);
    [self lablecustimizing:weddingTitleDate title:@"Date:" alignment: NSTextAlignmentLeft   FontSize :12];
    [firstBackView addSubview:weddingTitleDate];
    
    UITextField *weddingDateTextF = [[UITextField alloc]init];
    weddingDateTextF.frame = CGRectMake(SCREEN_WIDTH/2.46, SCREEN_HEIGHT/16, SCREEN_WIDTH/2.13, SCREEN_HEIGHT/20);
    [self textFieldCustomize:weddingDateTextF placeHolder: @"Date/month/year"];
    weddingDateTextF.tag =54;
    weddingDateTextF.text   =   [userDefaults valueForKey:KEY_RECEPTION_DAY];
    [firstBackView addSubview:weddingDateTextF];
    
    UILabel *weddingTitleTime = [[UILabel alloc]init];
    weddingTitleTime.frame = CGRectMake(SCREEN_WIDTH/10.67, SCREEN_HEIGHT/8.72, SCREEN_WIDTH/3.2, SCREEN_HEIGHT/20);
    [self lablecustimizing:weddingTitleTime title:@"Time:" alignment: NSTextAlignmentLeft   FontSize :12];
    [firstBackView addSubview:weddingTitleTime];
    
    UITextField *weddingTimeTextF = [[UITextField alloc]init];
    weddingTimeTextF.frame = CGRectMake(SCREEN_WIDTH/2.46, SCREEN_HEIGHT/8.72, SCREEN_WIDTH/2.13, SCREEN_HEIGHT/20);
    [self textFieldCustomize:weddingTimeTextF placeHolder: @"Time"];
    weddingTimeTextF.tag =55;
    weddingTimeTextF.text   =   [userDefaults valueForKey:KEY_RECEPTION_TIME];
    [firstBackView addSubview:weddingTimeTextF];
    
    UILabel *weddingTitleVenue = [[UILabel alloc]init];
    weddingTitleVenue.frame = CGRectMake(SCREEN_WIDTH/10.67, SCREEN_HEIGHT/6, SCREEN_WIDTH/3.2, SCREEN_HEIGHT/20);
    [self lablecustimizing:weddingTitleVenue title:@"Venue:" alignment: NSTextAlignmentLeft   FontSize :12];
    [firstBackView addSubview:weddingTitleVenue];
    
    UITextField *weddingVenueTextF      =   [[UITextField alloc]init];
    weddingVenueTextF.frame             =   CGRectMake(SCREEN_WIDTH/2.46, SCREEN_HEIGHT/6, SCREEN_WIDTH/2.13, SCREEN_HEIGHT/20);
    [self textFieldCustomize:weddingVenueTextF placeHolder: @""];
    weddingVenueTextF.layer.cornerRadius=   8.0f;
    weddingVenueTextF.layer.borderColor =   [UIColor whiteColor].CGColor;
    weddingVenueTextF.tag               =   56;
    weddingVenueTextF.layer.borderWidth =   0.5;
    weddingVenueTextF.layer.masksToBounds=  YES;
    weddingVenueTextF.text              =   [userDefaults valueForKey:KEY_RECEPTION_VENUE];
    UIView *paddingView1                =   [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH*0.02, 20)];
    weddingVenueTextF.leftView          =   paddingView1;
    weddingVenueTextF.leftViewMode      =   UITextFieldViewModeAlways;
    weddingVenueTextF.backgroundColor   =   [UIColor clearColor];
    [firstBackView addSubview:weddingVenueTextF];
    
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    toolBar.barStyle = UIBarStyleBlackOpaque;
    UIBarButtonItem *flexibleSpaceLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneBtnClicked)];
    
    [toolBar setItems:[NSArray arrayWithObjects:flexibleSpaceLeft,btn, nil]];
    
    
    weddingTimeTextF.inputAccessoryView = toolBar;
    weddingDateTextF.inputAccessoryView = toolBar;
    
    
}

-(void)lablecustimizing : (UILabel *)label title:(NSString*)tile alignment:(NSTextAlignment)textAlifnment FontSize: (float)fontsize{
    label.textAlignment = textAlifnment;
    label.text =tile;
    label.textColor = [UIColor whiteColor];
    label.backgroundColor = [UIColor clearColor];
    label.font  =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.04];
    
}
-(void)textFieldCustomize : (UITextField *)textField placeHolder: (NSString *)placeholder{
    textField.textColor = [UIColor whiteColor];
    textField.placeholder = placeholder;
    [textField setValue:[UIColor whiteColor]
             forKeyPath:@"_placeholderLabel.textColor"];
    textField.delegate = self;
    textField.font  =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.04];
    
    
}

#pragma mark Back Button Method
-(void)backButtonClicked{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark Method to be called when tap on logout button
-(void)logoutButtonClicked{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

#pragma mark Method to be called when submit button is tapped
-(void)submitButtonClicked{
    NSLog(@"ADD DATE TIME");
    
    UITextField *temp1  =   (UITextField *)[self.view viewWithTag:51];
    UITextField *temp2  =   (UITextField *)[self.view viewWithTag:52];
    UITextField *temp3  =   (UITextField *)[self.view viewWithTag:53];
    UITextField *temp4  =   (UITextField *)[self.view viewWithTag:54];
    UITextField *temp5  =   (UITextField *)[self.view viewWithTag:55];
    UITextField *temp6  =   (UITextField *)[self.view viewWithTag:56];
    
    if([common reachabilityFunction]) {
        NSString *urlStr = [NSString stringWithFormat:@"%@wedding.php?user_id=%d&event_type=Wedding&date=%@&time=%@&id=1&venue=%@&status=ghjkghjk",API_URL,[[userDefaults valueForKey:@"userId"] integerValue],temp1.text,temp2.text,temp3.text];
        
        urlStr  =   [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [userDefaults setValue:temp1.text forKey:KEY_WEDDING_DAY];
        [userDefaults setValue:temp2.text forKey:KEY_WEDDING_TIME];
        [userDefaults setValue:temp3.text forKey:KEY_WEDDING_VENUE];
        [userDefaults setValue:temp4.text forKey:KEY_RECEPTION_DAY];
        [userDefaults setValue:temp5.text forKey:KEY_RECEPTION_TIME];
        [userDefaults setValue:temp6.text forKey:KEY_RECEPTION_VENUE];
        
        [userDefaults synchronize];
        
        responseData   =   [[NSMutableData alloc]init];
        
        request        =   [NSURLRequest requestWithURL:[NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] cachePolicy:NSURLCacheStorageAllowed timeoutInterval:5];
        
        connect        =   [[NSURLConnection alloc] initWithRequest:request delegate:self];
        
        [connect start];
        
        NSString *urlStr2 = [NSString stringWithFormat:@"%@wedding.php?user_id=%d&event_type=Reception&date=%@&time=%@&id=1&venue=%@&status=ghjkghjk",API_URL,[[userDefaults valueForKey:@"userId"] integerValue],temp4.text,temp5.text,temp6.text];
        
        urlStr2  =   [urlStr2 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        responseData2   =   [[NSMutableData alloc]init];
        
        request2        =   [NSURLRequest requestWithURL:[NSURL URLWithString:[urlStr2 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] cachePolicy:NSURLCacheStorageAllowed timeoutInterval:5];
        
        connect2        =   [[NSURLConnection alloc] initWithRequest:request2 delegate:self];
        
        [connect2 start];
    }else{
        [common showAlert:@"Warning" _message:@"No internet connection."];
    }
    
}

-(void)datePickerChangeDate : (UIDatePicker *)date{
    for (int loopCount = 51; loopCount<=55; loopCount++) {
        UITextField *textField = (UITextField*)[self.view viewWithTag:loopCount];
        if (textField.editing == YES && textField.tag == 51) {
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
            [dateFormat setDateFormat:@"dd-MM-yyyy"];
            NSString *pickerDate = [dateFormat stringFromDate:date.date];
            textField.text = pickerDate;
        }else if (textField.editing == YES && textField.tag == 52){
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
            [dateFormat setDateFormat:@"hh:mm a"];
            NSString *pickerDate = [dateFormat stringFromDate:date.date];
            textField.text = pickerDate;
            
        } else if (textField.editing == YES && textField.tag == 54){
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
            [dateFormat setDateFormat:@"dd-MM-yyyy"];
            NSString *pickerDate = [dateFormat stringFromDate:date.date];
            textField.text = pickerDate;
            
        }else if (textField.editing == YES && textField.tag == 55){
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
            [dateFormat setDateFormat:@"hh:mm a"];
            NSString *pickerDate = [dateFormat stringFromDate:date.date];
            textField.text = pickerDate;
            
        }
    }
}

#pragma mark TextField delegate methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

// called when 'return' key pressed. return NO to ignore.
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField.tag==51) {
        datePickerObj.datePickerMode = UIDatePickerModeDate;
        textField.inputView = datePickerObj;
        datePickerObj.hidden = NO;
    }else if (textField.tag==52){
        datePickerObj.datePickerMode = UIDatePickerModeTime;
        textField.inputView = datePickerObj;
        datePickerObj.hidden = NO;
        
    } else if (textField.tag==54){
        datePickerObj.datePickerMode = UIDatePickerModeDate;
        textField.inputView = datePickerObj;
        datePickerObj.hidden = NO;
        
    }else if (textField.tag==55){
        datePickerObj.datePickerMode = UIDatePickerModeTime;
        textField.inputView = datePickerObj;
        datePickerObj.hidden = NO;
        
    }
    if (textField.tag==56 || textField.tag==55){
        [self animateTextField:textField up:YES];
    }
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField.tag==56 || textField.tag==55){
        [self animateTextField:textField up:NO];
    }
}

#pragma mark Method to be called when tap on Done button of tool bar
-(void)doneBtnClicked{
    for (int loopInc = 51; loopInc<=55; loopInc++) {
        UITextField *textOne = (UITextField *)[self.view viewWithTag:loopInc];
        [textOne resignFirstResponder];
    }
    
}

-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    int movementDistance = -100; // tweak as needed
    float movementDuration = 0.3f; // tweak as needed
    int movement = (up ? movementDistance : -movementDistance);
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

#pragma mark Url connection delegate methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    //    NSLog(@"didReceiveResponse");
    
    if (connection==connect){
        // Login
        [responseData setLength:0];
    }else if (connection==connect2){
        // Login
        [responseData2 setLength:0];
    }
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    //    NSLog(@"didFailWithError");
    
    if(responseData==nil){
        [common showAlert:@"Error" _message:@"Server Problem"];
    }else if(responseData2==nil){
        [common showAlert:@"Error" _message:@"Server Problem"];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    if(connection==connect){
        [responseData appendData:data];
    }else if(connection==connect2){
        [responseData2 appendData:data];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    if (connection==connect){
        NSError* error;
        
        NSMutableDictionary *reviewsDict = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        
        //        NSLog(@"DICT :: %@",reviewsDict);
        if ([[reviewsDict valueForKey:@"success"] integerValue] == 1){
            //            [common showAlert:@"Info" _message:@"Guest Added"];
        }else{
            [common showAlert:@"Warning" _message:@"Oops! An error occurred."];
        }
        
    }else if (connection==connect2){
        NSError* error;
        
        NSMutableDictionary *reviewsDict = [NSJSONSerialization JSONObjectWithData:responseData2 options:kNilOptions error:&error];
        
        //        NSLog(@"DICT :: %@",reviewsDict);
        if ([[reviewsDict valueForKey:@"success"] integerValue] == 1){
            [common showAlert:@"Info" _message:@"Wedding time added"];
        }else{
            [common showAlert:@"Warning" _message:@"Oops! An error occurred."];
        }
        
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
