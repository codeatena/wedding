//
//  WeddingViewController.h
//  Wedding
//
//  Created by apple on 07/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeddingViewController : UIViewController{
    NSUserDefaults      *userDefaults;
    UILabel             *dateLbl;
    UILabel             *daysLeftLbl;
}

@end
