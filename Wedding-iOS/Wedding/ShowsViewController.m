//
//  ShowsViewController.m
//  Wedding
//
//  Created by apple on 07/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import "ShowsViewController.h"

@interface ShowsViewController ()

@end

@implementation ShowsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIImageView *bgImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    //    bgImageView.image           =   [UIImage imageNamed:WEDDING_TAB_BG];
    [bgImageView setBackgroundColor:[UIColor colorWithRed:241.0f/255.0f green:155.0f/255.0f blue:204.0f/255.0f alpha:1.0f]];
    [self.view addSubview:bgImageView];
    
    UIImageView *topImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT*0.08)];
    topImageView.image           =   [UIImage imageNamed:TOP_IMG];
    [self.view addSubview:topImageView];
    
    UIImageView *titleImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.08, SCREEN_WIDTH, SCREEN_HEIGHT*0.065)];
    [titleImageView setBackgroundColor:[UIColor colorWithRed:197.0f/255.0f green:105.0f/255.0f blue:130.0f/255.0f alpha:1.0f]];
    titleImageView.alpha            =   0.7;
    [self.view addSubview:titleImageView];
    
    UILabel *titleLbl           =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.05, SCREEN_HEIGHT*0.08, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.06)];
    titleLbl.backgroundColor    =   [UIColor clearColor];
    titleLbl.text               =   @"Wedding Shows";
    titleLbl.textColor          =   [UIColor whiteColor];
    titleLbl.textAlignment      =   NSTextAlignmentLeft;
    titleLbl.font               =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.06];
    [self.view addSubview:titleLbl];
    
    UIButton *logoutBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.80, 0,SCREEN_WIDTH*0.20,SCREEN_HEIGHT*0.08)];
    [logoutBtn setBackgroundColor:[UIColor clearColor]];
    [logoutBtn setTitle:@"Logout" forState:UIControlStateNormal];
    [logoutBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    logoutBtn.titleLabel.font     =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.05];
    [logoutBtn addTarget:self action:@selector(logoutButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:logoutBtn];
    
    [self loadWeddingShowList];
    
    tableV            =   [[UITableView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.05, SCREEN_HEIGHT*0.18, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.70)];
    tableV.dataSource =   self;
    tableV.delegate   =   self;
    tableV.backgroundColor  =   [UIColor clearColor];
    //    tableV.separatorStyle   =   UITableViewCellSeparatorStyleNone;
    tableV.showsVerticalScrollIndicator =   NO;
    [self.view addSubview:tableV];
}

#pragma mark Method to be called when tap on logout button
-(void)logoutButtonClicked{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark method to load wedding show list
-(void)loadWeddingShowList{
    if([common reachabilityFunction]) {
        NSString *urlStr = [NSString stringWithFormat:@"%@get_shows.php",API_URL];
        
        urlStr  =   [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        responseData   =   [[NSMutableData alloc]init];
        
        request        =   [NSURLRequest requestWithURL:[NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] cachePolicy:NSURLCacheStorageAllowed timeoutInterval:5];
        
        connect        =   [[NSURLConnection alloc] initWithRequest:request delegate:self];
        
        [connect start];
    }else{
        [common showAlert:@"Warning" _message:@"No internet connection."];
    }
    
}

#pragma mark Table view Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //    NSLog(@"SECTION :: %d",section);
    return dataArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return SCREEN_HEIGHT*0.40;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *unifiedID = [NSString stringWithFormat:@"aCellIDPh_%d_%d",indexPath.section,indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:unifiedID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:unifiedID];
        
        cell.selectionStyle =   UITableViewCellSelectionStyleNone;
        cell.backgroundColor    =   [UIColor clearColor];
        
        
        
        UIImageView *imgV   =   [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.65, SCREEN_HEIGHT*0.07, SCREEN_WIDTH*0.25, SCREEN_HEIGHT*0.12)];
        imgV.contentMode = UIViewContentModeScaleAspectFit;
        [imgV sd_setImageWithURL:[NSURL URLWithString:[[dataArray objectAtIndex:indexPath.row] objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_NAME]];
        [cell.contentView addSubview:imgV];
        
        // 187 140 188
        
        UIImageView *titleBG    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.01, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.05)];
        [titleBG setBackgroundColor:[UIColor colorWithRed:187.0f/255.0f green:140.0f/255.0f blue:188.0f/255.0f alpha:1.0f]];
        [cell.contentView addSubview:titleBG];
        
        UILabel *datelbl    =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.01, SCREEN_HEIGHT*0.01, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.05)];
        [datelbl setBackgroundColor:[UIColor clearColor]];
        [datelbl setTextColor:[UIColor whiteColor]];
        [datelbl setTextAlignment:NSTextAlignmentLeft];
        datelbl.numberOfLines   =   0;
        datelbl.lineBreakMode   =   NSLineBreakByWordWrapping;
        datelbl.font    =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.04];
        datelbl.text    =   [[dataArray objectAtIndex:indexPath.row] objectForKey:@"date"];
        [cell.contentView addSubview:datelbl];
        
        UILabel *namelbl    =   [[UILabel alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.07, SCREEN_WIDTH*0.60, SCREEN_HEIGHT*0.05)];
        [namelbl setBackgroundColor:[UIColor clearColor]];
        [namelbl setTextColor:[UIColor blackColor]];
        [namelbl setTextAlignment:NSTextAlignmentLeft];
        namelbl.numberOfLines   =   0;
        namelbl.lineBreakMode   =   NSLineBreakByWordWrapping;
        namelbl.font    =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.04];
        namelbl.text    =   [[dataArray objectAtIndex:indexPath.row] objectForKey:@"name"];
        [cell.contentView addSubview:namelbl];
        
        UILabel *addresslbl    =   [[UILabel alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.13, SCREEN_WIDTH*0.60, SCREEN_HEIGHT*0.15)];
        [addresslbl setBackgroundColor:[UIColor clearColor]];
        [addresslbl setTextColor:[UIColor whiteColor]];
        [addresslbl setTextAlignment:NSTextAlignmentLeft];
        addresslbl.numberOfLines   =   0;
        addresslbl.lineBreakMode   =   NSLineBreakByWordWrapping;
        addresslbl.font    =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.04];
        addresslbl.text    =   [[dataArray objectAtIndex:indexPath.row] objectForKey:@"address"];
        [cell.contentView addSubview:addresslbl];
        
        UILabel *timelbl    =   [[UILabel alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.28, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.05)];
        [timelbl setBackgroundColor:[UIColor clearColor]];
        [timelbl setTextColor:[UIColor whiteColor]];
        [timelbl setTextAlignment:NSTextAlignmentLeft];
        timelbl.numberOfLines   =   0;
        timelbl.lineBreakMode   =   NSLineBreakByWordWrapping;
        timelbl.font    =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.04];
        timelbl.text    =   [NSString stringWithFormat:@"Time : %@",[[dataArray objectAtIndex:indexPath.row] objectForKey:@"time"]];
        [cell.contentView addSubview:timelbl];
        
        UILabel *contactlbl    =   [[UILabel alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.33, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.05)];
        [contactlbl setBackgroundColor:[UIColor clearColor]];
        [contactlbl setTextColor:[UIColor whiteColor]];
        [contactlbl setTextAlignment:NSTextAlignmentLeft];
        contactlbl.numberOfLines   =   0;
        contactlbl.lineBreakMode   =   NSLineBreakByWordWrapping;
        contactlbl.font    =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.04];
        contactlbl.text    =   [NSString stringWithFormat:@"Contact : %@",[[dataArray objectAtIndex:indexPath.row] objectForKey:@"show"]];
        [cell.contentView addSubview:contactlbl];
        
        
        
    }
    return cell;
    
    
}


#pragma mark Url connection delegate methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    //    NSLog(@"didReceiveResponse");
    
    if (connection==connect){
        // Login
        [responseData setLength:0];
    }
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    //    NSLog(@"didFailWithError");
    
    if(responseData==nil){
        [common showAlert:@"Error" _message:@"Server Problem"];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    if(connection==connect){
        [responseData appendData:data];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    if (connection==connect){
        NSError* error;
        
        //        NSString *responseString = [[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
        
        //        NSLog(@"Response :::: %@",responseString);
        
        NSMutableDictionary *reviewsDict = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        
        //        NSLog(@"DICT :: %@",[[reviewsDict valueForKey:@"get_suppler_category"] objectAtIndex:0]);
        dataArray    =   [[reviewsDict valueForKey:@"shows"] mutableCopy];
        
//        NSLog(@"DICT :: %@",dataArray);
        [tableV reloadData];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
