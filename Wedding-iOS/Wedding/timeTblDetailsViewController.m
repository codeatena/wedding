//
//  timeTblDetailsViewController.m
//  Wedding
//
//  Created by apple on 10/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import "timeTblDetailsViewController.h"

@interface timeTblDetailsViewController ()

@end

@implementation timeTblDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (id)initWithData:(int)_no{
    self = [super init];
    if (self) {
        // Custom initialization
        number  =   _no;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSMutableArray *titleArr    =   [[NSMutableArray alloc]initWithObjects:@"12-9 Months Before",@"9-6 Months Before",@"6-3 Months Before",@"3-2 Months Before",@"1 Months Before",@"Night before the Wedding",@"Wedding Day", nil];
    
    NSString *str1  =   @"- Meet religious minister/registrar to discuss wedding ceremony.\n- Set the budget.\n- Buy a wedding notebook or organiser.\n- Set up a filing system for details and receipts.\n- Choose the wedding date.\n- Order wedding cars to transport.\n- Draw up a guest list.\n- Decide Whether children are to be invited.\n- Choose and book venues for wedding and reception.\n- Obtain menus and Cost estimates from caterers.\n- Investigate photographers; an engagement photo can be taken at this point.\n- Look into entertainment for the wedding reception.";
    
    NSString *str2  =   @"- Finalise the requirements for the style of ceremony chosen.\n- Discuss the music with an organist,music group or wedding music company.\n- Discuss reading with the minister or registrar.\n- Ask family members or friend if they would like to read at the ceremony.\n- choose the caterers.\n- Research table settings, menus and seating plans.\n- See a wine merchant/venue representative about drinks.\n- Choose and order the wedding cake.\n- Choose flowers for bridal bouquets,bridesmaid bouquets, buttonholes,the venue and table centres.\n- Finalise the guest list.\n- Design and order the invitations and envelopes.\n- Book and pay deposits for the florist, photographers,caterers,cake making company and stationer.\nConfirm and book musical entertainment for the reception.\n- Book toastmaster, if required.";
    
    NSString *str3  =   @"- Hire or buy outfits for the groom, best man and ushers, or inform them, and fathers, of the dress code.\n- Choose/order outfits for the bridesmaids.\n-Shop for the mother-of-the-bride outfit.\n- Choose, buy and insure wedding rings.\n- Choose the presents for the wedding list.\n- Choose presents to thank the best man,ushers,attendants and other VIP’s.\n- Design orders of service.\n- Print orders of service(don’t forget spares).\n- Send off the forms for the bride’s new passport.\n- Look into arrangements and details for the honeymoon.\n- Plan extra information sheets for the invitation: maps, nearby B and Bs, Wedding list information or no-children rules.\n- Send out invitations.\n- Keep a list of acceptance and refusals, and send out any extra invitations.\n- Plan post-wedding lunches(if required).\n- Ensure hen and stag parties are being organised.";
    
    NSString *str4  =   @"- Start the reception seating plan, but allow for changes.\n- Politely chase any forgetful guest for RSVPs in order to finalise the guest list.\n- Give caterers notice of special dietary requirements.\n- Set a ceremony rehearsal time with the minister/registrar/officiant and inform the wedding party.\n- Meet the hair and makeup stylists to decide on a style for the bride and bridesmaid.\n- Final dress fittings for bride and bridesmaids.\n- Order favours for the reception(if required).\n- Buy a guest book for guest to sign.\n- Choose a first dance and consider practising.\n- Make a note of favourite songs for the DJ or band.\n- Buy clothes and accessories for the honeymoon.\n- Reconfirm honeymoon details.\n- Check that visas,passports and inoculations are all up to date.\n- Organise domestic arrangements for the honeymoon period: pets, house-sitting and zoo on.\n- Hold the hen and stag parties.";
    
    NSString *str5  =   @"- Pick up wedding rings; have them engraved, if desired.\n- Have engagement ring cleaned or polished.\n- Conﬁrm prices with suppliers, double-check delivery or arrival timings.\n- Pay any outstanding amounts to suppliers.\n- Reconﬁrm all details with suppliers and staff involved on the day; this includes caterers, cakes, flowers, photographs, and hair and make—up stylists.\n- Reconﬁrm all transport arrangements (locations and timings).\n- Conﬁrm ﬁnal numbers, dietary requirements and special requests with the caterers.\n- Draw up the ﬁnal seating plan.\n— Make up extra copies of the seating plan for the caterer, photographer and best man.\n- Arrange for name and address changes on your bank account, credit cards, driver's licence, social security and utilities.\n- Make a checklist of telephone numbers in case of any emergencies on the day: local cab ﬁrms, the wedding party's mobiles and suppliers. Also, brief wedding party of what they need on the day.\n- Draw up a list of important family and friends for the photographer, with a copy for the best man.\n- Brief the best man on ﬁnal payments due on the day, and supply cash or cheques, as required.\n— Organise reserved seating for the ceremony and brief the ushers.\n- Brief the best man and chief bridesmaid on any ﬁnal logistics and last minute duties.\n— Collect the wedding dress and bridesmaids’ outﬁts.\n— Practise putting on the wedding dress with whoever will be helping on the big day.\n- Try on the full bridal outﬁt — including jewellery and lingerie - and practise walking in shoes.\n— Order honeymoon currency/travellers cheques.\n— Pack and organise luggage for the honeymoon;";
    
    NSString *str6  =   @"The evening before the wedding is an opportunity for the bride and groom to relax and spend time reminiscing with parents or close friends. They will usually spent the evening, or at least the night, apa|1. The bridesmaids usually staythe night with the bride so they are in situ for the morning, and the best man, and possibly the ushers, often stay with the groom. The logistics obviously depend on the location of the wedding and the style of the venue. For example, if the wedding is at a hotel, then the entire wedding party might be staying there the night before. However, ifthe big day is held at the bride's parents house, then it is likely that members of the wedding party will be with either the bride or groom. Alcohol should be drunk in strict moderation and an early night is advisable!";
    NSString *str6_1    =   @"Wedding Rehearsal";
    NSString *str6_2    =   @"There will usually be a rehearsal of the wedding ceremony during the afternoon or evening preceding the wedding. Key members of the wedding party may be asked to attend. The ceremony ofﬁciant will run through the ceremony so everyone is prepared on the day.";
    NSString *str6_3    =   @"Pre-Wedding Dinners";
    NSString *str6_4    =   @"Pre-wedding dinners for family and close friends are an ideal time to get together and discuss the approaching day. They can be held in a hotel or restaurant, the wedding venue or in the more informal atmosphere of the parents’ home. A small dinner for close family only is a good way to spend the evening before the wedding; a larger pre-wedding dinner should be held two nights in advance, if possible, to keep stress levels to a minimum.";
    
    NSString *str7  =   @"The bride and groom should plan out the times of events of the day. The timing of the wedding day is crucial: good planning and timing can help the day run without a hitch. A schedule for the wedding should be mapped out, and all suppliers informed of the expected running order.\n\n The following is a rough timeline for the big day, from the ceremony to the end of the reception.";
    NSString *str7_1    =   @"Arrivals at the Ceremony";
    NSString *str7_2    =   @"Ushers 45 minutes before; best man and groom 45-30 minutes before; guests: up to 30 minutes before; groom's parents: 15 minutes before; mother of bride and bridesmaids ten minutes before; bride and father ﬁve minutes before.";
    NSString *str7_3    =   @"Church or Civil Ceremony";
    NSString *str7_4    =   @"20-60 minutes (civil ceremonies are shorter than religious ones) N.B. Allow plenty of time to qet from the ceremony venue to the reception, if applicable, especially if the area is prone to heavy trafﬁc.";
    NSString *str7_5    =   @"Photographs";
    NSString *str7_6    =   @"20-30 minutes Note: Allow approximately ten minutes for pictures of the bride and groom, ten to twenty minutes for other shots.";
    NSString *str7_7    =   @"Drinks Reception";
    NSString *str7_8    =   @"90 minutes, usually extends to two hours Note: A receiving line takes about 40 minutes and well organised photographs take 20-30 minutes.";
    NSString *str7_9    =   @"Seating guests";
    NSString *str7_10   =   @"Up to 30 minutes.";
    NSString *str7_11   =   @"The Wedding Meal";
    NSString *str7_12   =   @"Two hours for three courses.";
    NSString *str7_13   =   @"Speeches";
    NSString *str7_14   =   @"30 minutes, usually extending to 45 minutes (at least ten minutes per person, assuming there are three speakers).";
    NSString *str7_15   =   @"Cake Cutting";
    NSString *str7_16   =   @"15-20 minutes.";
    NSString *str7_17   =   @"Dancing";
    NSString *str7_18   =   @"Approximately three hours. Note: This depends upon when the bride and groom leave, and how long the party lasts, and the style of the venue/reception. 15 minutes (throwing bouquet and saying goodbye).";
    
    NSMutableArray *subDataArr  =   [[NSMutableArray alloc]initWithObjects:str7_1,str7_2,str7_3,str7_4,str7_5,str7_6,str7_7,str7_8,str7_9,str7_10,str7_11,str7_12,str7_13,str7_14,str7_15,str7_16,str7_17,str7_18, nil];
    
    
    NSMutableArray *dataArr     =   [[NSMutableArray alloc]initWithObjects:str1,str2,str3,str4,str5,str6,str7, nil];
    
    UIImageView *bgImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    //    bgImageView.image           =   [UIImage imageNamed:WEDDING_TAB_BG];
    [bgImageView setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:bgImageView];
    
    UIImageView *topImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT*0.08)];
    topImageView.image           =   [UIImage imageNamed:TOP_IMG];
    [self.view addSubview:topImageView];
    
    UIImageView *titleImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.08, SCREEN_WIDTH, SCREEN_HEIGHT*0.065)];
    [titleImageView setBackgroundColor:[UIColor colorWithRed:197.0f/255.0f green:105.0f/255.0f blue:130.0f/255.0f alpha:1.0f]];
    titleImageView.alpha            =   0.7;
    [self.view addSubview:titleImageView];
    
    UILabel *titleLbl           =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.10, SCREEN_HEIGHT*0.08, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.06)];
    titleLbl.backgroundColor    =   [UIColor clearColor];
    titleLbl.text               =   [titleArr objectAtIndex:number];
    titleLbl.textColor          =   [UIColor whiteColor];
    titleLbl.textAlignment      =   NSTextAlignmentLeft;
    titleLbl.font               =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.055];
    [self.view addSubview:titleLbl];
    
    UIButton *backBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.03, SCREEN_HEIGHT*0.09,SCREEN_WIDTH*0.05,SCREEN_HEIGHT*0.04)];
    [backBtn setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMG] forState:UIControlStateNormal];
    [backBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    
    UIButton *logoutBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.80, 0,SCREEN_WIDTH*0.20,SCREEN_HEIGHT*0.08)];
    [logoutBtn setBackgroundColor:[UIColor clearColor]];
    [logoutBtn setTitle:@"Logout" forState:UIControlStateNormal];
    [logoutBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    logoutBtn.titleLabel.font     =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.05];
    [logoutBtn addTarget:self action:@selector(logoutButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:logoutBtn];
    
    UIScrollView *contentScroll =   [[UIScrollView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.18, SCREEN_WIDTH, SCREEN_HEIGHT*0.80)];
    [contentScroll setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:contentScroll];
    
    UILabel *txtLbl           =  [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.05, SCREEN_HEIGHT*0.02, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.20)];
    txtLbl.backgroundColor    =   [UIColor clearColor];
    txtLbl.text               =   [dataArr objectAtIndex:number];
    txtLbl.textColor          =   [UIColor blackColor];
    txtLbl.textAlignment      =   NSTextAlignmentLeft;
    txtLbl.font               =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.04];
    txtLbl.numberOfLines      =   0;
    txtLbl.lineBreakMode      =   NSLineBreakByWordWrapping;
    [txtLbl sizeToFit];
    [contentScroll addSubview:txtLbl];
    
    float topMargin =   txtLbl.frame.size.height+SCREEN_HEIGHT*0.03;
    
    if (number == 5) {
        
        UILabel *titleLbl           =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.05, topMargin, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.06)];
        titleLbl.backgroundColor    =   [UIColor clearColor];
        titleLbl.text               =   str6_1;
        titleLbl.textColor          =   [UIColor colorWithRed:162.0f/255.0f green:128.0f/255.0f blue:80.0f/255.0f alpha:1.0f];
        titleLbl.textAlignment      =   NSTextAlignmentLeft;
        titleLbl.font               =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.055];
        [contentScroll addSubview:titleLbl];
        
        topMargin   +=  SCREEN_HEIGHT*0.08;
        
        UILabel *txtLbl           =  [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.05, topMargin, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.20)];
        txtLbl.backgroundColor    =   [UIColor clearColor];
        txtLbl.text               =   str6_2;
        txtLbl.textColor          =   [UIColor blackColor];
        txtLbl.textAlignment      =   NSTextAlignmentLeft;
        txtLbl.font               =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.04];
        txtLbl.numberOfLines      =   0;
        txtLbl.lineBreakMode      =   NSLineBreakByWordWrapping;
        [txtLbl sizeToFit];
        [contentScroll addSubview:txtLbl];
        
        topMargin   +=  txtLbl.frame.size.height;
        
        UILabel *titleLbl2           =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.05, topMargin, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.06)];
        titleLbl2.backgroundColor    =   [UIColor clearColor];
        titleLbl2.text               =   str6_3;
        titleLbl2.textColor          =   [UIColor colorWithRed:162.0f/255.0f green:128.0f/255.0f blue:80.0f/255.0f alpha:1.0f];
        titleLbl2.textAlignment      =   NSTextAlignmentLeft;
        titleLbl2.font               =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.055];
        [contentScroll addSubview:titleLbl2];
        
        topMargin   +=  SCREEN_HEIGHT*0.08;
        
        UILabel *txtLbl2           =  [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.05, topMargin, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.20)];
        txtLbl2.backgroundColor    =   [UIColor clearColor];
        txtLbl2.text               =   str6_4;
        txtLbl2.textColor          =   [UIColor blackColor];
        txtLbl2.textAlignment      =   NSTextAlignmentLeft;
        txtLbl2.font               =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.04];
        txtLbl2.numberOfLines      =   0;
        txtLbl2.lineBreakMode      =   NSLineBreakByWordWrapping;
        [txtLbl2 sizeToFit];
        [contentScroll addSubview:txtLbl2];
        
        topMargin   +=  txtLbl.frame.size.height;
    }else if (number == 6) {
        int index   =   0;
        for (int i=0 ; i<9; i++) {
            
            
            UILabel *titleLbl           =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.05, topMargin, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.06)];
            titleLbl.backgroundColor    =   [UIColor clearColor];
            titleLbl.text               =   [subDataArr objectAtIndex:index];
            titleLbl.textColor          =   [UIColor colorWithRed:162.0f/255.0f green:128.0f/255.0f blue:80.0f/255.0f alpha:1.0f];
            titleLbl.textAlignment      =   NSTextAlignmentLeft;
            titleLbl.font               =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.055];
            [contentScroll addSubview:titleLbl];
            
            topMargin   +=  SCREEN_HEIGHT*0.08;
            index++;
            
            UILabel *txtLbl           =  [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.05, topMargin, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.20)];
            txtLbl.backgroundColor    =   [UIColor clearColor];
            txtLbl.text               =   [subDataArr objectAtIndex:index];
            txtLbl.textColor          =   [UIColor blackColor];
            txtLbl.textAlignment      =   NSTextAlignmentLeft;
            txtLbl.font               =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.04];
            txtLbl.numberOfLines      =   0;
            txtLbl.lineBreakMode      =   NSLineBreakByWordWrapping;
            [txtLbl sizeToFit];
            [contentScroll addSubview:txtLbl];
            
            topMargin   +=  txtLbl.frame.size.height;
            index++;
            
            
        }
        
    }
    
    [contentScroll setContentSize:CGSizeMake(SCREEN_WIDTH, topMargin+SCREEN_HEIGHT*0.15)];
}

#pragma mark Back Button Method
-(void)backButtonClicked{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark Method to be called when tap on logout button
-(void)logoutButtonClicked{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
