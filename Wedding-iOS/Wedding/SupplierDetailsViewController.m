//
//  SupplierDetailsViewController.m
//  Wedding
//
//  Created by apple on 07/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import "SupplierDetailsViewController.h"
#import "DetailsViewController.h"
#import "GiftViewController.h"

@interface SupplierDetailsViewController ()

@end

@implementation SupplierDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)initWithCustomData:(int)catId{
    self = [super init];
    if (self) {
        // Custom initialization
        categoryId  =   catId;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    dataArray                   =   [[NSMutableArray alloc]init];
    
    UIImageView *bgImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    //    bgImageView.image           =   [UIImage imageNamed:WEDDING_TAB_BG];
    [bgImageView setBackgroundColor:[UIColor colorWithRed:241.0f/255.0f green:155.0f/255.0f blue:204.0f/255.0f alpha:1.0f]];
    [self.view addSubview:bgImageView];
    
    UIImageView *topImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT*0.08)];
    topImageView.image           =   [UIImage imageNamed:TOP_IMG];
    [self.view addSubview:topImageView];
    
    UIImageView *titleImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.08, SCREEN_WIDTH, SCREEN_HEIGHT*0.065)];
    [titleImageView setBackgroundColor:[UIColor colorWithRed:197.0f/255.0f green:105.0f/255.0f blue:130.0f/255.0f alpha:1.0f]];
    titleImageView.alpha            =   0.7;
    [self.view addSubview:titleImageView];
    
    UILabel *titleLbl           =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.10, SCREEN_HEIGHT*0.08, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.06)];
    titleLbl.backgroundColor    =   [UIColor clearColor];
    titleLbl.text               =   @"Category List";
    titleLbl.textColor          =   [UIColor whiteColor];
    titleLbl.textAlignment      =   NSTextAlignmentLeft;
    titleLbl.font               =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.06];
    [self.view addSubview:titleLbl];
    
    UIButton *backBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.03, SCREEN_HEIGHT*0.09,SCREEN_WIDTH*0.05,SCREEN_HEIGHT*0.04)];
    [backBtn setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMG] forState:UIControlStateNormal];
    [backBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    
    UIButton *logoutBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.80, 0,SCREEN_WIDTH*0.20,SCREEN_HEIGHT*0.08)];
    [logoutBtn setBackgroundColor:[UIColor clearColor]];
    [logoutBtn setTitle:@"Logout" forState:UIControlStateNormal];
    [logoutBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    logoutBtn.titleLabel.font     =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.05];
    [logoutBtn addTarget:self action:@selector(logoutButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:logoutBtn];
    
    tableV            =   [[UITableView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.05, SCREEN_HEIGHT*0.16, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.75)];
    tableV.dataSource =   self;
    tableV.delegate   =   self;
    tableV.backgroundColor  =   [UIColor clearColor];
    tableV.separatorStyle   =   UITableViewCellSeparatorStyleNone;
    tableV.showsVerticalScrollIndicator =   NO;
    [self.view addSubview:tableV];
    
    [self loadSuplierList];
}

#pragma mark Back Button Method
-(void)backButtonClicked{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark Method to be called when tap on logout button
-(void)logoutButtonClicked{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

#pragma mark Method to load supplier list
-(void)loadSuplierList{
    if([common reachabilityFunction]) {
        NSString *urlStr = [NSString stringWithFormat:@"%@get_suppler.php?cate_id=%d",API_URL,categoryId];
        
        urlStr  =   [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        responseData   =   [[NSMutableData alloc]init];
        
        request        =   [NSURLRequest requestWithURL:[NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] cachePolicy:NSURLCacheStorageAllowed timeoutInterval:5];
        
        connect        =   [[NSURLConnection alloc] initWithRequest:request delegate:self];
        
        [connect start];
    }else{
        [common showAlert:@"Warning" _message:@"No internet connection."];
    }
}

#pragma mark Url connection delegate methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    //    NSLog(@"didReceiveResponse");
    
    if (connection==connect){
        // Login
        [responseData setLength:0];
    }
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    //    NSLog(@"didFailWithError");
    
    if(responseData==nil){
        [common showAlert:@"Error" _message:@"Server Problem"];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    if(connection==connect){
        [responseData appendData:data];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    if (connection==connect){
        NSError* error;
        
        //        NSString *responseString = [[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
        
        //        NSLog(@"Response :::: %@",responseString);
        
        NSMutableDictionary *reviewsDict = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        
        //        NSLog(@"DICT :: %@",[[reviewsDict valueForKey:@"get_suppler_category"] objectAtIndex:0]);
        dataArray    =   [[reviewsDict valueForKey:@"suppliers"] mutableCopy];
        
        NSLog(@"SUPDICT :: %@",dataArray);
        [tableV reloadData];
    }
}

#pragma mark Table view Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    //    NSLog(@"SECTION :: %d",section);
    return dataArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return SCREEN_HEIGHT*0.12;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *unifiedID = [NSString stringWithFormat:@"aCellIDPh_%d_%d",indexPath.section,indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:unifiedID];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:unifiedID];
        
        cell.selectionStyle =   UITableViewCellSelectionStyleNone;
        cell.backgroundColor    =   [UIColor clearColor];
        
        //        [cell.imageView sd_setImageWithURL:[NSURL URLWithString:[[dataArray objectAtIndex:indexPath.row] objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_NAME]];
        //        cell.textLabel.text =   [[dataArray objectAtIndex:indexPath.row] objectForKey:@"suppler_name"];
        
        UIImageView *imgV   =   [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.01, SCREEN_HEIGHT*0.10, SCREEN_HEIGHT*0.10)];
        imgV.contentMode = UIViewContentModeScaleAspectFit;
        [imgV sd_setImageWithURL:[NSURL URLWithString:[[dataArray objectAtIndex:indexPath.row] objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_NAME]];
        [cell.contentView addSubview:imgV];
        
        UILabel *lbl    =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_HEIGHT*0.17, 0, SCREEN_WIDTH*0.60, SCREEN_HEIGHT*0.10)];
        [lbl setBackgroundColor:[UIColor clearColor]];
        [lbl setTextColor:[UIColor whiteColor]];
        [lbl setTextAlignment:NSTextAlignmentLeft];
        lbl.numberOfLines   =   0;
        lbl.lineBreakMode   =   NSLineBreakByWordWrapping;
        lbl.font    =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.04];
        lbl.text =   [NSString stringWithFormat:@"%@\n%@",[[dataArray objectAtIndex:indexPath.row] objectForKey:@"suppler_name"],[[dataArray objectAtIndex:indexPath.row] objectForKey:@"city"]];
        [cell.contentView addSubview:lbl];
        
        if ([[[dataArray objectAtIndex:indexPath.row] objectForKey:@"offer_exist"] integerValue] == 1) {
            UIButton *giftBtn   =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.75, SCREEN_HEIGHT*0.02, SCREEN_HEIGHT*0.10, SCREEN_HEIGHT*0.06)];
            [giftBtn setBackgroundImage:[UIImage imageNamed:GIFT_VOUCHER_IMG] forState:UIControlStateNormal];
            [giftBtn setBackgroundColor:[UIColor redColor]];
            [giftBtn setTag:indexPath.row+100];
            [giftBtn addTarget:self action:@selector(giftVoucherClicked:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:giftBtn];
        }
        
    }
    return cell;
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //    NSLog(@"Show supplier details");
    DetailsViewController *detailVC =   [[DetailsViewController alloc]initWithCustomData:[dataArray objectAtIndex:indexPath.row]];
    [self.navigationController pushViewController:detailVC animated:NO];
}

#pragma mark Method to be called when tap on gift voucher btn
-(void)giftVoucherClicked:(UIButton *)sender{
    //    NSLog(@"giftVoucherClicked %d",sender.tag-100);
    
    GiftViewController *giftVC =   [[GiftViewController alloc]init];
    [self.navigationController pushViewController:giftVC animated:NO];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
