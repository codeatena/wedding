//
//  CustomCollectionCell.m
//  Wedding
//
//  Created by apple on 07/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import "CustomCollectionCell.h"

@implementation CustomCollectionCell

@synthesize imageV;
@synthesize nameLbl;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        imageV  =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH*0.20, SCREEN_HEIGHT*0.14)];
        imageV.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:imageV];
        
        nameLbl =   [[UILabel alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.14, SCREEN_WIDTH*0.20, SCREEN_HEIGHT*0.05)];
        [nameLbl setBackgroundColor:[UIColor clearColor]];
        [nameLbl setTextColor:[UIColor whiteColor]];
        [nameLbl setTextAlignment:NSTextAlignmentCenter];
        nameLbl.numberOfLines   =   0;
        nameLbl.lineBreakMode   =   NSLineBreakByWordWrapping;
        nameLbl.font    =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.023];
        [self.contentView addSubview:nameLbl];
        
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
