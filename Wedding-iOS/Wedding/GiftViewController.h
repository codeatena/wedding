//
//  GiftViewController.h
//  Wedding
//
//  Created by apple on 08/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>

@interface GiftViewController : UIViewController<UIActionSheetDelegate,MFMailComposeViewControllerDelegate>

@end
