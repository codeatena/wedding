//
//  DetailsViewController.h
//  Wedding
//
//  Created by apple on 08/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface DetailsViewController : UIViewController<NSURLConnectionDelegate,NSURLConnectionDataDelegate,MFMailComposeViewControllerDelegate>{
    NSDictionary            *dataDict;
    int                     isFavorite;
    UIScrollView            *contentScroll;
    NSMutableData           *responseData;
    NSURLRequest            *request;
    NSURLConnection         *connect;
    NSMutableData           *responseData2;
    NSURLRequest            *request2;
    NSURLConnection         *connect2;
    NSDictionary            *suppDataDict;
    UIImageView             *supplierImgView;
    NSMutableArray          *imgArr;
    UIButton                *preBtn;
    UIButton                *nxtBtn;
    int                     imgIndex;
    int                     isZoomedIn;
    NSUserDefaults          *userDefaults;
}

- (id)initWithCustomData:(NSDictionary *)_dataDict;

@end
