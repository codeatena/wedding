//
//  LoginViewController.m
//  Wedding
//
//  Created by apple on 07/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import "LoginViewController.h"
#import "RegisterViewController.h"

#import "WeddingViewController.h"
#import "SupplierViewController.h"
#import "OfferViewController.h"
#import "ShowsViewController.h"
#import "AboutViewController.h"


@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view. 197 105 130
    
    userDefault                 =   [NSUserDefaults standardUserDefaults];
    
    isRememberMe                =   [userDefault integerForKey:@"rememberCredentials"];
    
    UIImageView *bgImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    bgImageView.image           =   [UIImage imageNamed:LOGIN_BG_IMG];
    [self.view addSubview:bgImageView];
    
    UIImageView *topImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT*0.08)];
    topImageView.image           =   [UIImage imageNamed:TOP_IMG];
    [self.view addSubview:topImageView];
    
    UIImageView *titleImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.08, SCREEN_WIDTH, SCREEN_HEIGHT*0.065)];
    [titleImageView setBackgroundColor:[UIColor colorWithRed:197.0f/255.0f green:105.0f/255.0f blue:130.0f/255.0f alpha:1.0f]];
    titleImageView.alpha            =   0.7;
    [self.view addSubview:titleImageView];
    
    UILabel *titleLbl           =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.05, SCREEN_HEIGHT*0.08, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.06)];
    titleLbl.backgroundColor    =   [UIColor clearColor];
    titleLbl.text               =   @"Login";
    titleLbl.textColor          =   [UIColor whiteColor];
    titleLbl.textAlignment      =   NSTextAlignmentLeft;
    titleLbl.font               =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.06];
    [self.view addSubview:titleLbl];
    
    UILabel *emailTitle         =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.10, SCREEN_HEIGHT*0.15, SCREEN_WIDTH*0.80, SCREEN_HEIGHT*0.06)];
    emailTitle.backgroundColor  =   [UIColor clearColor];
    emailTitle.text             =   EMAIL_TITLE_TEXT;
    emailTitle.textColor        =   [UIColor whiteColor];
    emailTitle.textAlignment    =   NSTextAlignmentLeft;
    emailTitle.font             =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.05];
    [self.view addSubview:emailTitle];
    
    emailTxt       =   [[UITextField alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.10, SCREEN_HEIGHT*0.22, SCREEN_WIDTH*0.80, SCREEN_HEIGHT*0.06)];
    emailTxt.placeholder        =   @"";
    emailTxt.delegate           =   self;
    emailTxt.backgroundColor    =   [UIColor blackColor];
    emailTxt.layer.cornerRadius =   22;
    emailTxt.font               =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.05];
    emailTxt.alpha              =   0.6;
    emailTxt.textColor          =   [UIColor whiteColor];
    emailTxt.text               =   [userDefault valueForKey:@"email"];
    // For adding padding in text field
    UIView *paddingView1        =   [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH*0.04, 20)];
    emailTxt.leftView           =   paddingView1;
    emailTxt.leftViewMode       =   UITextFieldViewModeAlways;
    [self.view addSubview:emailTxt];
    
    UILabel *passwordTitle         =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.10, SCREEN_HEIGHT*0.29, SCREEN_WIDTH*0.80, SCREEN_HEIGHT*0.06)];
    passwordTitle.backgroundColor  =   [UIColor clearColor];
    passwordTitle.text             =   PASSWORD_TITLE_TEXT;
    passwordTitle.textColor        =   [UIColor whiteColor];
    passwordTitle.textAlignment    =   NSTextAlignmentLeft;
    passwordTitle.font             =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.05];
    [self.view addSubview:passwordTitle];
    
    passwordTxt        =   [[UITextField alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.10, SCREEN_HEIGHT*0.36, SCREEN_WIDTH*0.80, SCREEN_HEIGHT*0.06)];
    passwordTxt.placeholder         =   @"";
    passwordTxt.secureTextEntry     =   YES;
    passwordTxt.delegate            =   self;
    passwordTxt.backgroundColor     =   [UIColor blackColor];
    passwordTxt.layer.cornerRadius  =   22;
    passwordTxt.text               =   [userDefault valueForKey:@"pass"];
    //    passwordTxt.borderStyle = UITextBorderStyleRoundedRect;
    passwordTxt.alpha               =   0.6;
    passwordTxt.textColor           =   [UIColor whiteColor];
    // For adding padding in text field
    UIView *paddingView2            =   [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH*0.04, 20)];
    passwordTxt.leftView            =   paddingView2;
    passwordTxt.leftViewMode        =   UITextFieldViewModeAlways;
    passwordTxt.font                =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.05];
    [self.view addSubview:passwordTxt];
    
    UIButton *rememberBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.10, SCREEN_HEIGHT*0.45, SCREEN_WIDTH*0.04, SCREEN_WIDTH*0.04)];
    [rememberBtn setImage:[UIImage imageNamed:CHECKBOX_UNSELECTED_IMG] forState:UIControlStateNormal];
    if (isRememberMe == 1) {
        [rememberBtn setImage:[UIImage imageNamed:CHECKBOX_SELECTED_IMG] forState:UIControlStateNormal];
    }
    [rememberBtn addTarget:self action:@selector(rememberBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:rememberBtn];
    
    UILabel *rememberLbl         =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.15, SCREEN_HEIGHT*0.43, SCREEN_WIDTH*0.35, SCREEN_HEIGHT*0.06)];
    rememberLbl.backgroundColor  =   [UIColor clearColor];
    rememberLbl.text             =   REMEMBER_LBL_TXT;
    rememberLbl.textColor        =   [UIColor whiteColor];
    rememberLbl.textAlignment    =   NSTextAlignmentLeft;
    rememberLbl.font             =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.045];
    [self.view addSubview:rememberLbl];
    
    UIButton *forgotPasswordBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.48, SCREEN_HEIGHT*0.43, SCREEN_WIDTH*0.45, SCREEN_HEIGHT*0.06)];
    [forgotPasswordBtn setBackgroundColor:[UIColor clearColor]];
    [forgotPasswordBtn setTitle:FORGOT_PASSWORD_TXT forState:UIControlStateNormal];
    [forgotPasswordBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    forgotPasswordBtn.titleLabel.font   =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.045];
    [forgotPasswordBtn addTarget:self action:@selector(forgotPassword) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:forgotPasswordBtn];
    
    float topMargin     =   SCREEN_HEIGHT*0.50;
    
    NSArray *titleArr   =   [[NSArray alloc]initWithObjects:@"Log in",@"Sign up",@"", nil];
    for (int i=0; i<3; i++) {
        UIButton *optionBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.10, topMargin, SCREEN_WIDTH*0.80, SCREEN_HEIGHT*0.08)];
        [optionBtn setBackgroundImage:[UIImage imageNamed:BUTTON_IMAGE] forState:UIControlStateNormal];
        [optionBtn setTitle:[titleArr objectAtIndex:i] forState:UIControlStateNormal];
        if (i==2) {
            optionBtn.frame     =   CGRectMake(SCREEN_WIDTH*0.15, topMargin, SCREEN_WIDTH*0.70, SCREEN_HEIGHT*0.08);
            [optionBtn setBackgroundImage:[UIImage imageNamed:FB_BUTTON_IMAGE] forState:UIControlStateNormal];
        }
        
        optionBtn.tag   =   i+1;
        [optionBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        optionBtn.titleLabel.font   =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.055];
        [optionBtn addTarget:self action:@selector(optionBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:optionBtn];
        
        topMargin   +=  SCREEN_HEIGHT*0.09;
    }
    
    //    emailTxt.text       =   @"sag@gmail.com";
    //    passwordTxt.text    =   @"sag";
    
}

#pragma mark Mathod to be called when sign in , sign up or login with facebook button is clicked
-(void)optionBtnClicked:(UIButton *)sender{
    if (sender.tag == 1) {
        //        NSLog(@"Login");
        if ([self validateForm]){
            if([common reachabilityFunction]) {
                NSString *urlStr = [NSString stringWithFormat:@"%@login.php?email=%@&password=%@",API_URL,emailTxt.text,passwordTxt.text];
                
                urlStr  =   [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                
                responseData   =   [[NSMutableData alloc]init];
                
                request        =   [NSURLRequest requestWithURL:[NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] cachePolicy:NSURLCacheStorageAllowed timeoutInterval:5];
                
                connect        =   [[NSURLConnection alloc] initWithRequest:request delegate:self];
                
                [connect start];
            }else{
                [common showAlert:@"Warning" _message:@"No internet connection."];
            }
        }
    }else if (sender.tag == 2){
        //        NSLog(@"sign Up");
        RegisterViewController *signUpVC  =   [[RegisterViewController alloc]init];
        [self.navigationController pushViewController:signUpVC animated:NO];
    }else{
        //        NSLog(@"fb");
    }
}

#pragma mark Method to validate sign in form
-(BOOL)validateForm{
    if (emailTxt.text.length <= 0) {
        [common showAlert:@"Warning" _message:@"Please enter email"];
        return false;
    }else if (passwordTxt.text.length <= 0){
        [common showAlert:@"Warning" _message:@"Please enter password"];
        return false;
    }else if (![common validateEmail:emailTxt.text]){
        [common showAlert:@"Warning" _message:@"Please enter valid email"];
        return false;
    }
    return true;
}

#pragma mark Method to be clicked when forgot password is tapped
-(void)forgotPassword{
    NSLog(@"forgotPassword");
    // Show alert view to enter budget
    UIAlertView *alerView = [[UIAlertView alloc]initWithTitle:@"Forgot Password?" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Cancel",@"Add", nil];
    alerView.alertViewStyle =   UIAlertViewStylePlainTextInput;
    UITextField *textField  =   [alerView textFieldAtIndex:0];
    textField.delegate      =   self;
    textField.placeholder   =   @"Please enter your email";
    [alerView show];
}

#pragma mark AlertView Delegate method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==1) {
        NSString *email = [alertView textFieldAtIndex:0].text;
        NSString *urlStr = [NSString stringWithFormat:@"%@forgetpassword.php?email=%@",API_URL,email];
        
        urlStr  =   [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        responseData2   =   [[NSMutableData alloc]init];
        
        request2        =   [NSURLRequest requestWithURL:[NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] cachePolicy:NSURLCacheStorageAllowed timeoutInterval:5];
        
        connect2        =   [[NSURLConnection alloc] initWithRequest:request2 delegate:self];
        
        [connect2 start];
    }
}

#pragma mark Method to be called when remember me checkbox is tapped
-(void)rememberBtnClicked:(UIButton *)sender{
    //    NSLog(@"rememberBtnClicked");
    if (isRememberMe == 0) {
        [sender setImage:[UIImage imageNamed:CHECKBOX_SELECTED_IMG] forState:UIControlStateNormal];
        isRememberMe    =   1;
    }else{
        [sender setImage:[UIImage imageNamed:CHECKBOX_UNSELECTED_IMG] forState:UIControlStateNormal];
        isRememberMe    =   0;
    }
}

#pragma mark TextField delegate methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark Url connection delegate methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    //    NSLog(@"didReceiveResponse");
    
    if (connection==connect){
        // Login
        [responseData setLength:0];
    }else if (connection==connect2){
        // Login
        [responseData2 setLength:0];
    }
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    //    NSLog(@"didFailWithError");
    
    if(responseData==nil){
        [common showAlert:@"Error" _message:@"Server Problem"];
    }else if(responseData2==nil){
        [common showAlert:@"Error" _message:@"Server Problem"];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    if(connection==connect){
        [responseData appendData:data];
    }else if(connection==connect2){
        [responseData2 appendData:data];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    if (connection==connect){
        NSError* error;
        
        NSMutableDictionary *reviewsDict = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        
        //        NSLog(@"DICT :: %@",reviewsDict);
        
        if ([[reviewsDict valueForKey:@"success"] integerValue] == 1){
            //            NSLog(@"Loged in");
            [userDefault setValue:[reviewsDict valueForKey:@"returnid"] forKey:@"userId"];
            if (isRememberMe) {
                [userDefault setInteger:1 forKey:@"rememberCredentials"];
                [userDefault setValue:emailTxt.text forKey:@"email"];
                [userDefault setValue:passwordTxt.text forKey:@"pass"];
            }else{
                [userDefault setInteger:0 forKey:@"rememberCredentials"];
                [userDefault setValue:@"" forKey:@"email"];
                [userDefault setValue:@"" forKey:@"pass"];
            }
            [userDefault synchronize];
            
            UIViewController *viewController1   =   [[WeddingViewController alloc] init];
            UIViewController *viewController2   =   [[SupplierViewController alloc] init];
            UIViewController *viewController3   =   [[OfferViewController alloc] init];
            UIViewController *viewController4   =   [[ShowsViewController alloc] init];
            UIViewController *viewController5   =   [[AboutViewController alloc] init];
            
            viewController1.tabBarItem.image    =   [UIImage imageNamed:WEDDING_TAB_IMAGE];
            viewController2.tabBarItem.image    =   [UIImage imageNamed:SUPPLIER_TAB_IMAGE];
            viewController3.tabBarItem.image    =   [UIImage imageNamed:OFFERS_TAB_IMAGE];
            viewController4.tabBarItem.image    =   [UIImage imageNamed:SHOWS_TAB_IMAGE];
            viewController5.tabBarItem.image    =   [UIImage imageNamed:ABOUT_TAB_IMAGE];
            
            viewController1.tabBarItem.title    =   @"Wedding";
            viewController2.tabBarItem.title    =   @"Suppliers";
            viewController3.tabBarItem.title    =   @"Offers";
            viewController4.tabBarItem.title    =   @"Shows";
            viewController5.tabBarItem.title    =   @"About";
            
            //    UINavigationController *nav=[[UINavigationController alloc] initWithRootViewController:viewController3];
            
            self.tabBarController   =   [[UITabBarController alloc] init];
            self.tabBarController.viewControllers = @[viewController1, viewController2, viewController3,viewController4,viewController5];
            
            [self.navigationController pushViewController:self.tabBarController animated:NO];
            
        }else{
            //            NSLog(@"Ooops");
            [common showAlert:@"Warning" _message:@"Oops! An error occurred."];
        }
    }else if (connection==connect2){
        NSError* error;
        
        NSMutableDictionary *reviewsDict = [NSJSONSerialization JSONObjectWithData:responseData2 options:kNilOptions error:&error];
        
        //        NSLog(@"DICT :: %@",reviewsDict);
        if ([[reviewsDict valueForKey:@"success"] integerValue] == 1){
            [common showAlert:@"Info" _message:@"Your Password Has Been Sent To Your Email Address."];
        }else{
            [common showAlert:@"Warning" _message:@"Oops! An error occurred."];
        }
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
