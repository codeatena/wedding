//
//  MapViewController.h
//  Wedding
//
//  Created by apple on 10/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MapViewController : UIViewController<MKMapViewDelegate>{
    NSString            *address;
    MKMapView           *myMapView;
    NSMutableArray      *annotationArray;
    MKPointAnnotation   *point;
}

- (id)initWithCustomData:(NSString *)_address;

@end
