//
//  RegisterViewController.m
//  Wedding
//
//  Created by apple on 07/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import "RegisterViewController.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    //    self.navigationController.navigationBarHidden = NO;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    isTnCAccepted               =   0;
    
    UIImageView *bgImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    bgImageView.image           =   [UIImage imageNamed:REGISTER_BG_IMG];
    [self.view addSubview:bgImageView];
    
    UIImageView *topImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT*0.08)];
    topImageView.image           =   [UIImage imageNamed:TOP_IMG];
    [self.view addSubview:topImageView];
    
    UIImageView *titleImageView    =   [[UIImageView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT*0.08, SCREEN_WIDTH, SCREEN_HEIGHT*0.065)];
    [titleImageView setBackgroundColor:[UIColor colorWithRed:197.0f/255.0f green:105.0f/255.0f blue:130.0f/255.0f alpha:1.0f]];
    titleImageView.alpha            =   0.7;
    [self.view addSubview:titleImageView];
    
    UILabel *titleLbl           =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.10, SCREEN_HEIGHT*0.08, SCREEN_WIDTH*0.90, SCREEN_HEIGHT*0.06)];
    titleLbl.backgroundColor    =   [UIColor clearColor];
    titleLbl.text               =   @"Register";
    titleLbl.textColor          =   [UIColor whiteColor];
    titleLbl.textAlignment      =   NSTextAlignmentLeft;
    titleLbl.font               =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.06];
    [self.view addSubview:titleLbl];
    
    contentScroll                   =   [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    contentScroll.backgroundColor   =   [UIColor clearColor];
    [self.view addSubview:contentScroll];
    
    UIButton *backBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.03, SCREEN_HEIGHT*0.09,SCREEN_WIDTH*0.05,SCREEN_HEIGHT*0.04)];
    [backBtn setBackgroundImage:[UIImage imageNamed:BACK_BUTTON_IMG] forState:UIControlStateNormal];
    [backBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    
    optionTitleArr     =   [[NSArray alloc]initWithObjects:@"First Name",@"Last Name",@"Email",@"Password",@"Confirm Password", nil];
    
    float   topMargin   =   SCREEN_HEIGHT*0.15;
    for (int i=0; i<5; i++) {
        
        UILabel *optionTitle         =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.10, topMargin, SCREEN_WIDTH*0.80, SCREEN_HEIGHT*0.06)];
        optionTitle.backgroundColor  =   [UIColor clearColor];
        optionTitle.text             =   [optionTitleArr objectAtIndex:i];
        optionTitle.textColor        =   [UIColor blackColor];
        optionTitle.textAlignment    =   NSTextAlignmentLeft;
        optionTitle.font             =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.05];
        [contentScroll addSubview:optionTitle];
        
        topMargin   +=  SCREEN_HEIGHT*0.06;
        
        UITextField *optionTxt       =   [[UITextField alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.10, topMargin, SCREEN_WIDTH*0.80, SCREEN_HEIGHT*0.06)];
        if (i>2) {
            optionTxt.secureTextEntry = YES;
        }
        optionTxt.placeholder        =   @"";
        optionTxt.delegate           =   self;
        optionTxt.tag                =   i+1;
        optionTxt.backgroundColor    =   [UIColor blackColor];
        optionTxt.layer.cornerRadius =   22;
        optionTxt.font               =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.05];
        optionTxt.alpha              =   0.6;
        optionTxt.textColor          =   [UIColor whiteColor];
        // For adding padding in text field
        UIView *paddingView1         =   [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH*0.04, 20)];
        optionTxt.leftView           =   paddingView1;
        optionTxt.leftViewMode       =   UITextFieldViewModeAlways;
        [contentScroll addSubview:optionTxt];
        
        topMargin   +=  SCREEN_HEIGHT*0.06;
    }
    
    UIButton *termBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.10, topMargin+SCREEN_HEIGHT*0.02, SCREEN_WIDTH*0.04, SCREEN_WIDTH*0.04)];
    [termBtn setImage:[UIImage imageNamed:CHECKBOX_UNSELECTED_IMG] forState:UIControlStateNormal];
    [termBtn addTarget:self action:@selector(termBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [contentScroll addSubview:termBtn];
    
    UILabel *termLbl         =   [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.15, topMargin, SCREEN_WIDTH*0.25, SCREEN_HEIGHT*0.06)];
    termLbl.backgroundColor  =   [UIColor clearColor];
    termLbl.text             =   @"I accept the";
    termLbl.textColor        =   [UIColor blackColor];
    termLbl.textAlignment    =   NSTextAlignmentLeft;
    termLbl.font             =   [UIFont systemFontOfSize:SCREEN_WIDTH*0.045];
    [contentScroll addSubview:termLbl];
    
    UIButton *tncBtn        =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.40, topMargin+2, SCREEN_WIDTH*0.50, SCREEN_HEIGHT*0.06)];
    tncBtn.backgroundColor  =   [UIColor clearColor];
    [tncBtn setTitle:@"Terms And Conditions" forState:UIControlStateNormal];
    [tncBtn setTitleColor:[UIColor colorWithRed:64.0f/255.0f green:170.0f/255.0f blue:163.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    tncBtn.titleLabel.font   =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.048];
    tncBtn.titleLabel.textAlignment =   NSTextAlignmentLeft;
    [tncBtn addTarget:self action:@selector(termAndConditionBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    [contentScroll addSubview:tncBtn];
    
    topMargin               +=  SCREEN_HEIGHT*0.07;
    
    UIButton *privacyLbl         =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.10, topMargin, SCREEN_WIDTH*0.35, SCREEN_HEIGHT*0.06)];
    privacyLbl.backgroundColor  =   [UIColor clearColor];
    [privacyLbl setTitle:@"Privacy Policy" forState:UIControlStateNormal];
    [privacyLbl setTitleColor:[UIColor colorWithRed:64.0f/255.0f green:170.0f/255.0f blue:163.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    privacyLbl.titleLabel.textAlignment    =   NSTextAlignmentLeft;
    privacyLbl.titleLabel.font             =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.048];
    [tncBtn addTarget:self action:@selector(showPrivacyPolicy) forControlEvents:UIControlEventTouchUpInside];
    [contentScroll addSubview:privacyLbl];
    
    topMargin               +=  SCREEN_HEIGHT*0.07;
    
    UIButton *optionBtn           =   [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH*0.10, topMargin, SCREEN_WIDTH*0.80, SCREEN_HEIGHT*0.08)];
    [optionBtn setBackgroundImage:[UIImage imageNamed:BUTTON_IMAGE] forState:UIControlStateNormal];
    [optionBtn setTitle:REGISTER_BTN_TITLE forState:UIControlStateNormal];
    [optionBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    optionBtn.titleLabel.font   =   [UIFont boldSystemFontOfSize:SCREEN_WIDTH*0.055];
    [optionBtn addTarget:self action:@selector(doRegisterUser) forControlEvents:UIControlEventTouchUpInside];
    [contentScroll addSubview:optionBtn];
    
}

#pragma mark Back Button Method
-(void)backButtonClicked{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark Method to show term and conditions
-(void)termAndConditionBtnClicked{
    NSLog(@"Show Term and condition");
}
#pragma mark Method to show privacy policy
-(void)showPrivacyPolicy{
    NSLog(@"showPrivacyPolicy");
}

#pragma mark Term and condition check box tapped
-(void)termBtnClicked:(UIButton *)sender{
    NSLog(@"Term and condition Check box tapped");
    if (isTnCAccepted == 0) {
        [sender setImage:[UIImage imageNamed:CHECKBOX_SELECTED_IMG] forState:UIControlStateNormal];
        isTnCAccepted    =   1;
    }else{
        [sender setImage:[UIImage imageNamed:CHECKBOX_UNSELECTED_IMG] forState:UIControlStateNormal];
        isTnCAccepted    =   0;
    }
}

#pragma mark Method to be called when register button is clicked
-(void)doRegisterUser{
    if ([self validateForm]) {
        if([common reachabilityFunction]) {
            NSString *urlStr = [NSString stringWithFormat:@"%@create_user.php?firstname=%@&lastname=%@&email=%@&password=%@",API_URL,((UITextField *)[self.view viewWithTag:1]).text,((UITextField *)[self.view viewWithTag:2]).text,((UITextField *)[self.view viewWithTag:3]).text,((UITextField *)[self.view viewWithTag:4]).text];
            
            urlStr  =   [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            
            responseData   =   [[NSMutableData alloc]init];
            
            request        =   [NSURLRequest requestWithURL:[NSURL URLWithString:[urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] cachePolicy:NSURLCacheStorageAllowed timeoutInterval:5];
            
            connect        =   [[NSURLConnection alloc] initWithRequest:request delegate:self];
            
            [connect start];
        }else{
            [common showAlert:@"Warning" _message:@"No internet connection."];
        }
    }
}

#pragma mark Method to validate register form
-(BOOL)validateForm{
    for (int i=0; i<5; i++) {
        UITextField *temp   =   (UITextField *)[self.view viewWithTag:i+1];
        if (temp.text.length <= 0) {
            [common showAlert:@"Warning" _message:[NSString stringWithFormat:@"Please enter %@",[optionTitleArr objectAtIndex:i]]];
            return false;
        }else if (i==2){
            if (![common validateEmail:temp.text]) {
                [common showAlert:@"Warning" _message:@"Please enter valid email"];
                return false;
            }
        }
    }
    UITextField *tempPass    =   (UITextField *)[self.view viewWithTag:4];
    UITextField *tempCPass   =   (UITextField *)[self.view viewWithTag:5];
    if (![tempPass.text isEqualToString:tempCPass.text]) {
        [common showAlert:@"Warning" _message:@"Password and confirm password must be same."];
        return false;
    }
    if (!isTnCAccepted) {
        [common showAlert:@"Warning" _message:@"Please accept Term and Conditions"];
        return false;
    }
    return true;
}

#pragma mark TextField delegate methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark Method to be called when keyboard hide
-(void)keyboardDidHide:(NSNotification *)notification{
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    
    contentScroll.frame    =   CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    
    [UIView commitAnimations];
    
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    CGRect Frame    =   contentScroll.frame;
    if(textField.tag >= 3){
        Frame= CGRectMake(contentScroll.frame.origin.x, -SCREEN_HEIGHT*0.25, contentScroll.frame.size.width, contentScroll.frame.size.height);
    }
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.4];
    
    contentScroll.frame =   Frame;
    
    [UIView commitAnimations];
}

#pragma mark Url connection delegate methods
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    //    NSLog(@"didReceiveResponse");
    
    if (connection==connect){
        // Login
        [responseData setLength:0];
    }
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    //    NSLog(@"didFailWithError");
    
    if(responseData==nil){
        [common showAlert:@"Error" _message:@"Server Problem"];
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    if(connection==connect){
        [responseData appendData:data];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    if (connection==connect){
        NSError* error;
        
        //        NSString *responseString = [[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
        
        //        NSLog(@"Response :::: %@",responseString);
        
        NSMutableDictionary *reviewsDict = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        
        //        NSLog(@"DICT :: %@",reviewsDict);
        
        if ([[reviewsDict valueForKey:@"success"] integerValue] == 1){
            //            NSLog(@"User Registered");
            [common showAlert:@"Message" _message:@"Registration Done."];
            [self.navigationController popViewControllerAnimated:NO];
            
        }else{
            //            NSLog(@"Ooops");
            [common showAlert:@"Warning" _message:@"Oops! An error occurred."];
        }
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
