//
//  SupplierDetailsViewController.h
//  Wedding
//
//  Created by apple on 07/10/14.
//  Copyright (c) 2014 apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SupplierDetailsViewController : UIViewController<NSURLConnectionDelegate,NSURLConnectionDataDelegate,UITableViewDelegate,UITableViewDataSource>{
    int                     categoryId;
    UITableView             *tableV;
    NSMutableData           *responseData;
    NSURLRequest            *request;
    NSURLConnection         *connect;
    NSMutableArray          *dataArray;
}

- (id)initWithCustomData:(int)catId;

@end
