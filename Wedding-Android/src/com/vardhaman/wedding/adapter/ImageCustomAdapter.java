package com.vardhaman.wedding.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.vardhaman.wedding.R;
import com.vardhaman.wedding.model.SupplierModel;
import com.vardhaman.wedding.restinteraction.RESTInteraction;
import com.vardhaman.wedding.util.Util;

public class ImageCustomAdapter extends ArrayAdapter<SupplierModel> {
	private Context context;
	private ArrayList<SupplierModel> items;
	private int layoutId;
	private RESTInteraction restInteraction;

	
	public ImageCustomAdapter(Context context, int gridSingle,
			ArrayList<SupplierModel> getCategoryList) {
		super(context, gridSingle, getCategoryList);
		this.context = context;
		layoutId = gridSingle;
		items = getCategoryList;
	}
	
	static class ViewHolder {
		ImageView categoryImage;
		public TextView categoryName;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		View rowView = convertView;
		final ViewHolder holder;
		if (rowView == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			rowView = inflater.inflate(layoutId, null, true);
			holder = new ViewHolder();
			holder.categoryImage = (ImageView) rowView.findViewById(R.id.grid_image);
			
			holder.categoryName = (TextView) rowView.findViewById(R.id.grid_text);
			
			rowView.setTag(holder);
		} else {
			holder = (ViewHolder) rowView.getTag();
		}
		
		Log.e("supplier categoryimage", Util.getCategoryList.get(position).getCategoryImage());
		Util.loadImage(holder.categoryImage,
				Util.getCategoryList.get(position).getCategoryImage(),
				0);
		
		holder.categoryName.setText(Util.getCategoryList.get(position).getCategoryName());
	
		return rowView;
	}
}
