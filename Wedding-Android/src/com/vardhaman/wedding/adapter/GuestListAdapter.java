package com.vardhaman.wedding.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.vardhaman.wedding.ConnectionDetector;
import com.vardhaman.wedding.MyWeddingGroup;
import com.vardhaman.wedding.R;
import com.vardhaman.wedding.fragment.GuestListFragment;
import com.vardhaman.wedding.model.GuestList;
import com.vardhaman.wedding.restinteraction.RESTInteraction;
import com.vardhaman.wedding.util.ApiUrl;
import com.vardhaman.wedding.util.Util;

public class GuestListAdapter extends ArrayAdapter<GuestList> {

	private Context context;
	private ArrayList<GuestList> items;
	private int layoutId;
	private RESTInteraction restInteraction;
	Boolean isInternetPresent = false;
	ConnectionDetector cd;
	Boolean check = false;

	public GuestListAdapter(Context context, int textViewResourceId,
			ArrayList<GuestList> list) {
		super(context, textViewResourceId, list);
		restInteraction = RESTInteraction.getInstance(context);
		cd = new ConnectionDetector(context);
		isInternetPresent = cd.isConnectingToInternet();
		this.context = context;
		layoutId = textViewResourceId;
		this.items = list;
		
	}

	static class ViewHolder {
		ImageView starImages;
		public CheckBox checkRelation;
		public TextView arrived;
		public TextView guestName;
		public ImageView guests_pic;
		public ImageView extend_pic;

	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		View rowView = convertView;
		final ViewHolder holder;
		if (rowView == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			rowView = inflater.inflate(layoutId, null, true);
			holder = new ViewHolder();
			holder.starImages = (ImageView) rowView.findViewById(R.id.starImages);
			holder.checkRelation = (CheckBox) rowView.findViewById(R.id.checkRelation);
			holder.arrived = (TextView) rowView.findViewById(R.id.arrived);
			holder.guestName = (TextView) rowView.findViewById(R.id.guestName);
			holder.guests_pic = (ImageView) rowView.findViewById(R.id.guests_pic);
			holder.extend_pic = (ImageView) rowView.findViewById(R.id.extentPic);
			rowView.setTag(holder);
		} else {
			holder = (ViewHolder) rowView.getTag();
		}
		new GiftListAsync().execute();
		if(items.get(position).getFavouite().equalsIgnoreCase("favourite"))
		{
			holder.starImages.setImageResource(R.drawable.star);
		}
		else{
			holder.starImages.setImageResource(R.drawable.starwhite);
		}

		holder.extend_pic.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				final Dialog dialog = new Dialog(context);
				dialog.setContentView(R.layout.dialog_add_gift);
				dialog.setTitle("Add Gift");
				final EditText addGift;
				Button add, cancel;
				addGift = (EditText)dialog.findViewById(R.id.addGift);
				add = (Button)dialog.findViewById(R.id.btnAddGift);
				cancel = (Button)dialog.findViewById(R.id.btnCancelGift);

				add.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

						String gift = addGift.getText().toString();
						String guestId = items.get(position).getId();
						if (guestId != null && !guestId.matches("") && gift.length()>0) {
						for (int i = 0; i < Util.getGiftDetail.size(); i++) {
							if (guestId.equalsIgnoreCase(Util.getGiftDetail.get(i).getId())) {
								check = true;
								showAlertDialog(guestId, gift);
								break;
							}
						}
						
						if (check == false) {
							new AddGiftAsync(guestId, gift).execute();
							
						}
						check = false;
						dialog.dismiss();
						}else {
							Toast.makeText(context, "Please add gift.", Toast.LENGTH_SHORT).show();
						}

					}
				});

				cancel.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						dialog.dismiss();
					}
				});

				dialog.show();

			}
		});

		Util.loadImage(holder.guests_pic,
				ApiUrl.imageDownloadURL + items.get(position).getProfile_pic(),
				0);
		// holder.starImages.setImageResource(items.get(position).getStarIcon());
		// holder.checkRelation.setText(items.get(position).getCheckBox());
		holder.checkRelation.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!isInternetPresent) {
					Toast.makeText(context, "No Internet Connection..Please try later..!", Toast.LENGTH_SHORT).show();
				}else {
					if (holder.checkRelation.isChecked()) {
						new CheckStatusAsync().execute(items.get(position).getId(),"true");
					}
					else {
						new CheckStatusAsync().execute(items.get(position).getId(),"false");
					}
				}
			}
		} );
		if (items.get(position).getStatus().equalsIgnoreCase("false")) {
			holder.arrived.setText("Waiting");
			holder.checkRelation.setChecked(false);
		} else {
			holder.arrived.setText("Arrived");
			holder.checkRelation.setChecked(true);
		}

		holder.guestName.setText(items.get(position).getFistname());

		return rowView;
	}

	private class CheckStatusAsync extends AsyncTask<String, Void, String> {



		@Override
		protected String doInBackground(String... params) {

			return restInteraction.checkStatus(ApiUrl.checkStatusURL + "id="
					+ params[0]+"&status="+params[1]);
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);


			if (result.equals("Successful")) {
			} else {
				//				Toast.makeText(context, "Please check internet connection.", Toast.LENGTH_SHORT).show();
			}
			((MyWeddingGroup)context).guestListFragment.new GuestListAsync(GuestListFragment.spinnerString2WithoutSpace).execute();
		}
	}

	public class AddGiftAsync extends AsyncTask<String, Void, String> {
		String guestId;
		String gift;
		ProgressDialog progressDialog;

		public AddGiftAsync(String guestId, String gift) {
			this.guestId = guestId;
			this.gift = gift;
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(context);
			progressDialog.setCancelable(false);
			progressDialog.setMessage("Please wait...");
			progressDialog.show();

		}

		@Override
		protected String doInBackground(String... params) {

			return restInteraction.addGift(ApiUrl.AddGiftUrl + "guest_id="
					+ guestId + "&user_id=" +  Util.loginData.getUserId() + "&grift=" + gift);
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			progressDialog.dismiss();

			if (result.equals("Successful")) {
				Toast.makeText(context, "Gift added successfully.", Toast.LENGTH_SHORT).show();
			} else {
				//				Toast.makeText(context, "Please check internet connection.", Toast.LENGTH_SHORT).show();
			}
			((MyWeddingGroup)context).guestListFragment.new GuestListAsync(GuestListFragment.spinnerString2WithoutSpace).execute();
		}
	}

	void showAlertDialog(final String guestId, final String gift){
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

		// set title
		alertDialogBuilder.setTitle("Confirmation!");

		// set dialog message
		alertDialogBuilder
		.setMessage("Are you sure? We see you have already entered a gift earlier.")
		.setCancelable(false)
		.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				// if this button is clicked, close
				// current activity
				new AddGiftAsync(guestId, gift).execute();
				dialog.dismiss();
			}
		})
		.setNegativeButton("No",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				// if this button is clicked, just close
				// the dialog box and do nothing
				dialog.cancel();
			}
		});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}
	
	public class GiftListAsync extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {

			return restInteraction.getAllGift(ApiUrl.GetGiftsUrl
					+ "user_id=" + Util.loginData.getUserId());
		}

		
	}

}
