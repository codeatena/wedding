package com.vardhaman.wedding.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.vardhaman.wedding.ConnectionDetector;
import com.vardhaman.wedding.R;
import com.vardhaman.wedding.model.GiftListModel;
import com.vardhaman.wedding.restinteraction.RESTInteraction;
import com.vardhaman.wedding.util.ApiUrl;
import com.vardhaman.wedding.util.Util;

public class GiftListAdapter extends ArrayAdapter<GiftListModel>{
	private Context context;
	private ArrayList<GiftListModel> items;
	private int layoutId;
	private RESTInteraction restInteraction;
	Boolean isInternetPresent = false;
	ConnectionDetector cd;

	public GiftListAdapter(Context context, int giftlistItem,
			ArrayList<GiftListModel> getGiftDetail) {
		super(context, giftlistItem, getGiftDetail);
		this.context = context;
		layoutId = giftlistItem;
		items = getGiftDetail;
		restInteraction = RESTInteraction.getInstance(context);
		
	}
	
	static class ViewHolder {
		ImageView starImages;
		public TextView relation;
		public TextView guestName;
		public ImageView guests_pic;
		public ImageView gift_pic;

	}

	public View getView(final int position, View convertView, ViewGroup parent) {

		View rowView = convertView;
		final ViewHolder holder;
		if (rowView == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			rowView = inflater.inflate(layoutId, null, true);
			holder = new ViewHolder();
			holder.guests_pic = (ImageView)rowView.findViewById(R.id.pic);
			holder.starImages = (ImageView)rowView.findViewById(R.id.fav_icon);
			holder.gift_pic = (ImageView)rowView.findViewById(R.id.gift_pic);
			holder.guestName = (TextView)rowView.findViewById(R.id.name);
			holder.relation = (TextView)rowView.findViewById(R.id.relations);
			
			rowView.setTag(holder);
		} else {
			holder = (ViewHolder) rowView.getTag();
		}
		

		Util.loadImage(holder.guests_pic,ApiUrl.imageDownloadURL + items.get(position).getProfilePic(),0);
		if (items.get(position).getFavourite().equalsIgnoreCase("favourite")) {
			holder.starImages.setImageResource(R.drawable.star);
		}
		holder.guestName.setText(items.get(position).getFirstName());
		holder.relation.setText(items.get(position).getGift() + " - " + items.get(position).getRelation());
		// holder.starImages.setImageResource(items.get(position).getStarIcon());
		// holder.checkRelation.setText(items.get(position).getCheckBox());
		return rowView;
	}
}
