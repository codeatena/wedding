package com.vardhaman.wedding.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vardhaman.wedding.R;
import com.vardhaman.wedding.SupplierDetailActivity;
import com.vardhaman.wedding.VouchersActivity;
import com.vardhaman.wedding.model.CategoryModel;
import com.vardhaman.wedding.util.Util;


public class CategoryListAdapter extends ArrayAdapter<CategoryModel> {

	private Context context;
	private ArrayList<CategoryModel> items;
	private int layoutId;
	int check;


	public CategoryListAdapter(Context context, int textViewResourceId,
			ArrayList<CategoryModel> list) {
		super(context, textViewResourceId, list);
	
		this.context = context;
		layoutId = textViewResourceId;
		this.items = list;
	}

	static class ViewHolder {
		public TextView tvSupplierCity;
		public TextView tvSupplierName;
		public ImageView ivCategoryItem;
		public ImageView offerImage;
		public LinearLayout clickable;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		View rowView = convertView;
		final ViewHolder holder;
		if (rowView == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			rowView = inflater.inflate(layoutId, null, true);
			holder = new ViewHolder();
			
			holder.clickable = (LinearLayout)rowView.findViewById(R.id.clickableText);
			holder.tvSupplierCity = (TextView) rowView.findViewById(R.id.tvSupplierCity);
			holder.tvSupplierName = (TextView) rowView.findViewById(R.id.tvSupplierName);
			holder.ivCategoryItem = (ImageView) rowView.findViewById(R.id.ivCategoryItem);
			holder.offerImage = (ImageView)rowView.findViewById(R.id.offerImage);
			rowView.setTag(holder);
		} else {
			holder = (ViewHolder) rowView.getTag();
		}
		
		holder.clickable.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				context.startActivity(new Intent(context, SupplierDetailActivity.class).putExtra("supplierId", items.get(position).getId()));
			
			}
		});
		
		holder.tvSupplierCity.setText(items.get(position).getCity());
		

		Util.loadImage(holder.ivCategoryItem,items.get(position).getImage(),0);

		holder.tvSupplierName.setText(items.get(position).getSupplerName());
		if (items.get(position).getOffers()==1) {
			holder.offerImage.setImageResource(R.drawable.offer_icon);
		}
		holder.offerImage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				check = 1;
				context.startActivity(new Intent(context , VouchersActivity.class).putExtra("supplierId", items.get(position).getsupplierId()));
				
			}
		});

		return rowView;
	}

	

}
