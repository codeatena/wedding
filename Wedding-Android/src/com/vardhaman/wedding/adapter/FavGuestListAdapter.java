package com.vardhaman.wedding.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.vardhaman.wedding.MyWeddingGroup;
import com.vardhaman.wedding.R;
import com.vardhaman.wedding.model.FavModel;
import com.vardhaman.wedding.model.GuestList;
import com.vardhaman.wedding.restinteraction.RESTInteraction;
import com.vardhaman.wedding.util.ApiUrl;
import com.vardhaman.wedding.util.Util;

public class FavGuestListAdapter extends ArrayAdapter<FavModel> {

	private Context context;
	private ArrayList<FavModel> items;
	private int layoutId;
	private RESTInteraction restInteraction;

	public FavGuestListAdapter(Context context, int textViewResourceId,
			ArrayList<FavModel> list) {
		super(context, textViewResourceId, list);
		restInteraction = RESTInteraction.getInstance(context);
		this.context = context;
		layoutId = textViewResourceId;
		this.items = list;
	}

	static class ViewHolder {
		ImageView ivFGLImage;
		public TextView tvFGLName;
		public ImageView ivFGLImageStar;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		View rowView = convertView;
		final ViewHolder holder;
		if (rowView == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			rowView = inflater.inflate(layoutId, null, true);
			holder = new ViewHolder();
			holder.ivFGLImage = (ImageView) rowView
					.findViewById(R.id.ivFGLImage);
			holder.tvFGLName = (TextView) rowView.findViewById(R.id.tvFGLName);
			holder.ivFGLImageStar = (ImageView) rowView
					.findViewById(R.id.ivFGLImageStar);
			rowView.setTag(holder);
		} else {
			holder = (ViewHolder) rowView.getTag();
		}
		
			holder.ivFGLImageStar.setImageResource(R.drawable.star);
			Util.loadImage(holder.ivFGLImage, items.get(position).getSupplierImage(), 0);

			holder.tvFGLName.setText(items.get(position).getSupplierName());
		

		return rowView;
	}


}
