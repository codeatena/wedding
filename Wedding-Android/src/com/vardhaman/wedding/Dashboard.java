package com.vardhaman.wedding;




import java.lang.reflect.Field;

import android.app.ActionBar;
import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

@SuppressWarnings("deprecation")
public class Dashboard extends TabActivity{
	TabHost tabHost;
	ActionBar actionBar;
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dashboard);
		actionBar = getActionBar();
		tabHost =(TabHost) findViewById(android.R.id.tabhost);
		tabHost.getTabWidget().setDividerDrawable(null);
		
		//		First TAb
		TabSpec suppliers=tabHost.newTabSpec("My Wedding");
		View tabview = createTabView(tabHost.getContext(),"My Wedding",R.layout.tabs_bg_mywedding);
		Intent mywWddingIntent = new Intent(this, MyWeddingGroup.class);
		suppliers.setContent(mywWddingIntent);
		suppliers.setIndicator(tabview);
		tabHost.addTab(suppliers);
		// Second Tab
		TabSpec vouchers=tabHost.newTabSpec("Suppliers");
		View tabview1 = createTabView(tabHost.getContext(),"Suppliers",R.layout.tabs_bg_suppliers);
		Intent suppliersIntent = new Intent(this, SupplierGroup.class);
		vouchers.setContent(suppliersIntent);
		vouchers.setIndicator(tabview1);
		tabHost.addTab(vouchers);
		// Third Tab
		TabSpec fairs=tabHost.newTabSpec("Offers");
		View tabview2 = createTabView(tabHost.getContext(),"Offers",R.layout.tabs_bg_vouchers);
		Intent vouchersIntent = new Intent(this, VouchersActivity.class);
		fairs.setContent(vouchersIntent);
		fairs.setIndicator(tabview2);
		tabHost.addTab(fairs);
		// Four Tab
		TabSpec weddingShows=tabHost.newTabSpec("Shows");
		View tabview3 = createTabView(tabHost.getContext(),"My Wedding",R.layout.tabs_bg_shows);
		Intent weddingShowIntent = new Intent(this, ShowsGroup.class);
		weddingShows.setContent(weddingShowIntent);
		weddingShows.setIndicator(tabview3);
		tabHost.addTab(weddingShows);
		// five Tab
		TabSpec about=tabHost.newTabSpec("About");
		View tabview4 = createTabView(tabHost.getContext(),"About",R.layout.tabs_bg_about);
		Intent aboutIntent = new Intent(this, AboutActivity.class);
		about.setContent(aboutIntent);
		about.setIndicator(tabview4);
		tabHost.addTab(about);
		getOverflowMenu();
		
	}
	
	private void getOverflowMenu() {

	     try {
	        ViewConfiguration config = ViewConfiguration.get(this);
	        Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
	        if(menuKeyField != null) {
	            menuKeyField.setAccessible(true);
	            menuKeyField.setBoolean(config, false);
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
	
	@Override
		public boolean onCreateOptionsMenu(Menu menu) {
			// Inflate the menu; this adds items to the action bar if it is present.
			getMenuInflater().inflate(R.menu.my_wedding, menu);
			return true;
		}
	
		@Override
		public boolean onOptionsItemSelected(MenuItem item) {
			// Handle item selection
			switch (item.getItemId()) {
			case R.id.action_settings:
				startActivity(new Intent(Dashboard.this, LoginActivity.class));
				return true;
			
			default:
				return super.onOptionsItemSelected(item);
			}
		}
	private static View createTabView(final Context context, final String text, int layout) {
	    View view = LayoutInflater.from(context).inflate(layout, null);
	    return view;
	}
}
