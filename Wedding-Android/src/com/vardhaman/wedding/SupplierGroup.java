package com.vardhaman.wedding;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.vardhaman.wedding.fragment.CategoryListFragment;
import com.vardhaman.wedding.fragment.SupplierDetailFragment;
import com.vardhaman.wedding.fragment.SupplierFragment;


public class SupplierGroup extends FragmentActivity {
	
	public SupplierFragment supplierFragment;
	public CategoryListFragment categoryListFragment;
	public SupplierDetailFragment supplierDetailFragment;
	public static int lVPosition;
	FragmentManager fManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.follow_group);
		lVPosition = 0;
		fManager = getSupportFragmentManager();
		supplierFragment = new SupplierFragment();
		Bundle bundle = new Bundle();
		bundle.putInt("page", 1);
		supplierFragment.setArguments(bundle);
		loadFragment(R.id.fmLayout, supplierFragment);
	}

	public void loadFragment(int resourceId,Fragment fragment){
		FragmentTransaction ft = fManager.beginTransaction();
		ft.setTransition(android.R.anim.bounce_interpolator);
		ft.add(resourceId, fragment);
		ft.commit();		
	}

	public void unLoadFragment1(){
		fManager.beginTransaction().remove(supplierFragment).commit();
	}
	public void unLoadFragment2(){
		fManager.beginTransaction().remove(categoryListFragment).commit();
	}
	public void unLoadFragment3(){
		fManager.beginTransaction().remove(supplierDetailFragment).commit();
	}
	

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		this.getParent().onBackPressed();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		//		addGuestFragment.onActivityResult(requestCode, resultCode, data);
		super.onActivityResult(requestCode, resultCode, data);
	}


}