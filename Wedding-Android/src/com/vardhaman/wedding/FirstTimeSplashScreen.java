package com.vardhaman.wedding;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class FirstTimeSplashScreen extends Activity implements OnClickListener{
	Button btnEnterHere, btnGotoApp;
	boolean check = false;
	SharedPreferences sharedPreferences;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		Boolean boolean1 = sharedPreferences.getBoolean("check", false);
		if (boolean1) {
			startActivity(new Intent(FirstTimeSplashScreen.this, LoginActivity.class));
			finish();
		}
		setContentView(R.layout.first_splash_layout);
		init();
	}
	void init(){
		
		check = true;
		sharedPreferences.edit()
		.putBoolean("check", check)
		.commit();
		btnEnterHere = (Button)findViewById(R.id.btnEnterHere);
		btnGotoApp = (Button)findViewById(R.id.btnGotoApp);
		btnEnterHere.setOnClickListener(this);
		btnGotoApp.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnEnterHere:
			break;
		case R.id.btnGotoApp:
			startActivity(new Intent(FirstTimeSplashScreen.this, LoginActivity.class));
			finish();
			break;

		default:
			break;
		}

	}
}
