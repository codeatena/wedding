package com.vardhaman.wedding;

import java.net.URLEncoder;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.vardhaman.wedding.restinteraction.RESTInteraction;
import com.vardhaman.wedding.util.ApiUrl;

public class SignUpActivity extends Activity implements OnClickListener{
	EditText etFirstName, etLastName, etEmail, etPassword, etConfirmPassword;
	String firstName, lastName, mail, password, confirmpassword;
	CheckBox cBAccept;
	Button btnRegister;
	private RESTInteraction restInteraction;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activty_signup);
		init();
	}
	
	void init(){
		restInteraction=RESTInteraction.getInstance(this);
		etFirstName = (EditText)findViewById(R.id.etFirstName);
		etLastName = (EditText)findViewById(R.id.etLastName);
		etEmail = (EditText)findViewById(R.id.etEmail);
		etPassword = (EditText)findViewById(R.id.etPassword);
		etConfirmPassword = (EditText)findViewById(R.id.etConfirmPassword);
		cBAccept = (CheckBox)findViewById(R.id.cBAccept);
		btnRegister =(Button)findViewById(R.id.btnRegister);
		btnRegister.setOnClickListener(this);
	}
	private class RegistraionAsync extends AsyncTask<String, Void, String>
	{
		private ProgressDialog progressDialog;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog=new ProgressDialog(SignUpActivity.this);
			progressDialog.setCancelable(false);
			progressDialog.setMessage("Please wait...");
			progressDialog.show();
		}
		@SuppressWarnings("deprecation")
		@Override
		protected String doInBackground(String... params) {
		
			return restInteraction.sendSignUpRequest(ApiUrl.registraionURL+
					"firstname="+URLEncoder.encode(etFirstName.getText().toString().trim())+
					"&lastname="+URLEncoder.encode(etLastName.getText().toString().trim())+
					"&password="+URLEncoder.encode(etPassword.getText().toString().trim())+
					"&email="+URLEncoder.encode(etEmail.getText().toString().trim()));
		}
		@Override
		protected void onPostExecute(String result) {
			
			super.onPostExecute(result);
			progressDialog.dismiss();
			if(result!=null)
			{
				if(result=="Successful")
				{
					Toast.makeText(SignUpActivity.this, "Registration Successful", Toast.LENGTH_SHORT).show();
					startActivity(new Intent(SignUpActivity.this,LoginActivity.class));
				}
				else {
					Toast.makeText(SignUpActivity.this, "Internet connection is slow. Please try again later.",Toast.LENGTH_SHORT).show();
				}
			}
			else {
				Toast.makeText(SignUpActivity.this, "Network Error", Toast.LENGTH_SHORT).show();
			}
		}
	
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btnRegister:
			Validation();
				
			break;

		default:
			break;
		}
	}
	
	public Boolean Validation(){
		Boolean isEmpty=false;
		firstName = etFirstName.getText().toString();
		lastName = etLastName.getText().toString();
		mail = etEmail.getText().toString();
		password = etPassword.getText().toString();
		confirmpassword = etConfirmPassword.getText().toString();
		if (firstName.length() == 0 || lastName.length() == 0 || mail.length() == 0 || password.length() == 0 
				|| confirmpassword.length() == 0) {
			Toast.makeText(SignUpActivity.this, "Please enter all fields", Toast.LENGTH_SHORT).show();
		}else if (!isEmailValid(mail)) {
			isEmpty=true;
			etEmail.setError("Enter valid email!");
		}
		else if (!password.equals(confirmpassword)) {
			Toast.makeText(SignUpActivity.this, "Please check your password", Toast.LENGTH_SHORT).show();
		}else if (!cBAccept.isChecked()) {
			Toast.makeText(SignUpActivity.this, "Please check the Terms and Conditions", Toast.LENGTH_SHORT).show();
		}
		else {
			new RegistraionAsync().execute();
		}
		return false;
		
	}
	boolean isEmailValid(CharSequence email) {
		return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
	}

}
