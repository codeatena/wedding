package com.vardhaman.wedding.fragment;

import com.vardhaman.wedding.MyWeddingGroup;
import com.vardhaman.wedding.R;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class TimeTableDetailsFragment extends Fragment implements OnClickListener{
	Context context;
	View view;
	ImageView backNav;
	TextView back_1;
	int checkCount;
	
	public View onCreateView(LayoutInflater inflater, android.view.ViewGroup container, Bundle savedInstanceState) {
		context = getActivity();
		checkCount = WeddingTimeTableFragment.count;
		if (checkCount==1) {
			view = inflater.inflate(R.layout.tweleve_to_nine_layout, container, false);
			init();
			checkCount=0;
		}else if (checkCount == 2) {
			view = inflater.inflate(R.layout.nine_to_six_layout, container, false);
			init();
			checkCount=0;
		}else if (checkCount == 3) {
			view = inflater.inflate(R.layout.six_to_three_layout, container, false);
			init();
			checkCount=0;
		}else if (checkCount == 4) {
			view = inflater.inflate(R.layout.three_to_two_layout, container, false);
			init();
			checkCount=0;
		}else if (checkCount==5) {
			view = inflater.inflate(R.layout.one_month_before_layout, container, false);
			init();
			checkCount=0;
		}else if (checkCount==6) {
			view = inflater.inflate(R.layout.night_before_wedding_layout, container, false);
			init();
			checkCount=0;
		}else if (checkCount==7 ) {
			view = inflater.inflate(R.layout.on_wedding_day_layout, container, false);
			init();
			checkCount=0;
		}
		
		return view;
	}
	
	void init(){
		backNav = (ImageView)view.findViewById(R.id.back);
		backNav.setOnClickListener(this);
		back_1 = (TextView)view.findViewById(R.id.back_1);
		back_1.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		MyWeddingGroup followGroup = (MyWeddingGroup)context;		
		switch (v.getId()) {
		case R.id.back:
			followGroup.weddingTimeTableFragment = new WeddingTimeTableFragment();
			followGroup.unLoadFragment8();
			followGroup.loadFragment(R.id.fmLayout, followGroup.weddingTimeTableFragment);
			break;
		case R.id.back_1:
			followGroup.weddingTimeTableFragment = new WeddingTimeTableFragment();
			followGroup.unLoadFragment8();
			followGroup.loadFragment(R.id.fmLayout, followGroup.weddingTimeTableFragment);
			break;

		default:
			break;
		}
	}

}
