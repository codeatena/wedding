package com.vardhaman.wedding.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.vardhaman.wedding.R;
import com.vardhaman.wedding.restinteraction.RESTInteraction;
import com.vardhaman.wedding.util.ApiUrl;
import com.vardhaman.wedding.util.Util;

public class WeddingShowsFragment extends Fragment implements OnClickListener{
	Context context;
	View view;
	RESTInteraction restInteraction;
	ListView listView;
	@Override
	public View onCreateView(LayoutInflater inflater, android.view.ViewGroup container, Bundle savedInstanceState) {
		context = getActivity();
		view = inflater.inflate(R.layout.activity_wedding_shows, container, false);
		restInteraction = RESTInteraction.getInstance(context);
		init();
		return view;
	}
	void init(){
		listView = (ListView)view.findViewById(R.id.listView);
		new GetShowsAsync().execute();
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub

	}

	class MyAdapter extends BaseAdapter{

		LayoutInflater lInflater;

		public MyAdapter() {
			lInflater = LayoutInflater.from(context);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return Util.getShows.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			if(convertView == null)
				convertView = lInflater.inflate(R.layout.shows_list_items, null);
			
			TextView showsDate, showsName, showsAdd_1, showsTime, showsContact;
			ImageView showImage;
			
			showsDate = (TextView)convertView.findViewById(R.id.showsDate);
			showsName = (TextView)convertView.findViewById(R.id.showsName);
			showsAdd_1 = (TextView)convertView.findViewById(R.id.showsAdd_1);
			showsTime = (TextView)convertView.findViewById(R.id.showsTime);
			showsContact = (TextView)convertView.findViewById(R.id.showsContact);
			showImage = (ImageView)convertView.findViewById(R.id.showImage);
			
			showsDate.setText(Util.getShows.get(position).getShowDate());
			showsName.setText(Util.getShows.get(position).getShowName());
			showsAdd_1.setText(Util.getShows.get(position).getShowAddress());
			showsTime.setText("Time: " + Util.getShows.get(position).getShowTime());
			showsContact.setText("Contact: " + Util.getShows.get(position).getShowContact());
			Util.loadImage(showImage, Util.getShows.get(position).getShowImage(), 0);
			
			showsName.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					String url = "http://www.essexbrides.net/index.php/component/weddingfair_diary?Itemid=27";
					Intent i = new Intent(Intent.ACTION_VIEW);
					i.setData(Uri.parse(url));
					startActivity(i);
				}
			});

			return convertView;
		}

	}
	
	private class GetShowsAsync extends AsyncTask<String, Void, String>
	{
		private ProgressDialog progressDialog;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog=new ProgressDialog(context);
			progressDialog.setCancelable(false);
			progressDialog.setMessage("Please wait...");
			progressDialog.show();
		}
		@SuppressWarnings("deprecation")
		@Override
		protected String doInBackground(String... params) {
		
			return restInteraction.getAllShows(ApiUrl.getShowsUrl);
		}
		@Override
		protected void onPostExecute(String result) {
			
			super.onPostExecute(result);
			progressDialog.dismiss();
			if(result!=null)
			{
				if(result=="Successful")
				{
					listView.setAdapter(new MyAdapter());
					//startActivity(new Intent(SignUpActivity.this,LoginActivity.class));
				}
				else {
					Toast.makeText(context, "Internet connection is slow. Please try again later.",Toast.LENGTH_SHORT).show();
				}
			}
			else {
				Toast.makeText(context, "Network Error", Toast.LENGTH_SHORT).show();
			}
		}
	
	}

}
