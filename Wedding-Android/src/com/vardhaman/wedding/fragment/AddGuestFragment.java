package com.vardhaman.wedding.fragment;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.Calendar;

import org.json.JSONArray;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.vardhaman.wedding.MyWeddingGroup;
import com.vardhaman.wedding.R;
import com.vardhaman.wedding.restinteraction.RESTInteraction;
import com.vardhaman.wedding.util.ApiUrl;
import com.vardhaman.wedding.util.UploadDb;
import com.vardhaman.wedding.util.Util;

public class AddGuestFragment extends Fragment implements OnClickListener {
	Context context;
	View view;
	EditText guestFirstname, guestLastname, guestContactNo,
	guestEmail, guestAddress;
	Button btnAddGuest;
	Spinner guestRelation;
	ImageView addGuestPic, backNavigation_addguest, ivStarFav;
	TextView backNavigation_addguest_1;
	private static final int PICK_REQUEST_IMAG = 1;
	private static final int CAPTURE_REQUEST_IMAGE = 2;
	private final int PIC_CROP = 3;
	public static String TEMP_PROFILE_PIC_FILE;
	private Uri uri;
	private String filePath;
	String imgArray;
	private RESTInteraction restInteraction;
	String favourite;
	boolean flag = false;
	String spinnerString;
	private Spinner guestOption;
	private String spinnerString1;
	 String spinnerString1WithoutSpace;


	public View onCreateView(LayoutInflater inflater,
			android.view.ViewGroup container, Bundle savedInstanceState) {
		context = getActivity();
		view = inflater.inflate(R.layout.add_guest, container, false);
		init();
		return view;
	}

	void init() {
		restInteraction = RESTInteraction.getInstance(context);
		TEMP_PROFILE_PIC_FILE = Calendar.getInstance().getTimeInMillis()
				+ ".jpg";
		guestFirstname = (EditText) view.findViewById(R.id.guestFirstname);
		guestLastname = (EditText) view.findViewById(R.id.guestLastname);
		guestRelation = (Spinner) view.findViewById(R.id.guestRelation);
		guestOption = (Spinner) view.findViewById(R.id.guestOption);
		guestContactNo = (EditText) view.findViewById(R.id.guestContactNo);
		guestEmail = (EditText) view.findViewById(R.id.guestEmail);
		guestAddress = (EditText) view.findViewById(R.id.guestAddress);
		btnAddGuest = (Button) view.findViewById(R.id.btnAddGuest);
		addGuestPic = (ImageView) view.findViewById(R.id.addGuestPic);
		ivStarFav = (ImageView) view.findViewById(R.id.ivStarFav);
		ivStarFav.setOnClickListener(this);
		addGuestPic.setOnClickListener(this);
		backNavigation_addguest = (ImageView)view.findViewById(R.id.backNavigation_addguest);
		backNavigation_addguest.setOnClickListener(this);
		backNavigation_addguest_1 = (TextView)view.findViewById(R.id.backNavigation_addguest_1);
		backNavigation_addguest_1.setOnClickListener(this);
		btnAddGuest.setOnClickListener(this);
		guestRelation.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parentView, View arg1,
					int arg2, long arg3) {
				 ((TextView)parentView.getChildAt(0)).setTextColor(Color.WHITE);
				spinnerString = guestRelation.getItemAtPosition(arg2).toString();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		guestOption.setOnItemSelectedListener(new OnItemSelectedListener() {

			
			@Override
			public void onItemSelected(AdapterView<?> parentView1, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				 ((TextView)parentView1.getChildAt(0)).setTextColor(Color.WHITE);
				 spinnerString1 = guestOption.getItemAtPosition(arg2).toString();
				 spinnerString1WithoutSpace = spinnerString1.replace(" ", "");
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	private class addGuestAsync extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(context);
			progressDialog.setCancelable(false);
			progressDialog.setMessage("Please wait...");
			progressDialog.show();

		}

		@Override
		protected String doInBackground(String... params) {

			return restInteraction.addGuest(ApiUrl.addGuestURL + "user_id="
					+ Util.loginData.getUserId()
					+ "&firstname="+ URLEncoder.encode(guestFirstname.getText().toString().trim())
					+ "&lastname="+ URLEncoder.encode(guestLastname.getText().toString().trim())
					+ "&relation="+ spinnerString
					+ "&contact_no="+ URLEncoder.encode(guestContactNo.getText().toString().trim())
					+ "&email="+ URLEncoder.encode(guestEmail.getText().toString().trim())
					+ "&address="+ URLEncoder.encode(guestAddress.getText().toString().trim())
					+ "&profile_pic=" + TEMP_PROFILE_PIC_FILE 
					+"&guest_option="+spinnerString1WithoutSpace
					+ "&favouite="+ favourite);
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			progressDialog.dismiss();
			if (result.equals("Successful")) {
				Toast.makeText(context, "Guest Added Successfull", Toast.LENGTH_SHORT).show();

			} else if(result.equals("Unsuccessful")){
				Toast.makeText(context, "Email already exist.", Toast.LENGTH_SHORT).show();
			}else {
				Toast.makeText(context, "Please try again.", Toast.LENGTH_SHORT).show();
			}
		}
	}



	@Override
	public void onClick(View v) {
		MyWeddingGroup followGroup1 = (MyWeddingGroup) context;
		switch (v.getId()) {

		case R.id.addGuestPic:
			showDialog();
			break;
		case R.id.btnAddGuest:
			if (spinnerString.equalsIgnoreCase("Choose Relation")) {
				Toast.makeText(getActivity(), "Please choose Relation.", Toast.LENGTH_SHORT).show();
			}else {
				new addGuestAsync().execute();
		}
			break;
		case R.id.ivStarFav:

			if (!flag) {
				favourite="favourite";
				ivStarFav.setImageResource(R.drawable.star);
				flag = true;
			} else {
				favourite="unfavourite";
				ivStarFav.setImageResource(R.drawable.starwhite);
				flag = false;
			}

			break;
		case R.id.backNavigation_addguest:
			
			followGroup1.guestListFragment = new GuestListFragment();
			followGroup1.unLoadFragment4();
			followGroup1.loadFragment(R.id.fmLayout,
					followGroup1.guestListFragment);
			break;
		case R.id.backNavigation_addguest_1:
			followGroup1.guestListFragment = new GuestListFragment();
			followGroup1.unLoadFragment4();
			followGroup1.loadFragment(R.id.fmLayout,
					followGroup1.guestListFragment);
			break;

		default:
			break;
		}
	}

	private void showDialog() {
		final Dialog dialog = new Dialog(getActivity(),
				android.R.style.Theme_Translucent_NoTitleBar);
		dialog.setContentView(R.layout.custom);
		((TextView) dialog.findViewById(R.id.text_camera))
		.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				takePic(CAPTURE_REQUEST_IMAGE);
			}
		});
		((TextView) dialog.findViewById(R.id.text_gallery))
		.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				takePic(PICK_REQUEST_IMAG);
			}
		});
		((ImageView) dialog.findViewById(R.id.iVCamCancel))
		.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.show();
	}

	private void takePic(int action) {
		if (isSDCARDMounted()) {
			switch (action) {
			case PICK_REQUEST_IMAG:
				Intent i = new Intent(
						Intent.ACTION_PICK,
						android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				i.setType("image/*");
				i.putExtra("crop", "true");
				i.putExtra("aspectX", 1);
				i.putExtra("aspectY", 1);
				i.putExtra("outputX", 150);
				i.putExtra("outputY", 150);
				i.putExtra(MediaStore.EXTRA_OUTPUT, getTempUri());
				i.putExtra("outputFormat",
						Bitmap.CompressFormat.JPEG.toString());
				startActivityForResult(i, PICK_REQUEST_IMAG);
				break;
			case CAPTURE_REQUEST_IMAGE:
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				uri = getOutputMediaFileUri(); // create a file to save the
				// image
				intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
				startActivityForResult(intent, CAPTURE_REQUEST_IMAGE);
				break;

			default:
				break;
			}
		}
	}

	/** Create a file Uri for saving an image or video */
	private static Uri getOutputMediaFileUri() {
		return Uri.fromFile(getOutputMediaFile());
	}

	static boolean isSDCARDMounted() {
		String status = Environment.getExternalStorageState();
		if (status.equals(Environment.MEDIA_MOUNTED))
			return true;
		return false;
	}

	private Uri getTempUri() {
		return Uri.fromFile(getTempFile());
	}

	private File getTempFile() {
		if (isSDCARDMounted()) {

			File f = new File(Environment.getExternalStorageDirectory(),
					TEMP_PROFILE_PIC_FILE);
			try {
				f.createNewFile();
			} catch (IOException e) {

			}
			return f;
		} else {
			return null;
		}
	}

	/** Create a File for saving an image or video */
	private static File getOutputMediaFile() {
		File mediaStorageDir = new File(
				Environment
				.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
				"Wedding");
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				Log.d("", "failed to create directory");
				return null;
			}
		}
		File mediaFile = new File(mediaStorageDir.getPath() + File.separator
				+ "IMG_DEMO.jpeg");
		if (!mediaFile.exists())
			try {
				mediaFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		return mediaFile;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == Activity.RESULT_OK) {
			switch (requestCode) {
			case CAPTURE_REQUEST_IMAGE: {

				cropImage(uri);
			}
			break;

			case PICK_REQUEST_IMAG: {
				File tempFile = getTempFile();
				filePath = Environment.getExternalStorageDirectory() + "/"
						+ TEMP_PROFILE_PIC_FILE;
				imgArray = imageToByteArray(filePath);
			}
			break;
			case PIC_CROP: {
				File tempFile = getTempFile();
				filePath = Environment.getExternalStorageDirectory() + "/"
						+ TEMP_PROFILE_PIC_FILE;
				imgArray = imageToByteArray(filePath);
			}
			break;
			default:
				break;
			}
		}
	}

	private String imageToByteArray(String path) {
		Bitmap bm = BitmapFactory.decodeFile(path);
		addGuestPic.setImageBitmap(bm);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			new SendingImageAsync(path).execute();
			bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
			return Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);
		} catch (Exception e) {
			filePath = null;
			return null;
		}
	}

	private void cropImage(Uri picUri) {
		Intent cropIntent = new Intent("com.android.camera.action.CROP");
		cropIntent.setDataAndType(picUri, "image/*");
		cropIntent.putExtra("crop", "true");
		cropIntent.putExtra("aspectX", 1);
		cropIntent.putExtra("aspectY", 1);
		cropIntent.putExtra("outputX", 150);
		cropIntent.putExtra("outputY", 150);
		cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, getTempUri());
		cropIntent.putExtra("outputFormat",
				Bitmap.CompressFormat.JPEG.toString());
		startActivityForResult(cropIntent, PIC_CROP);
	}

	class SendingImageAsync extends AsyncTask<String, String, String> {
		JSONArray jsonArray;
		UploadDb uploadDb;
		String path;

		public SendingImageAsync(String path) {
			// TODO Auto-generated constructor stub
			this.path = path;
			uploadDb = new UploadDb();
		}

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		/**
		 * getting All products from url
		 * */
		protected String doInBackground(String... args) {
			try {
				if (path != null && !path.matches("")) {
					uploadDb.doFileUpload(path);
					Log.e("imageTo", "Method called");
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String result) {

		}
	}

}
