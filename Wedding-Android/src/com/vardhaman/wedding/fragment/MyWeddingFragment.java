package com.vardhaman.wedding.fragment;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.vardhaman.wedding.LoginActivity;
import com.vardhaman.wedding.MyWeddingGroup;
import com.vardhaman.wedding.R;

public class MyWeddingFragment extends Fragment implements OnClickListener{
	Context context;
	View view;
	ImageView ivGiftList;
	ImageView ivGuestList;
	ImageView ivProgramme;
	ImageView ivDaysNotes;
	//	ImageView ivShows;
	ImageView ivBudget;
	TextView weddDate, daysLeft;
	String weddingdate;
	SharedPreferences preferences;
	ImageView ivFavGuestList;

	@Override
	public View onCreateView(LayoutInflater inflater, android.view.ViewGroup container, Bundle savedInstanceState) {
		context = getActivity();
		view = inflater.inflate(R.layout.activity_wedding_layout, container, false);
		init();
		return view;
	}
//	public boolean onCreateOptionsMenu(Menu menu) {
//	    MenuInflater inflater = getActivity().getMenuInflater();
//	    inflater.inflate(R.menu.my_wedding, menu);
//	    return true;
//	}
//	
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		// Handle item selection
//		switch (item.getItemId()) {
//		case R.id.action_settings:
//			startActivity(new Intent(getActivity(), LoginActivity.class));
//			return true;
//		default:
//			return super.onOptionsItemSelected(item);
//		}
//	}

	@SuppressLint("NewApi")
	void init(){
		preferences = PreferenceManager.getDefaultSharedPreferences(context);
		weddingdate = preferences.getString("weddingDate", "");
		ivGiftList = (ImageView)view.findViewById(R.id.ivGiftList);
		ivGiftList.setOnClickListener(this);
		ivGuestList = (ImageView)view.findViewById(R.id.ivGuestList);
		ivGuestList.setOnClickListener(this);
		ivDaysNotes = (ImageView)view.findViewById(R.id.ivDaysNotes);
		ivDaysNotes.setOnClickListener(this);
		ivProgramme = (ImageView)view.findViewById(R.id.ivProgramme);
		ivProgramme.setOnClickListener(this);
		//		ivShows = (ImageView)view.findViewById(R.id.ivShows);
		//		ivShows.setOnClickListener(this);
		ivFavGuestList=(ImageView)view.findViewById(R.id.ivFavGuestList);
		ivFavGuestList.setOnClickListener(this);
		ivBudget=(ImageView)view.findViewById(R.id.ivBudget);
		ivBudget.setOnClickListener(this);
		weddDate = (TextView)view.findViewById(R.id.weddDate);
		daysLeft = (TextView)view.findViewById(R.id.daysLeft);
		if (!weddingdate.equalsIgnoreCase("Date/month/year") && !weddingdate.matches("")) {
			weddDate.setText(weddingdate);
		}
		Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
		calendar.set(Calendar.HOUR, 0);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		calendar.set(Calendar.SECOND, 0);
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		Calendar cal  = Calendar.getInstance();
		try {
			cal.setTime(df.parse(weddingdate));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		long days = daysBetween(calendar, cal);

		if (days != 0) {
			daysLeft.setText(days+"");
			preferences.edit()
			.putLong("daysLeft", days)
			.commit();

		}
		if (days == 0 ) {
			daysLeft.setText("Today");
			preferences.edit()
			.putLong("daysLeft", days)
			.commit();
		}
	}
	
	@Override
	public void onClick(View v) {
		MyWeddingGroup followGroup = (MyWeddingGroup)context;
		switch (v.getId()) {
		case R.id.ivGiftList:
			followGroup.giftListFragment = new GiftListFragment();
			followGroup.unLoadFragment1();
			followGroup.loadFragment(R.id.fmLayout, followGroup.giftListFragment);
			break;
		case R.id.ivGuestList:
			followGroup.guestListFragment = new GuestListFragment();
			followGroup.unLoadFragment1();
			followGroup.loadFragment(R.id.fmLayout, followGroup.guestListFragment);
			break;
		case R.id.ivProgramme:
			followGroup.weddingTimeTableFragment = new WeddingTimeTableFragment();
			followGroup.unLoadFragment1();
			followGroup.loadFragment(R.id.fmLayout, followGroup.weddingTimeTableFragment);
			break;
		case R.id.ivDaysNotes:
			followGroup.dayAndNotesFragment = new DayAndNotesFragment();
			followGroup.unLoadFragment1();
			followGroup.loadFragment(R.id.fmLayout, followGroup.dayAndNotesFragment);
			break;
		case R.id.ivBudget:
			followGroup.weddingBudgetFragment = new WeddingBudgetFragment();
			followGroup.unLoadFragment1();
			followGroup.loadFragment(R.id.fmLayout, followGroup.weddingBudgetFragment);
			break;
		case R.id.ivFavGuestList:
			followGroup.favGuestFragment = new FavouriteGuestFragment();
			followGroup.unLoadFragment1();
			followGroup.loadFragment(R.id.fmLayout, followGroup.favGuestFragment);
			break;

		default:
			break;
		}

	}

	@SuppressLint("NewApi")
	public static long daysBetween(Calendar startDate, Calendar endDate) {
		long end = endDate.getTimeInMillis();
		long start = startDate.getTimeInMillis();
		return TimeUnit.MILLISECONDS.toDays(Math.abs(end - start));
	}

}
