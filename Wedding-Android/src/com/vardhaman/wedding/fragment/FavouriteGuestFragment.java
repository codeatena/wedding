package com.vardhaman.wedding.fragment;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.vardhaman.wedding.MyWeddingGroup;
import com.vardhaman.wedding.R;
import com.vardhaman.wedding.SupplierDetailActivity;
import com.vardhaman.wedding.adapter.FavGuestListAdapter;
import com.vardhaman.wedding.model.FavModel;
import com.vardhaman.wedding.restinteraction.RESTInteraction;
import com.vardhaman.wedding.util.ApiUrl;
import com.vardhaman.wedding.util.Util;

public class FavouriteGuestFragment extends Fragment implements OnClickListener {
	Context context;
	View view;
	private ImageView ivBackNavFavGuestList;
	private RESTInteraction restInteraction;
	private ListView favGuestListView;
	private ArrayList<FavModel> favGuestList = new ArrayList<FavModel>();
	TextView ivBackNavFavGuestList_1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		context = getActivity();
		view = inflater.inflate(R.layout.favguestlist, container, false);
		init();
		return view;
	}

	public void init() {
		restInteraction = RESTInteraction.getInstance(context);
		favGuestListView = (ListView) view.findViewById(R.id.favGuestListView);
		ivBackNavFavGuestList = (ImageView)view.findViewById(R.id.ivBackNavFavGuestList);
		ivBackNavFavGuestList.setOnClickListener(this);
		ivBackNavFavGuestList_1 = (TextView)view.findViewById(R.id.ivBackNavFavGuestList_1);
		ivBackNavFavGuestList_1.setOnClickListener(this);
		favGuestListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				startActivity(new Intent(context, SupplierDetailActivity.class).putExtra("supplierId", Util.getFavouriteList.get(position).getSupplierCategoryId()));	
				
			}
		});
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		new FavGuestListAsync().execute();
		super.onResume();
	}

	public class FavGuestListAsync extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(context);
			progressDialog.setCancelable(false);
			progressDialog.setMessage("Please wait...");
			progressDialog.show();

		}

		@Override
		protected String doInBackground(String... params) {

			return restInteraction.favGuestList(ApiUrl.getFavouritesUrl + "user_id=" + Util.loginData.getUserId());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			progressDialog.dismiss();
			if (result.equals("Successful")) {
				
				favGuestListView.setAdapter(new FavGuestListAdapter(context,
						R.layout.favguestlistitem, Util.getFavouriteList));
			} else if (result.equalsIgnoreCase("Unsuccessful")) {
				Toast.makeText(context, "There is no guest added as a favorite.", Toast.LENGTH_SHORT).show();
			}else{
				Toast.makeText(context, "Please check internet connection.", Toast.LENGTH_SHORT).show();
			}
		}
	}

	@Override
	public void onClick(View v) {
		MyWeddingGroup followGroup2 = (MyWeddingGroup) context;
		switch (v.getId()) {
		case R.id.ivBackNavFavGuestList:
			followGroup2.myWeddingFragment = new MyWeddingFragment();
			followGroup2.unLoadFragment7();
			followGroup2.loadFragment(R.id.fmLayout,
					followGroup2.myWeddingFragment);
			break;
		case R.id.ivBackNavFavGuestList_1:
			followGroup2.myWeddingFragment = new MyWeddingFragment();
			followGroup2.unLoadFragment7();
			followGroup2.loadFragment(R.id.fmLayout,
					followGroup2.myWeddingFragment);
			break;

		default:
			break;
		}

	}

}
