package com.vardhaman.wedding.fragment;

import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Calendar;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.vardhaman.wedding.Dashboard;
import com.vardhaman.wedding.MyWeddingGroup;
import com.vardhaman.wedding.R;
import com.vardhaman.wedding.restinteraction.RESTInteraction;
import com.vardhaman.wedding.util.ApiUrl;
import com.vardhaman.wedding.util.Util;


public class DayAndNotesFragment extends Fragment implements OnClickListener{
	TextView weddingDate, weddingTime,receptionDate, receptionTime , backNavigation_timetable_1;
	EditText weddingVenue, receptionVenue;
	Context context;
	View view;
	Button submit;
	private int picDate = 1;
	private int picTime = 4;
	private final int WEDDING_START_DATE = 2;
	private final int RECEPTION_START_DATE = 3;
	private final int WEDDING_START_TIME = 5;
	private final int RECEPTION_START_TIME = 6;
	String wedding_Venue, reception_Venue;
	RESTInteraction restInteraction;
	ProgressDialog progressDialog;
	SharedPreferences sharedPreferences;
	ImageView backNav;
	public View onCreateView(LayoutInflater inflater, android.view.ViewGroup container, Bundle savedInstanceState) {
		context = getActivity();
		view = inflater.inflate(R.layout.activity_dates_notes, container, false);
		init();
		return view;
	}
	
	void init(){
		restInteraction = RESTInteraction.getInstance(context);
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		weddingDate = (TextView)view.findViewById(R.id.wedding_date);
		weddingTime = (TextView)view.findViewById(R.id.wedding_time);
		receptionDate = (TextView)view.findViewById(R.id.reception_date);
		receptionTime = (TextView)view.findViewById(R.id.reception_time);
		weddingVenue = (EditText)view.findViewById(R.id.wedding_venue);
		receptionVenue = (EditText)view.findViewById(R.id.reception_venue);
		submit = (Button)view.findViewById(R.id.submit_tt);
		weddingDate.setOnClickListener(this);
		weddingTime.setOnClickListener(this);
		receptionDate.setOnClickListener(this);
		receptionTime.setOnClickListener(this);
		submit.setOnClickListener(this);
		backNav = (ImageView)view.findViewById(R.id.backNavigation_timetable);
		backNav.setOnClickListener(this);
		backNavigation_timetable_1 = (TextView)view.findViewById(R.id.backNavigation_timetable_1);
		backNavigation_timetable_1.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		MyWeddingGroup followGroup = (MyWeddingGroup)context;
		switch (v.getId()) {
		case R.id.wedding_date:
			picDate = WEDDING_START_DATE;
			showDatePickerDialog(v);
			break;
		case R.id.wedding_time:
			picTime = WEDDING_START_TIME;
			showTimePickerDialog(v);
			break;
		case R.id.reception_date:
			picDate = RECEPTION_START_DATE;
			showDatePickerDialog(v);
			break;
		case R.id.reception_time:
			picTime = RECEPTION_START_TIME;
			showTimePickerDialog(v);
			break;
		case R.id.submit_tt:
			wedding_Venue = weddingVenue.getText().toString();
			reception_Venue = receptionVenue.getText().toString();
			if (!weddingDate.getText().toString().equalsIgnoreCase("Date/month/year") && !weddingDate.getText().toString().matches("")) {
				sharedPreferences.edit()
				.putString("weddingDate", weddingDate.getText().toString())
				.commit();
			}
			if (!receptionDate.getText().toString().equalsIgnoreCase("Date/month/year") && !receptionDate.getText().toString().matches("")) {
				sharedPreferences.edit()
				.putString("receptionDate", receptionDate.getText().toString())
				.commit();
			}
			if ( wedding_Venue.length()>0 && reception_Venue.length()>0) {
				new WeddingTimeTableAsync().execute();
				new ReceptionTimeTableAsync().execute();
			}else {
				Toast.makeText(context, "Please fill all fields.", Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.backNavigation_timetable:
					
			followGroup.myWeddingFragment = new MyWeddingFragment();
			followGroup.unLoadFragment6();
			followGroup.loadFragment(R.id.fmLayout, followGroup.myWeddingFragment);
			break;
		case R.id.backNavigation_timetable_1:
			followGroup.myWeddingFragment = new MyWeddingFragment();
			followGroup.unLoadFragment6();
			followGroup.loadFragment(R.id.fmLayout, followGroup.myWeddingFragment);
			break;
		default:
			break;
		}

	}
	
	public void showDatePickerDialog(View v) {
		DialogFragment newFragment = new DatePickerFragment();
		newFragment.show(this.getFragmentManager(), "datePicker");
	}

	@SuppressLint("ValidFragment")
	public class DatePickerFragment extends DialogFragment implements
	DatePickerDialog.OnDateSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current date as the default date in the picker
			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);
			// Create a new instance of DatePickerDialog and return it
			return new DatePickerDialog(getActivity(), this, year, month, day);
		}

		public void onDateSet(DatePicker view, int year, int month, int day) {
			if(picDate==WEDDING_START_DATE){
				weddingDate.setText(new StringBuilder().append(day)
						.append("-").append(month+1).append("-").append(year));
			}
			else if (picDate==RECEPTION_START_DATE) {
				receptionDate.setText(new StringBuilder().append(day)
						.append("-").append(month+1).append("-").append(year));
			}
		}
	}

	public void showTimePickerDialog(View v) {
		DialogFragment newFragment = new TimePickerFragment();
		newFragment.show(getFragmentManager(), "timePicker");
	}

	@SuppressLint({ "ValidFragment", "NewApi" })
	public class TimePickerFragment extends DialogFragment implements
	TimePickerDialog.OnTimeSetListener {

		@TargetApi(Build.VERSION_CODES.HONEYCOMB)
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current time as the default values for the picker
			final Calendar c = Calendar.getInstance();
			int hour = c.get(Calendar.HOUR_OF_DAY);
			int minute = c.get(Calendar.MINUTE);

			// Create a new instance of TimePickerDialog and return it
			return new TimePickerDialog(getActivity(), this, hour, minute,
					DateFormat.is24HourFormat(getActivity()));
		}

		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			String tempHour = hourOfDay < 10 ? "0"+hourOfDay : ""+hourOfDay;
			String tempMinute = minute < 10 ? "0"+minute : ""+minute; 
			 if(picTime==WEDDING_START_TIME){
				weddingTime.setText(new StringBuilder().append(tempHour)
						.append(":").append(tempMinute));
			}
			else if (picTime==RECEPTION_START_TIME) {
				receptionTime.setText(new StringBuilder().append(tempHour)
						.append(":").append(tempMinute));
			}
		}
	}
	private class WeddingTimeTableAsync extends AsyncTask<String, Void, String> {
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(context);
			progressDialog.setCancelable(false);
			progressDialog.setMessage("Please wait...");
			progressDialog.show();

		}

		@Override
		protected String doInBackground(String... params) {

			return restInteraction.weddingTable(ApiUrl.weddingTimeTableURL
					+ "user_id=" + Util.loginData.getUserId()
					+ "&event_type=" + "Wedding"
					+ "&date=" + weddingDate.getText().toString()
					+ "&time=" + weddingTime.getText().toString()
					+ "&venue=" + URLEncoder.encode(wedding_Venue)
					+ "&status=" + "start");
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			if (result.equals("Successful")) {

			} else {
				Toast.makeText(context, result, Toast.LENGTH_SHORT)
				.show();
			}
		}
	}

	private class ReceptionTimeTableAsync extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {

			return restInteraction.receptionTable(ApiUrl.weddingTimeTableURL
					+ "user_id=" + Util.loginData.getUserId()
					+ "&event_type=" + "Reception"
					+ "&date=" + receptionDate.getText().toString()
					+ "&time=" + receptionTime.getText().toString()
					+ "&venue=" + URLEncoder.encode(reception_Venue)
					+ "&status=" + "start");
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			progressDialog.dismiss();
			if (result.equals("Successful")) {
				Toast.makeText(context, "Successfully created", Toast.LENGTH_SHORT)
				.show();
				startActivity(new Intent(context, Dashboard.class));
			} else {
				Toast.makeText(context, result, Toast.LENGTH_SHORT)
				.show();
			}
		}
	}

	
}
