package com.vardhaman.wedding.fragment;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

import com.vardhaman.wedding.ConnectionDetector;
import com.vardhaman.wedding.MyWeddingGroup;
import com.vardhaman.wedding.R;
import com.vardhaman.wedding.adapter.GuestListAdapter;
import com.vardhaman.wedding.model.GuestList;
import com.vardhaman.wedding.restinteraction.RESTInteraction;
import com.vardhaman.wedding.util.ApiUrl;
import com.vardhaman.wedding.util.MySQLiteHelper;
import com.vardhaman.wedding.util.Util;

public class GuestListFragment extends Fragment implements OnClickListener{
	Context context;
	View view;
	ImageView backNavigation;
	ListView listView;
	Button addGuest;
	TextView backNavigation_1;
	private RESTInteraction restInteraction;
	MySQLiteHelper mySQLiteHelper;
	Cursor cursor, cursorArrivedGuest;
	Boolean isInternetPresent = false;
	ConnectionDetector cd;
	TextView guestCount;
	private Spinner spinnerFilterGuest;

	 String spinnerFilterGuest1;
	public static String spinnerString2WithoutSpace;
	
	@Override
	public View onCreateView(LayoutInflater inflater, android.view.ViewGroup container, Bundle savedInstanceState) {
		context = getActivity();
		view = inflater.inflate(R.layout.guestlist_layout, container, false);
		Util.guestArrayList.clear();
		init();
		return view;
	}
	
	@Override
	public void onResume() {
		
		updateCount();
		
		if (!isInternetPresent) {

			Toast.makeText(getActivity(), "Please Check Internet Connection..!", Toast.LENGTH_SHORT).show();
		} else {
//			new GuestListAsync().execute();
		}
		
		super.onResume();
	}
	
	public void updateCount(){
		ArrayList<String> arrivedGuest = new ArrayList<String>();
//		cursor = mySQLiteHelper.getAllGuest();
//		cursorArrivedGuest = mySQLiteHelper.getArrivedGuest();
		for (int i = 0; i < Util.guestArrayList.size(); i++) {
			String lenght = Util.guestArrayList.get(i).getStatus();
			if (lenght.equalsIgnoreCase("true")) {
				arrivedGuest.add(lenght);
			}
		}
		guestCount.setText("(" + arrivedGuest.size() + " / " + Util.guestArrayList.size()+ ")");
	}
	
	void init(){
		mySQLiteHelper = new MySQLiteHelper(context);
		restInteraction=RESTInteraction.getInstance(context);
		cd = new ConnectionDetector(context);
		isInternetPresent = cd.isConnectingToInternet();
		listView = (ListView)view.findViewById(R.id.guestListView);
		backNavigation = (ImageView)view.findViewById(R.id.backNavigation);
		backNavigation.setOnClickListener(this);
		backNavigation_1 = (TextView)view.findViewById(R.id.backNavigation_1);
		backNavigation_1.setOnClickListener(this);
		addGuest = (Button)view.findViewById(R.id.addGuest);
		addGuest.setOnClickListener(this);
		guestCount = (TextView)view.findViewById(R.id.guestCount);
		spinnerFilterGuest = (Spinner) view.findViewById(R.id.spinnerFilterGuest);
		spinnerFilterGuest.setOnItemSelectedListener(new OnItemSelectedListener() {


			@Override
			public void onItemSelected(AdapterView<?> parentView, View arg1,
					int arg2, long arg3) {
				 ((TextView)parentView.getChildAt(0)).setTextColor(Color.WHITE);
				spinnerFilterGuest1 = spinnerFilterGuest.getItemAtPosition(arg2).toString();
				 spinnerString2WithoutSpace = spinnerFilterGuest1.replace(" ", "");
				 new GuestListAsync(spinnerString2WithoutSpace).execute();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	public class GuestListAsync extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;
		String spinnerString2WithoutSpace;

		public GuestListAsync(String spinnerString2WithoutSpace) {
			this.spinnerString2WithoutSpace = spinnerString2WithoutSpace;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(context);
			progressDialog.setCancelable(false);
			progressDialog.setMessage("Please wait...");
			progressDialog.show();

		}

		@Override
		protected String doInBackground(String... params) {

			return restInteraction.fetchGuestList(ApiUrl.fetchGuestURL
					+ "id=" + Util.loginData.getUserId()
					+ "&guest_option=" + spinnerString2WithoutSpace);
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
		
			progressDialog.dismiss();
			if (result.equals("Successful")) {
				updateCount();
				listView.setAdapter(new GuestListAdapter(context, R.layout.guestlist_item, Util.guestArrayList));
			}else if (result.equalsIgnoreCase("Unsuccessful")) {
				listView.setAdapter(new GuestListAdapter(context, R.layout.guestlist_item, Util.guestArrayList));
				Toast.makeText(context, "There is no Guest in your list.", Toast.LENGTH_SHORT)
				.show();
			}  
			else {
				Toast.makeText(context, "Please check internet connection.", Toast.LENGTH_SHORT)
						.show();
			}
		}
	}

	@Override
	public void onClick(View arg0) {
		MyWeddingGroup followGroup = (MyWeddingGroup)context;
		switch (arg0.getId()) {
		case R.id.addGuest:
					
			followGroup.addGuestFragment = new AddGuestFragment();
			followGroup.unLoadFragment3();
			followGroup.loadFragment(R.id.fmLayout, followGroup.addGuestFragment);
			break;
			
		case R.id.backNavigation:
			followGroup.myWeddingFragment = new MyWeddingFragment();
			followGroup.unLoadFragment3();
			followGroup.loadFragment(R.id.fmLayout, followGroup.myWeddingFragment);
			break;
		case R.id.backNavigation_1:
			followGroup.myWeddingFragment = new MyWeddingFragment();
			followGroup.unLoadFragment3();
			followGroup.loadFragment(R.id.fmLayout, followGroup.myWeddingFragment);
			break;

		default:
			break;
		}
	}
}
