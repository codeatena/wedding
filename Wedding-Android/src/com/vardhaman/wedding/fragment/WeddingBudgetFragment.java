package com.vardhaman.wedding.fragment;

import com.vardhaman.wedding.MyWeddingGroup;
import com.vardhaman.wedding.R;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class WeddingBudgetFragment extends Fragment implements OnClickListener{
	Context context;
	View view;
	ImageView backNav;
	TextView totalBudget, spentDate, remainingBudget, remainingDays;
	EditText etTransportation, etCeremony, etFlowers, etPhoto, etWeddingAttire, etEverythng, etReception;
	Button btnAddTotal;
	SharedPreferences sharedPreferences;
	String totalWeddingBudget;
	int totalBudgetFigure;
	int trans, ceremny, flwrs, photography, wddngAttr, evryThng, rcptn, spntToDate, rmngBudget;
	long daysLeft;
	EditText addBudget;
	ScrollView scrollView;
	TextView backNavigation_budget_1;

	public View onCreateView(LayoutInflater inflater,
			android.view.ViewGroup container, Bundle savedInstanceState) {
		context = getActivity();
		view = inflater.inflate(R.layout.wedding_budget_layout, container, false);
		init();
		return view;
	}
	void init(){
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		totalWeddingBudget = sharedPreferences.getString("totalBudget", "");
		
		daysLeft = sharedPreferences.getLong("daysLeft", 0);
		trans = sharedPreferences.getInt("a", 0);
		ceremny = sharedPreferences.getInt("b", 0);
		flwrs = sharedPreferences.getInt("c", 0);
		photography = sharedPreferences.getInt("d", 0);
		wddngAttr = sharedPreferences.getInt("e", 0);
		evryThng = sharedPreferences.getInt("f", 0);
		rcptn = sharedPreferences.getInt("g", 0);
		spntToDate = sharedPreferences.getInt("spentDate", 0);
		rmngBudget = sharedPreferences.getInt("remainingBudget", 0);
		backNav = (ImageView)view.findViewById(R.id.backNavigation_budget);
		backNav.setOnClickListener(this);
		backNavigation_budget_1 = (TextView)view.findViewById(R.id.backNavigation_budget_1);
		backNavigation_budget_1.setOnClickListener(this);
		totalBudget = (TextView)view.findViewById(R.id.totalBudget);
		spentDate = (TextView)view.findViewById(R.id.spentDate);
		remainingBudget = (TextView)view.findViewById(R.id.remainingBudget);
		remainingDays = (TextView)view.findViewById(R.id.remainingDays);
		etTransportation = (EditText)view.findViewById(R.id.etTrans);
		etCeremony = (EditText)view.findViewById(R.id.etCeremony);
		etFlowers = (EditText)view.findViewById(R.id.etFlowers);
		etPhoto = (EditText)view.findViewById(R.id.etPhoto);
		etWeddingAttire = (EditText)view.findViewById(R.id.etWeddingAttire);
		etEverythng = (EditText)view.findViewById(R.id.etEvrythng);
		etReception = (EditText)view.findViewById(R.id.etReception);
		btnAddTotal = (Button)view.findViewById(R.id.btnAddTotal);
		btnAddTotal.setOnClickListener(this);
		scrollView = (ScrollView)view.findViewById(R.id.scrollView);
		if (daysLeft != 0) {
			remainingDays.setText("Remaining Days: " +daysLeft);
		}
		if (daysLeft == 0) {
			remainingDays.setText("Remaining Days: " +"Today");
		}
		if (spntToDate != 0 ) {
			spentDate.setText(spntToDate+"");
		}
		if (totalWeddingBudget.length()>0) {
			totalBudget.setText(totalWeddingBudget);
			totalBudgetFigure= Integer.parseInt(totalWeddingBudget);
		}else {
			final Dialog dialog = new Dialog(context);
			dialog.setContentView(R.layout.total_budget_dialog);
			dialog.setTitle("Add Total Budget");
			 
			Button addTotalBudget, btnCancel;
			addBudget = (EditText)dialog.findViewById(R.id.budget);
			addTotalBudget = (Button)dialog.findViewById(R.id.btnAdd);
			btnCancel = (Button)dialog.findViewById(R.id.btnCancel);
			addTotalBudget.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					String string = addBudget.getText().toString();
					
					if (string.length()<=0) {
						Toast.makeText(context, "Please enter total amount", Toast.LENGTH_SHORT).show();
					}else {
						
					totalBudget.setText(string);
					totalBudgetFigure= Integer.parseInt(string);
					sharedPreferences.edit()
					.putString("totalBudget", string)
					.commit();
					dialog.dismiss();
					}
				}
			});
			btnCancel.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View arg0) {
					dialog.dismiss();
					MyWeddingGroup followGroup1 = (MyWeddingGroup) context;
					followGroup1.myWeddingFragment = new MyWeddingFragment();
					followGroup1.unLoadFragment9();
					followGroup1.loadFragment(R.id.fmLayout, followGroup1.myWeddingFragment);
					
				}
			});
			dialog.show();
		}
		
		if (trans != 0) {
			etTransportation.setText(trans+"");
		}
		if (ceremny != 0) {
			etCeremony.setText(ceremny+"");
		}
		if (flwrs != 0) {
			etFlowers.setText(flwrs+"");
		}
		if (photography != 0) {
			etPhoto.setText(photography+"");
		}
		if (wddngAttr != 0) {
			etWeddingAttire.setText(wddngAttr+"");
		}
		if (evryThng != 0) {
			etEverythng.setText(evryThng+"");
		}
		if (rcptn != 0) {
			etReception.setText(rcptn+"");
		}
		if (rmngBudget != 0 ) {
			remainingBudget.setText(rmngBudget+"");
		}
	}


	@Override
	public void onClick(View v) {
		MyWeddingGroup followGroup1 = (MyWeddingGroup) context;
		switch (v.getId()) {
		case R.id.backNavigation_budget:
			followGroup1.myWeddingFragment = new MyWeddingFragment();
			followGroup1.unLoadFragment9();
			followGroup1.loadFragment(R.id.fmLayout, followGroup1.myWeddingFragment);
			break;
		case R.id.backNavigation_budget_1:
			followGroup1.myWeddingFragment = new MyWeddingFragment();
			followGroup1.unLoadFragment9();
			followGroup1.loadFragment(R.id.fmLayout, followGroup1.myWeddingFragment);
			break;
		case R.id.btnAddTotal:
			int a = Integer.parseInt(etTransportation.getText().toString());
			int b = Integer.parseInt(etCeremony.getText().toString());
			int c = Integer.parseInt(etFlowers.getText().toString());
			int d = Integer.parseInt(etPhoto.getText().toString());
			int e = Integer.parseInt(etWeddingAttire.getText().toString());
			int f = Integer.parseInt(etEverythng.getText().toString());
			int g = Integer.parseInt(etReception.getText().toString());
			sharedPreferences.edit()
			.putInt("a", a)
			.putInt("b", b)
			.putInt("c", c)
			.putInt("d", d)
			.putInt("e", e)
			.putInt("f", f)
			.putInt("g", g)
			.commit();
			int total = a+b+c+d+e+f+g;
			spentDate.setText(total+"");
			sharedPreferences.edit()
			.putInt("spentDate", total)
			.commit();
			int remng = totalBudgetFigure - total;
			sharedPreferences.edit()
			.putInt("remainingBudget", remng)
			.commit();
			remainingBudget.setText(remng+"");
			scrollView.fullScroll(ScrollView.FOCUS_UP);

			break;

		default:
			break;
		}		
	}

}
