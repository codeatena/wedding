package com.vardhaman.wedding.fragment;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.vardhaman.wedding.R;
import com.vardhaman.wedding.SupplierActivity;
import com.vardhaman.wedding.SupplierGroup;
import com.vardhaman.wedding.adapter.ImageCustomAdapter;
import com.vardhaman.wedding.restinteraction.RESTInteraction;
import com.vardhaman.wedding.util.ApiUrl;
import com.vardhaman.wedding.util.Util;

public class SupplierFragment extends Fragment implements OnClickListener{
	Context context;
	View view;
	GridView grid;
	public RESTInteraction restInteraction;

	@Override
	public View onCreateView(LayoutInflater inflater, android.view.ViewGroup container, Bundle savedInstanceState) {
		context = getActivity();
		view = inflater.inflate(R.layout.activity_suppliers, container, false);
		init();
		return view;
	}

	@SuppressLint("NewApi")
	void init(){
		restInteraction=RESTInteraction.getInstance(context);
		grid = (GridView)view.findViewById(R.id.gridViewSupImages);

		grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
//				Toast.makeText(context,
//						"You Clicked at " + txtId[position], Toast.LENGTH_SHORT)
//						.show();
//				Fragment fragment = new CategoryListFragment();
				Bundle bundle = new Bundle();
				bundle.putString("categoryId", Util.getCategoryList.get(position).getId());
				SupplierGroup followGroup = (SupplierGroup)context;		
				followGroup.categoryListFragment = new CategoryListFragment();
				followGroup.categoryListFragment.setArguments(bundle);
				followGroup.unLoadFragment1();
				followGroup.loadFragment(R.id.fmLayout, followGroup.categoryListFragment);
			}
		});


	}
	
	@Override
	public void onResume() {
		new SupplierAsync().execute();
		super.onResume();
	}

//	@Override
//	public void onClick(View v) {
//		switch (v.getId()) {
//		case R.id.ivGiftList:
//			SupplierGroup followGroup = (SupplierGroup)context;		
//			followGroup.categoryListFragment = new CategoryListFragment();
//			followGroup.unLoadFragment1();
//			followGroup.loadFragment(R.id.categoryListView, followGroup.categoryListFragment);
//			break;
//
//		default:
//			break;
//		}
//
//	}
	private class SupplierAsync extends AsyncTask<String, Void, String>
	{
		private ProgressDialog progressDialog;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog=new ProgressDialog(context);
			progressDialog.setCancelable(false);
			progressDialog.setMessage("Please wait...");
			progressDialog.show();
		}
		@SuppressWarnings("deprecation")
		@Override
		protected String doInBackground(String... params) {
		
			return restInteraction.getSupplierList(ApiUrl.SupplierListURL);
		}
		@Override
		protected void onPostExecute(String result) {
			
			super.onPostExecute(result);
			progressDialog.dismiss();
			if(result!=null)
			{
				if(result=="Successful")
				{
					grid.setAdapter(new ImageCustomAdapter(context, R.layout.grid_single, Util.getCategoryList));
					
				}
				else {
					Toast.makeText(context, "Internet connection is slow. Please try again later.",Toast.LENGTH_SHORT).show();
				}
			}
			else {
				Toast.makeText(context, "Network Error", Toast.LENGTH_SHORT).show();
			}
		}
	
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
	}


}
