package com.vardhaman.wedding.fragment;

import java.util.Calendar;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.vardhaman.wedding.R;
import com.vardhaman.wedding.ShowsGroup;
import com.vardhaman.wedding.adapter.GiftListAdapter;
import com.vardhaman.wedding.restinteraction.RESTInteraction;
import com.vardhaman.wedding.util.ApiUrl;
import com.vardhaman.wedding.util.Util;

public class ShowsDetailFragment extends Fragment implements OnClickListener{
	Context context;
	View view;
	ImageView backNav;
	TextView setStartDate, setEndDate, setTime;
	private int picDate = 1;
	private final int START_DATE = 2;
	private final int END_DATE = 3;
	EditText showMembers, showVenue;
	Button showSubmit;
	RESTInteraction restInteraction ;
	String startdate, endDate, venue, time, member;
	TextView backNavigation_weddingShows_1;
	@Override
	public View onCreateView(LayoutInflater inflater, android.view.ViewGroup container, Bundle savedInstanceState) {
		context = getActivity();
		view = inflater.inflate(R.layout.wedding_show_detail, container, false);
		init();
		return view;
	}
	void init(){
		restInteraction = RESTInteraction.getInstance(context);
		backNav = (ImageView)view.findViewById(R.id.backNavigation_weddingShows);
		backNav.setOnClickListener(this);
		backNavigation_weddingShows_1 = (TextView)view.findViewById(R.id.backNavigation_weddingShows_1);
		backNavigation_weddingShows_1.setOnClickListener(this);
		setStartDate = (TextView)view.findViewById(R.id.startDateText);
		setStartDate.setOnClickListener(this);
		setEndDate = (TextView)view.findViewById(R.id.endDateText);
		setEndDate.setOnClickListener(this);
		setTime = (TextView)view.findViewById(R.id.setTime);
		setTime.setOnClickListener(this);
		showVenue = (EditText)view.findViewById(R.id.showVenue);
		showMembers = (EditText)view.findViewById(R.id.showMembers);
		showSubmit = (Button)view.findViewById(R.id.showSubmit);
		showSubmit.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		ShowsGroup followGroup1 = (ShowsGroup)context;		
		switch (v.getId()) {
		case R.id.backNavigation_weddingShows:
			followGroup1.weddingShowsFragment = new WeddingShowsFragment();
			followGroup1.unLoadFragment2();
			followGroup1.loadFragment(R.id.fmLayout, followGroup1.weddingShowsFragment);
			break;
		case R.id.backNavigation_weddingShows_1:
			followGroup1.weddingShowsFragment = new WeddingShowsFragment();
			followGroup1.unLoadFragment2();
			followGroup1.loadFragment(R.id.fmLayout, followGroup1.weddingShowsFragment);
			break;
		case R.id.startDateText:
			picDate = START_DATE;
			showDatePickerDialog(v);
			break;
		case R.id.endDateText:
			picDate = END_DATE;
			showDatePickerDialog(v);
			break;
		case R.id.setTime:
			showTimePickerDialog(v);
			break;
		case R.id.showSubmit:
			startdate = setStartDate.getText().toString();
			endDate = setEndDate.getText().toString();
			venue = showVenue.getText().toString();
			time = setTime.getText().toString();
			member = showMembers.getText().toString();
			if (startdate.equalsIgnoreCase("StartDate") || endDate.equalsIgnoreCase("EndDate") || venue.length()<=0
					|| time.equalsIgnoreCase("Time") || member.length()<=0) {
				Toast.makeText(context, "Please fill all fields.", Toast.LENGTH_SHORT)
				.show();
			}else {
				new AddShowsAsync().execute();
			}
			break;

		default:
			break;
		}

	}
	public void showDatePickerDialog(View v) {
		DialogFragment newFragment = new DatePickerFragment();
		newFragment.show(this.getFragmentManager(), "datePicker");
	}

	@SuppressLint("ValidFragment")
	public class DatePickerFragment extends DialogFragment implements
	DatePickerDialog.OnDateSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current date as the default date in the picker
			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);
			// Create a new instance of DatePickerDialog and return it
			return new DatePickerDialog(getActivity(), this, year, month, day);
		}

		public void onDateSet(DatePicker view, int year, int month, int day) {
			if(picDate==START_DATE){
				setStartDate.setText(new StringBuilder().append(day)
						.append("-").append(month+1).append("-").append(year));
			}
			else if (picDate==END_DATE) {
				setEndDate.setText(new StringBuilder().append(day)
						.append("-").append(month+1).append("-").append(year));
			}
		}
	}

	public void showTimePickerDialog(View v) {
		DialogFragment newFragment = new TimePickerFragment();
		newFragment.show(getFragmentManager(), "timePicker");
	}


	@SuppressLint({ "ValidFragment", "NewApi" })
	public class TimePickerFragment extends DialogFragment implements
	TimePickerDialog.OnTimeSetListener {

		@TargetApi(Build.VERSION_CODES.HONEYCOMB)
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current time as the default values for the picker
			final Calendar c = Calendar.getInstance();
			int hour = c.get(Calendar.HOUR_OF_DAY);
			int minute = c.get(Calendar.MINUTE);

			// Create a new instance of TimePickerDialog and return it
			return new TimePickerDialog(getActivity(), this, hour, minute,
					DateFormat.is24HourFormat(getActivity()));
		}

		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			String tempHour = hourOfDay < 10 ? "0"+hourOfDay : ""+hourOfDay;
			String tempMinute = minute < 10 ? "0"+minute : ""+minute; 
			setTime.setText(new StringBuilder().append(tempHour)
					.append(":").append(tempMinute));

		}
	}

	public class AddShowsAsync extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(context);
			progressDialog.setCancelable(false);
			progressDialog.setMessage("Please wait...");
			progressDialog.show();

		}

		@Override
		protected String doInBackground(String... params) {

			return restInteraction.addWeddingShows(ApiUrl.addWeddingShows
					+ "user_id=" + Util.loginData.getUserId()
					+ "&start_date=" + startdate
					+ "&end_date=" + endDate
					+ "&venue=" + venue
					+ "&time=" + time
					+ "&member=" + member);
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			progressDialog.dismiss();
			if (result.equals("Successful")) {
				setStartDate.setText("StartDate");
				setEndDate.setText("EndDate");
				showMembers.setText("");
				showVenue.setText("");
				setTime.setText("Time");
				Toast.makeText(context, "Successfully Added.", Toast.LENGTH_SHORT)
				.show();
			}else if (result.equalsIgnoreCase("Unsuccessful")) {
				Toast.makeText(context, "Not Added", Toast.LENGTH_SHORT)
				.show();
			}  
			else {
				Toast.makeText(context, "Please check internet connection.", Toast.LENGTH_SHORT)
				.show();
			}
		}
	}
}
