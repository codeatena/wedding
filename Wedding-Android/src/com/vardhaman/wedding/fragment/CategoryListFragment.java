package com.vardhaman.wedding.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.vardhaman.wedding.R;
import com.vardhaman.wedding.SupplierDetailActivity;
import com.vardhaman.wedding.SupplierGroup;
import com.vardhaman.wedding.adapter.CategoryListAdapter;
import com.vardhaman.wedding.restinteraction.RESTInteraction;
import com.vardhaman.wedding.util.ApiUrl;
import com.vardhaman.wedding.util.Util;

public class CategoryListFragment extends Fragment implements OnClickListener{
	
	Context context;
	View view;
	ImageView backNavigation;
	ListView listView;
	Button addGuest;
	RESTInteraction restInteraction;
	String categoryId;
	TextView backNavigationCategory_1;
	
	@Override
	public View onCreateView(LayoutInflater inflater, android.view.ViewGroup container, Bundle savedInstanceState) {
		context = getActivity();
		view = inflater.inflate(R.layout.category_detail, container, false);
		init();
		return view;
	}
	
	@Override
	public void onResume() {
		new CategoryListAsync().execute();
		super.onResume();
	}
	
	void init(){
		Util.getCategoryDetail.clear();
		restInteraction = RESTInteraction.getInstance(context);
		listView = (ListView)view.findViewById(R.id.categoryListView);
		backNavigation = (ImageView)view.findViewById(R.id.backNavigationCategory);
		backNavigation.setOnClickListener(this);
		backNavigationCategory_1 = (TextView)view.findViewById(R.id.backNavigationCategory_1);
		backNavigationCategory_1.setOnClickListener(this);
		Bundle bundle = getArguments();
		categoryId = bundle.getString("categoryId");
		
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
//				startActivity(new Intent(context, SupplierDetailActivity.class).putExtra("supplierId", Util.getCategoryDetail.get(pos).getId()));
			}
		});
	
	}
	public class CategoryListAsync extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(context);
			progressDialog.setCancelable(false);
			progressDialog.setMessage("Please wait...");
			progressDialog.show();

		}

		@Override
		protected String doInBackground(String... params) {
				return restInteraction.fetchCategoryList(ApiUrl.SupplierDetailListURL + "cate_id=" + categoryId);
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
		
			progressDialog.dismiss();
			if (result.equals("Successful")) {
				listView.setAdapter(new CategoryListAdapter(context, R.layout.category_list_item, Util.getCategoryDetail));
			}
			else if (result.equals("Unsuccessful")) {
				Toast.makeText(context, "No Suppliers in the list.", Toast.LENGTH_SHORT)
				.show();
			}
			else{
				Toast.makeText(context, "Please check internet connection.", Toast.LENGTH_SHORT)
						.show();
			}
		}
	}

	@Override
	public void onClick(View arg0) {
		SupplierGroup followGroup1 = (SupplierGroup)context;		
		switch (arg0.getId()) {
		case R.id.backNavigationCategory:
			followGroup1.supplierFragment = new SupplierFragment();
			followGroup1.unLoadFragment2();
			followGroup1.loadFragment(R.id.fmLayout, followGroup1.supplierFragment);
			break;
		case R.id.backNavigationCategory_1:
			followGroup1.supplierFragment = new SupplierFragment();
			followGroup1.unLoadFragment2();
			followGroup1.loadFragment(R.id.fmLayout, followGroup1.supplierFragment);
			break;

		default:
			break;
		}
	}
}
