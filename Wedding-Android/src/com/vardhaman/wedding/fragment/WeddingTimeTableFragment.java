package com.vardhaman.wedding.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vardhaman.wedding.MyWeddingGroup;
import com.vardhaman.wedding.R;

public class WeddingTimeTableFragment extends Fragment implements OnClickListener{
	Context context;
	View view;
	ImageView backNav;
	LinearLayout twelveText, nineText, sixText, threeText, oneText, nightBefore, onWeddingDay;
	public static int count = 0;
	TextView ivBackNavTimeTable_1;

	public View onCreateView(LayoutInflater inflater, android.view.ViewGroup container, Bundle savedInstanceState) {
		context = getActivity();
		view = inflater.inflate(R.layout.wedding_timetable_layout, container, false);
		init();
		return view;
	}

	void init(){
		twelveText = (LinearLayout)view.findViewById(R.id.twelveText);
		twelveText.setOnClickListener(this);
		nineText = (LinearLayout)view.findViewById(R.id.nineText);
		nineText.setOnClickListener(this);
		sixText = (LinearLayout)view.findViewById(R.id.sixText);
		sixText.setOnClickListener(this);
		threeText = (LinearLayout)view.findViewById(R.id.threeText);
		threeText.setOnClickListener(this);
		oneText = (LinearLayout)view.findViewById(R.id.oneText);
		oneText.setOnClickListener(this);
		nightBefore = (LinearLayout)view.findViewById(R.id.nightBeforeText);
		nightBefore.setOnClickListener(this);
		onWeddingDay = (LinearLayout)view.findViewById(R.id.weddingText);
		onWeddingDay.setOnClickListener(this);
		backNav = (ImageView)view.findViewById(R.id.ivBackNavTimeTable);
		backNav.setOnClickListener(this);
		ivBackNavTimeTable_1 = (TextView)view.findViewById(R.id.ivBackNavTimeTable_1);
		ivBackNavTimeTable_1.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		MyWeddingGroup followGroup1 = (MyWeddingGroup)context;
		switch (v.getId()) {
		case R.id.twelveText:
			count = 1;
			followGroup1.tableDetailsFragment = new TimeTableDetailsFragment();
			followGroup1.unLoadFragment5();
			followGroup1.loadFragment(R.id.fmLayout, followGroup1.tableDetailsFragment);
			break;
		case R.id.nineText:
			count = 2;
			followGroup1.tableDetailsFragment = new TimeTableDetailsFragment();
			followGroup1.unLoadFragment5();
			followGroup1.loadFragment(R.id.fmLayout, followGroup1.tableDetailsFragment);
			break;
		case R.id.sixText:
			count = 3;
			followGroup1.tableDetailsFragment = new TimeTableDetailsFragment();
			followGroup1.unLoadFragment5();
			followGroup1.loadFragment(R.id.fmLayout, followGroup1.tableDetailsFragment);
			break;
		case R.id.threeText:
			count = 4;
			followGroup1.tableDetailsFragment = new TimeTableDetailsFragment();
			followGroup1.unLoadFragment5();
			followGroup1.loadFragment(R.id.fmLayout, followGroup1.tableDetailsFragment);
			break;
		case R.id.oneText:
			count = 5;
			followGroup1.tableDetailsFragment = new TimeTableDetailsFragment();
			followGroup1.unLoadFragment5();
			followGroup1.loadFragment(R.id.fmLayout, followGroup1.tableDetailsFragment);
			break;
		case R.id.nightBeforeText:
			count = 6;
			followGroup1.tableDetailsFragment = new TimeTableDetailsFragment();
			followGroup1.unLoadFragment5();
			followGroup1.loadFragment(R.id.fmLayout, followGroup1.tableDetailsFragment);
			break;
		case R.id.weddingText:
			count = 7;
			followGroup1.tableDetailsFragment = new TimeTableDetailsFragment();
			followGroup1.unLoadFragment5();
			followGroup1.loadFragment(R.id.fmLayout, followGroup1.tableDetailsFragment);
			break;
		case R.id.ivBackNavTimeTable:
			followGroup1.myWeddingFragment = new MyWeddingFragment();
			followGroup1.unLoadFragment5();
			followGroup1.loadFragment(R.id.fmLayout, followGroup1.myWeddingFragment);
			break;
		case R.id.ivBackNavTimeTable_1:
			followGroup1.myWeddingFragment = new MyWeddingFragment();
			followGroup1.unLoadFragment5();
			followGroup1.loadFragment(R.id.fmLayout, followGroup1.myWeddingFragment);
			break;

		default:
			break;
		}
	}


}
