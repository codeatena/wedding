package com.vardhaman.wedding.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.vardhaman.wedding.R;
import com.vardhaman.wedding.SupplierGroup;

public class SupplierDetailFragment extends Fragment implements OnClickListener{
	Context context;
	View view;
	ImageView backNavigation;
	TextView backNavigationSupplier_1;

	@Override
	public View onCreateView(LayoutInflater inflater, android.view.ViewGroup container, Bundle savedInstanceState) {
		context = getActivity();
		view = inflater.inflate(R.layout.supplier_detail_layout, container, false);
		init();
		return view;
	}
	
	void init(){
		backNavigation = (ImageView)view.findViewById(R.id.backNavigationSupplier);
		backNavigation.setOnClickListener(this);
		backNavigationSupplier_1 = (TextView)view.findViewById(R.id.backNavigationSupplier_1);
		backNavigationSupplier_1.setOnClickListener(this);
		Bundle bundle = getArguments();
		String supplierId = bundle.getString("supplierId");
	}

	@Override
	public void onClick(View v) {
		SupplierGroup followGroup1 = (SupplierGroup)context;
		switch (v.getId()) {
		case R.id.backNavigationSupplier:
			followGroup1.categoryListFragment = new CategoryListFragment();
			followGroup1.unLoadFragment3();
			followGroup1.loadFragment(R.id.fmLayout, followGroup1.categoryListFragment);
			break;
		case R.id.backNavigationSupplier_1:
			followGroup1.categoryListFragment = new CategoryListFragment();
			followGroup1.unLoadFragment3();
			followGroup1.loadFragment(R.id.fmLayout, followGroup1.categoryListFragment);
			break;

		default:
			break;
		}
		
	}

}
