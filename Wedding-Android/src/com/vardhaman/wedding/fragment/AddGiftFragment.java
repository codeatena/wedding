package com.vardhaman.wedding.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.vardhaman.wedding.MyWeddingGroup;
import com.vardhaman.wedding.R;
import com.vardhaman.wedding.adapter.GuestListAdapter.AddGiftAsync;
import com.vardhaman.wedding.restinteraction.RESTInteraction;
import com.vardhaman.wedding.util.ApiUrl;
import com.vardhaman.wedding.util.Util;

public class AddGiftFragment extends Fragment implements OnClickListener{
	Context context;
	View view;
	ImageView backNavigation_addgift;
	TextView backNavigation_addgift_1;
	AutoCompleteTextView giftUserFirstname;
	EditText giftNameUser;
	RESTInteraction restInteraction;
	int pos;
	String guestId;
	Boolean check = false;
	Button addGift;
	boolean flag = false;

	@Override
	public View onCreateView(LayoutInflater inflater, android.view.ViewGroup container, Bundle savedInstanceState) {
		context = getActivity();
		view = inflater.inflate(R.layout.add_gift_layout, container, false);
		init();
		return view;
	}

	void init(){
		backNavigation_addgift = (ImageView)view.findViewById(R.id.backNavigation_addgift);
		backNavigation_addgift.setOnClickListener(this);
		backNavigation_addgift_1 = (TextView)view.findViewById(R.id.backNavigation_addgift_1);
		backNavigation_addgift_1.setOnClickListener(this);
		giftUserFirstname = (AutoCompleteTextView)view.findViewById(R.id.giftUserFirstname);
		giftNameUser = (EditText)view.findViewById(R.id.giftNameUser);
		restInteraction = RESTInteraction.getInstance(context);
		addGift = (Button)view.findViewById(R.id.btnAddGift);
		addGift.setOnClickListener(this);
		giftUserFirstname.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				flag = true;
				String guestName = giftUserFirstname.getText().toString();
				for (int i = 0; i < restInteraction.allGuestNames.length; i++) {
					if (restInteraction.allGuestNames[i].equalsIgnoreCase(guestName)) {
						pos = i;

					}
				}
				guestId = restInteraction.allGuestIds[pos];
				System.out.println(guestId + ":-):-):-):-):-):-):-)");

			}
		});
	}
	@Override
	public void onResume() {
		new GuestListAsync().execute();
		super.onResume();
	} 

	@Override
	public void onClick(View v) {
		MyWeddingGroup followGroup1 = (MyWeddingGroup) context;
		switch (v.getId()) {
		case R.id.backNavigation_addgift:
			followGroup1.giftListFragment = new GiftListFragment();
			followGroup1.unLoadFragment10();
			followGroup1.loadFragment(R.id.fmLayout,followGroup1.giftListFragment);

			break;
		case R.id.backNavigation_addgift_1:
			followGroup1.giftListFragment = new GiftListFragment();
			followGroup1.unLoadFragment10();
			followGroup1.loadFragment(R.id.fmLayout,followGroup1.giftListFragment);

			break;
		case R.id.btnAddGift:
			if (!flag) {
				Toast.makeText(context, "Please Select Guest only from existing list.", Toast.LENGTH_SHORT).show();
				
			}else {
			flag = false;
			if (guestId != null && !guestId.matches("") && giftNameUser.getText().toString().length()>0) {
				for (int i = 0; i < Util.getGiftDetail.size(); i++) {
					if (guestId.equalsIgnoreCase(Util.getGiftDetail.get(i).getId())) {
						check = true;
						showAlertDialog(guestId, giftNameUser.getText().toString());
						break;
					}
				}
				
				if (check == false) {
					new AddGiftAsync(guestId, giftNameUser.getText().toString()).execute();
					
				}
				check = false;
				}else {
					Toast.makeText(context, "Please add gift.", Toast.LENGTH_SHORT).show();
				}
			}
//			new AddGiftAsync(guestId, giftNameUser.getText().toString()).execute();
			break;

		default:
			break;
		}
	}
	public class GuestListAsync extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;
		String spinnerString2WithoutSpace;


		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(context);
			progressDialog.setCancelable(false);
			progressDialog.setMessage("Please wait...");
			progressDialog.show();

		}

		@Override
		protected String doInBackground(String... params) {

			return restInteraction.GetAllGuest(ApiUrl.getAllGuestUrl
					+ "user_id=" + Util.loginData.getUserId());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			progressDialog.dismiss();
			if (result.equals("Successful")) {
				for (int j = 0; j < restInteraction.allGuestNames.length; j++) {

					ArrayAdapter adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, restInteraction.allGuestNames);
					giftUserFirstname.setAdapter(adapter);

				}
			}else if (result.equalsIgnoreCase("Unsuccessful")) {
				Toast.makeText(context, "There is no Guest in your list.", Toast.LENGTH_SHORT)
				.show();
			}  
			else {
				Toast.makeText(context, "Please check internet connection.", Toast.LENGTH_SHORT)
				.show();
			}
		}
	}
	
	void showAlertDialog(final String guestId, final String gift){
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

		// set title
		alertDialogBuilder.setTitle("Confirmation!");

		// set dialog message
		alertDialogBuilder
		.setMessage("Are you sure? We see you have already entered a gift earlier.")
		.setCancelable(false)
		.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				// if this button is clicked, close
				// current activity
				new AddGiftAsync(guestId, gift).execute();
				dialog.dismiss();
			}
		})
		.setNegativeButton("No",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				// if this button is clicked, just close
				// the dialog box and do nothing
				dialog.cancel();
			}
		});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}

	public class AddGiftAsync extends AsyncTask<String, Void, String> {
		String guestId;
		String gift;
		ProgressDialog progressDialog;

		public AddGiftAsync(String guestId, String gift) {
			this.guestId = guestId;
			this.gift = gift;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(context);
			progressDialog.setCancelable(false);
			progressDialog.setMessage("Please wait...");
			progressDialog.show();

		}

		@Override
		protected String doInBackground(String... params) {

			return restInteraction.addGift(ApiUrl.AddGiftUrl + "guest_id="
					+ guestId + "&user_id=" +  Util.loginData.getUserId() + "&grift=" + gift);
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			progressDialog.dismiss();

			if (result.equals("Successful")) {
				Toast.makeText(context, "Gift added successfully.", Toast.LENGTH_SHORT).show();
			} else {
				//				Toast.makeText(context, "Please check internet connection.", Toast.LENGTH_SHORT).show();
			}
//			((MyWeddingGroup)context).guestListFragment.new GuestListAsync(GuestListFragment.spinnerString2WithoutSpace).execute();
		}
	}

}
