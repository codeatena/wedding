package com.vardhaman.wedding.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.vardhaman.wedding.MyWeddingGroup;
import com.vardhaman.wedding.R;
import com.vardhaman.wedding.adapter.GiftListAdapter;
import com.vardhaman.wedding.restinteraction.RESTInteraction;
import com.vardhaman.wedding.util.ApiUrl;
import com.vardhaman.wedding.util.Util;

public class GiftListFragment extends Fragment implements OnClickListener{
	Context context;
	View view;
	ImageView backNavigation_giftList;
	ListView listView;
	Integer images[] = {R.drawable.p1, R.drawable.p2, R.drawable.p3};
	Integer dotImages[] = {R.drawable.reddot, R.drawable.greendot, R.drawable.reddot2};
	RESTInteraction restInteraction;
	TextView backNavigation_giftList_1;
	Button addGiftAdd;
	
	@Override
	public View onCreateView(LayoutInflater inflater, android.view.ViewGroup container, Bundle savedInstanceState) {
		context = getActivity();
		view = inflater.inflate(R.layout.gift_list, container, false);
		init();
		return view;
	}
	
	void init(){
		restInteraction = RESTInteraction.getInstance(context);
		listView = (ListView)view.findViewById(R.id.giftListView);
		addGiftAdd = (Button)view.findViewById(R.id.addGiftAdd);
		addGiftAdd.setOnClickListener(this);
		backNavigation_giftList = (ImageView)view.findViewById(R.id.backNavigation_giftList);
		backNavigation_giftList.setOnClickListener(this);
		backNavigation_giftList_1 = (TextView)view.findViewById(R.id.backNavigation_giftList_1);
		backNavigation_giftList_1.setOnClickListener(this);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				final String giftId = Util.getGiftDetail.get(pos).getgiftId();
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		 
					alertDialogBuilder.setTitle("Delete Gift..!");
		 
					alertDialogBuilder
						.setMessage("You want to delete this gift?")
						.setCancelable(false)
						.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								new DeleteGiftAsyn(giftId).execute();
								
							}
						  })
						.setNegativeButton("No",new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,int id) {
								// if this button is clicked, just close
								// the dialog box and do nothing
								dialog.cancel();
							}
						});
		 
						// create alert dialog
						AlertDialog alertDialog = alertDialogBuilder.create();
		 
						// show it
						alertDialog.show();
			}
		});
	}
	
	@Override
	public void onResume() {
		new GiftListAsync().execute();
		super.onResume();
	}

	@Override
	public void onClick(View v) {
		MyWeddingGroup followGroup = (MyWeddingGroup)context;		
		switch (v.getId()) {
		case R.id.backNavigation_giftList:
			followGroup.myWeddingFragment = new MyWeddingFragment();
			followGroup.unLoadFragment2();
			followGroup.loadFragment(R.id.fmLayout, followGroup.myWeddingFragment);
			break;
		case R.id.backNavigation_giftList_1:
			followGroup.myWeddingFragment = new MyWeddingFragment();
			followGroup.unLoadFragment2();
			followGroup.loadFragment(R.id.fmLayout, followGroup.myWeddingFragment);
			break;
		case R.id.addGiftAdd:
			followGroup.addGiftFragment = new AddGiftFragment();
			followGroup.unLoadFragment2();
			followGroup.loadFragment(R.id.fmLayout, followGroup.addGiftFragment);
			break;

		default:
			break;
		}
		
	}
	
	
	public class GiftListAsync extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(context);
			progressDialog.setCancelable(false);
			progressDialog.setMessage("Please wait...");
			progressDialog.show();

		}

		@Override
		protected String doInBackground(String... params) {

			return restInteraction.getAllGift(ApiUrl.GetGiftsUrl
					+ "user_id=" + Util.loginData.getUserId());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
		
			progressDialog.dismiss();
			if (result.equals("Successful")) {
				listView.setAdapter(new GiftListAdapter(context, R.layout.giftlist_item, Util.getGiftDetail));
			}else if (result.equalsIgnoreCase("Unsuccessful")) {
				Toast.makeText(context, "Ther is no gifts. Please add first..!", Toast.LENGTH_SHORT)
				.show();
			}  
			else {
				Toast.makeText(context, "Please check internet connection.", Toast.LENGTH_SHORT)
						.show();
			}
		}
	}
	public class DeleteGiftAsyn extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;
		String giftId;

		public DeleteGiftAsyn(String giftId) {
			this.giftId = giftId;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(context);
			progressDialog.setCancelable(false);
			progressDialog.setMessage("Please wait...");
//			progressDialog.show();

		}

		@Override
		protected String doInBackground(String... params) {

			return restInteraction.deleteGift(ApiUrl.deleteGiftUrl
					+ "id=" + giftId
					+ "&user_id=" + Util.loginData.getUserId());
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

//			progressDialog.dismiss();
			if (result.equals("Successful")) {
				new GiftListAsync().execute();
				Toast.makeText(context, "Gift successfully deleted.", Toast.LENGTH_SHORT)
				.show();
			}else if (result.equalsIgnoreCase("Unsuccessful")) {
				Toast.makeText(context, "Oops error occured. Try later..!", Toast.LENGTH_SHORT)
				.show();
			}  
			else {
				Toast.makeText(context, "Please check internet connection.", Toast.LENGTH_SHORT)
				.show();
			}
		}
	}
	
}
