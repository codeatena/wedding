package com.vardhaman.wedding.restinteraction;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ActionBar;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.vardhaman.wedding.model.CategoryModel;
import com.vardhaman.wedding.model.FavModel;
import com.vardhaman.wedding.model.GiftListModel;
import com.vardhaman.wedding.model.GuestList;
import com.vardhaman.wedding.model.OffersModel;
import com.vardhaman.wedding.model.Shows;
import com.vardhaman.wedding.model.SupplierModel;
import com.vardhaman.wedding.util.MySQLiteHelper;
import com.vardhaman.wedding.util.Util;

public class RESTInteraction {

	private static RESTInteraction _instance;
	private DefaultHttpClient httpclient;
	private HttpPost postRequest;
	private HttpResponse httpResponse;
	private HttpEntity httpEntity;
	Context mContext;
	SharedPreferences sharedPreferences;
	public String userId;
	public String fName, lNmae, addrss, cty, mail, birthdate, cntry;
	private ArrayList<NameValuePair> nameValuePairs;
	public static String fromId;
	public String message;
	public String toId;
	public static String statusCheck;
	private HttpGet getRequest;
	private HttpDelete delRequest;
	public String supplier_cat_id, suppliername, id, phoneNo, address, email, web, description, productImage;
	public String imagesArray[];
	MySQLiteHelper mySQLiteHelper;
	public String lat, lng, place;
	public static String[] guestNames, guestIds;
	public static String[] allGuestNames, allGuestIds;
	
	public RESTInteraction(Context ctx) { 
		mContext = ctx;
	}

	public static RESTInteraction getInstance(Context ctx) {
		if (_instance == null) {
			_instance = new RESTInteraction(ctx);
		}
		return _instance;
	}

	private void initializeHttpClient() {
		httpclient = new DefaultHttpClient();
		nameValuePairs = new ArrayList<NameValuePair>();
	}

	private String sendHttpRequest(String url) {
		String jSonStr = null;
		try {
			postRequest = new HttpPost(url);
			postRequest.setEntity(new UrlEncodedFormEntity(nameValuePairs,
					HTTP.UTF_8));
			httpResponse = httpclient.execute(postRequest);
			httpEntity = httpResponse.getEntity();
			if (httpEntity != null) {
				jSonStr = EntityUtils.toString(httpEntity);
				return jSonStr;
			}
		} catch (Exception e) {
			if (e != null) {
				e.getMessage();
			}
		}
		return jSonStr;
	}

	private String sendHttpGetRequest(String url) {
		String jSonStr = null;
		try {
			httpclient = new DefaultHttpClient();
			getRequest = new HttpGet(url);
			httpResponse = httpclient.execute(getRequest);
			httpEntity = httpResponse.getEntity();
			if (httpEntity != null) {
				jSonStr = EntityUtils.toString(httpEntity);
				return jSonStr;
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jSonStr;
	}

	public String sendSignUpRequest(String url) {
		try {
			JSONObject jsonObj = new JSONObject(sendHttpGetRequest(url));
			if (jsonObj.has("success")) {
				if (jsonObj.getString("success").equals("1")) {
					userId = jsonObj.getString("returnid");

					return "Successful";
				} else {
					return jsonObj.getString("message");
				}
			}
			return null;
		} catch (Exception e) {
			e.getMessage();
		}
		return null;
	}

	public String sendLoginRequest(String url) {
		try {
			JSONObject jsonObj = new JSONObject(sendHttpGetRequest(url));
			if (jsonObj.has("success")) {
				if (jsonObj.getString("success").equals("1")) {
					userId = jsonObj.getString("returnid");
					Util.loginData.setUserId(userId);
					return "Successful";
				} else {
					return "Unsuccessful";
				}
			}
		} catch (Exception e) {
			e.getMessage();
		}
		return "Login fail";
	}

	public String addGuest(String url) {
		try {
			JSONObject jsonObj = new JSONObject(sendHttpGetRequest(url));
			if (jsonObj.has("success")) {
				if (jsonObj.getString("success").equals("1")) {

					return "Successful";
				} else {
					return "Unsuccessful";
				}
			}
		} catch (Exception e) {
			e.getMessage();
		}
		return "Login fail";
	}

	public String checkStatus(String url) {
		try {
			JSONObject jsonObj = new JSONObject(sendHttpGetRequest(url));
			if (jsonObj.has("success")) {
				if (jsonObj.getString("success").equals("1")) {

					return "Successful";
				} else {
					return "Unsuccessful";
				}
			}
		} catch (Exception e) {
			e.getMessage();
		}
		return "Login fail";
	}

	public String getSupplierList(String url) {
		try {
			Log.e("supplier url", url);
			Util.getCategoryList.clear();
			JSONObject jsonObj = new JSONObject(sendHttpGetRequest(url));
			Log.e("supplier response", jsonObj.toString());
			if (jsonObj.has("success")) {
				if (jsonObj.getString("success").equals("1")) {
					JSONArray jsonArray = jsonObj
							.getJSONArray("get_suppler_category");
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jObject = jsonArray.getJSONObject(i);
						Util.getCategoryList.add(new SupplierModel(jObject
								.getString("id"), jObject
								.getString("category_name"), jObject
								.getString("category_image")));

					}
					return "Successful";
				} else {
					return "Unsuccessful";
				}
			}
		} catch (Exception e) {
			e.getMessage();
		}
		return "Network Error";
	}

	public String fetchGuestList(String url) {
		mySQLiteHelper = new MySQLiteHelper(mContext);
		userId = Util.loginData.getUserId();
		try {
			Util.guestArrayList.clear();
			JSONObject jsonObj = new JSONObject(sendHttpGetRequest(url));
			if (jsonObj.has("success")) {
				if (jsonObj.getString("success").equals("1")) {
					JSONArray jsonArray = jsonObj
							.getJSONArray("get_guest_user");
					guestNames = new String[jsonArray.length()];
					guestIds = new String[jsonArray.length()];
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jObject = jsonArray.getJSONObject(i);
						Util.guestArrayList.add(new GuestList(jObject
								.getString("id"),
								jObject.getString("fistname"), jObject
								.getString("lastname"), jObject
								.getString("relation"), jObject
								.getString("contact_no"), jObject
								.getString("email"), jObject
								.getString("address"), jObject
								.getString("profile_pic"), jObject
								.getString("favouite"), jObject
								.getString("status")));
						guestIds[i] = jObject.getString("id");
						guestNames[i] =  jObject.getString("fistname");

//						ContentValues contentValues = new ContentValues();
//						contentValues.put(MySQLiteHelper.KEY_USERID, userId);
//						contentValues.put(MySQLiteHelper.KEY_GUEST_ID, jObject.getString("id"));
//						contentValues.put(MySQLiteHelper.KEY_GUEST_FIRSTNAME, jObject.getString("fistname"));
//						contentValues.put(MySQLiteHelper.KEY_GUEST_LASTNAME, jObject.getString("lastname"));
//						contentValues.put(MySQLiteHelper.KEY_GUEST_RELATION, jObject.getString("relation"));
//						contentValues.put(MySQLiteHelper.KEY_GUEST_CONTACT, jObject.getString("contact_no"));
//						contentValues.put(MySQLiteHelper.KEY_GUEST_EMAIL, jObject.getString("email"));
//						contentValues.put(MySQLiteHelper.KEY_GUEST_ADDRESS, jObject.getString("address"));
//						contentValues.put(MySQLiteHelper.KEY_GUEST_PROFILE_PIC, jObject.getString("profile_pic"));
//						contentValues.put(MySQLiteHelper.KEY_FAVOURITE, jObject.getString("favouite"));
//						contentValues.put(MySQLiteHelper.KEY_STATUS, jObject.getString("status"));
//						mySQLiteHelper.addAllGuestToDB(jObject.getString("id"), contentValues);

					}
					return "Successful";
				} else {
					return "Unsuccessful";
				}
			}
		} catch (Exception e) {
			e.getMessage();
		}
		return "Login fail";
	}

	
	public String GetAllGuest(String url) {
		mySQLiteHelper = new MySQLiteHelper(mContext);
		userId = Util.loginData.getUserId();
		try {
			
			JSONObject jsonObj = new JSONObject(sendHttpGetRequest(url));
			if (jsonObj.has("success")) {
				if (jsonObj.getString("success").equals("1")) {
					JSONArray jsonArray = jsonObj
							.getJSONArray("get_guest");
					allGuestNames = new String[jsonArray.length()];
					allGuestIds = new String[jsonArray.length()];
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jObject = jsonArray.getJSONObject(i);
						
						allGuestIds[i] = jObject.getString("id");
						allGuestNames[i] =  jObject.getString("fistname");
					}
					return "Successful";
				} else {
					return "Unsuccessful";
				}
			}
		} catch (Exception e) {
			e.getMessage();
		}
		return "Login fail";
	}
	public String fetchCategoryList(String url) {

		try {
			Util.getCategoryDetail.clear();
			JSONObject jsonObj = new JSONObject(sendHttpGetRequest(url));
			if (jsonObj.has("success")) {
				String success = jsonObj.getString("success");
				if (success.equalsIgnoreCase("1")) {
					JSONArray jsonArray = jsonObj.getJSONArray("suppliers");
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject object = jsonArray.getJSONObject(i);
						Util.getCategoryDetail.add(new CategoryModel(
								object.getString("id"),
								object.getString("suppler_name"), 
								object.getString("image"), 
								object.getString("city"), 
								object.getInt("offer_exist"),
								object.getString("suppler_category_id")));
					}

					return "Successful";
				} else {
					return "Unsuccessful";
				}
			} else {
				return "Unsuccessful";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Login fail";
	}

	public String fetchProductList(String url) {

		try {
			JSONObject jsonObj = new JSONObject(sendHttpGetRequest(url));
			if (jsonObj.has("success")) {
				imagesArray = new String[0];
				String success = jsonObj.getString("success");
				if (success.equalsIgnoreCase("1")) {
					suppliername = jsonObj.getString("suppler_name");
					id = jsonObj.getString("id");
					supplier_cat_id = jsonObj.getString("suppler_category_id");
					phoneNo = jsonObj.getString("suppler_phone");
					address = jsonObj.getString("address");
					email = jsonObj.getString("email");
					web = jsonObj.getString("web");
					description = jsonObj.getString("description");
					String image = jsonObj.getString("image");
					JSONArray jsonArray = jsonObj.getJSONArray("inventorys");
					imagesArray = new String[jsonArray.length()];
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject object = jsonArray.getJSONObject(i);
						productImage = object.getString("image");
						imagesArray[i] = productImage;
					}

					return "Successful";
				} else {
					return "Unsuccessful";
				}
			} else {
				return "Unsuccessful";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Login fail";
	}
	public String forgetPassword(String url) {

		try {
			JSONObject jsonObj = new JSONObject(sendHttpGetRequest(url));
			if (jsonObj.has("success")) {
				String success = jsonObj.getString("success");
				if (success.equalsIgnoreCase("1")) {
					message = jsonObj.getString("message");

					return "Successful";
				} else {
					return "Unsuccessful";
				}
			} else {
				return "Unsuccessful";
			}

		} catch (Exception e) {
			e.getMessage();
		}
		return "Login fail";
	}

	public String ceremonyTable(String url) {

		try {
			JSONObject jsonObj = new JSONObject(sendHttpGetRequest(url));
			if (jsonObj.has("success")) {
				String success = jsonObj.getString("success");
				if (success.equalsIgnoreCase("1")) {
					message = jsonObj.getString("message");

					return "Successful";
				} else {
					return "Unsuccessful";
				}
			} else {
				return "Unsuccessful";
			}

		} catch (Exception e) {
			e.getMessage();
		}
		return "Failed";
	}

	public String weddingTable(String url) {

		try {
			JSONObject jsonObj = new JSONObject(sendHttpGetRequest(url));
			if (jsonObj.has("success")) {
				String success = jsonObj.getString("success");
				if (success.equalsIgnoreCase("1")) {
					message = jsonObj.getString("message");

					return "Successful";
				} else {
					return "Unsuccessful";
				}
			} else {
				return "Unsuccessful";
			}

		} catch (Exception e) {
			e.getMessage();
		}
		return "Failed";
	}

	public String receptionTable(String url) {

		try {
			JSONObject jsonObj = new JSONObject(sendHttpGetRequest(url));
			if (jsonObj.has("success")) {
				String success = jsonObj.getString("success");
				if (success.equalsIgnoreCase("1")) {
					message = jsonObj.getString("message");

					return "Successful";
				} else {
					return "Unsuccessful";
				}
			} else {
				return "Unsuccessful";
			}

		} catch (Exception e) {
			e.getMessage();
		}
		return "Failed";
	}

	public String favGuestList(String url) {
		try {
			Util.getFavouriteList.clear();
			JSONObject jsonObj = new JSONObject(sendHttpGetRequest(url));
			if (jsonObj.has("success")) {
				if (jsonObj.getString("success").equals("1")) {
					JSONArray jsonArray = jsonObj.getJSONArray("Favorites");
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jObject = jsonArray.getJSONObject(i);
						Util.getFavouriteList.add(new FavModel(jObject.getString("id"),
								jObject.getString("suppler_name"), jObject
								.getString("supplier_id"), jObject
								.getString("image")));

					}
					return "Successful";
				} else {
					return "Unsuccessful";
				}
			}
		} catch (Exception e) {
			e.getMessage();
		}
		return "Login fail";
	}

	public String addGift(String url) {

		try {
			JSONObject jsonObj = new JSONObject(sendHttpGetRequest(url));
			if (jsonObj.has("success")) {
				String success = jsonObj.getString("success");
				if (success.equalsIgnoreCase("1")) {

					return "Successful";
				} else {
					return "Unsuccessful";
				}
			} else {
				return "Unsuccessful";
			}

		} catch (Exception e) {
			e.getMessage();
		}
		return "Failed";
	}

	public String getAllGift(String url) {

		try {
			Util.getGiftDetail.clear();
			JSONObject jsonObj = new JSONObject(sendHttpGetRequest(url));
			if (jsonObj.has("success")) {
				String success = jsonObj.getString("success");
				if (success.equalsIgnoreCase("1")) {
					JSONArray jsonArray = jsonObj.getJSONArray("guest");
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jObject = jsonArray.getJSONObject(i);
						Util.getGiftDetail.add(new GiftListModel(jObject
								.getString("girft_id"),jObject
								.getString("guest_id"),
								jObject.getString("firstname"), jObject
								.getString("lastname"), jObject
								.getString("grift"), jObject
								.getString("relation"), jObject
								.getString("profile_pic"), 
								jObject.getString("favouite")));
					}
					return "Successful";
				} else {
					return "Unsuccessful";
				}
			} else {
				return "Unsuccessful";
			}

		} catch (Exception e) {
			e.getMessage();
		}
		return "Failed";
	}

	public String getAllOffers(String url) {

		try {
			Util.getOfferDetail.clear();
			JSONObject jsonObj = new JSONObject(sendHttpGetRequest(url));
			if (jsonObj.has("success")) {
				String success = jsonObj.getString("success");
				if (success.equalsIgnoreCase("1")) {
					JSONArray jsonArray = jsonObj.getJSONArray("inventorys");
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jObject = jsonArray.getJSONObject(i);
						Util.getOfferDetail.add(new OffersModel(jObject
								.getString("id"),
								jObject.getString("name"), jObject
								.getString("supplier_id"), jObject
								.getString("image")));
					}
					return "Successful";
				} else {
					return "Unsuccessful";
				}
			} else {
				return "Unsuccessful";
			}

		} catch (Exception e) {
			e.getMessage();
		}
		return "Failed";
	}

	public String getLatLong(String url) {

		try {
			JSONObject jsonObj = new JSONObject(sendHttpGetRequest(url));
			if (jsonObj.has("success")) {
				String success = jsonObj.getString("success");
				if (success.equalsIgnoreCase("1")) {
					lat = jsonObj.getString("lat");
					lng = jsonObj.getString("long");
					place = jsonObj.getString("address");
					return "Successful";
				} else {
					return "Unsuccessful";
				}
			} else {
				return "Unsuccessful";
			}

		} catch (Exception e) {
			e.getMessage();
		}
		return "Failed";
	}

	public String addWeddingShows(String url) {

		try {
			JSONObject jsonObj = new JSONObject(sendHttpGetRequest(url));
			if (jsonObj.has("success")) {
				String success = jsonObj.getString("success");
				if (success.equalsIgnoreCase("1")) {
					return "Successful";
				} else {
					return "Unsuccessful";
				}
			} else {
				return "Not Added";
			}

		} catch (Exception e) {
			e.getMessage();
		}
		return "Failed";
	}
	
	public String deleteGift(String url) {

		try {
			JSONObject jsonObj = new JSONObject(sendHttpGetRequest(url));
			if (jsonObj.has("success")) {
				String success = jsonObj.getString("success");
				if (success.equalsIgnoreCase("1")) {
					return "Successful";
				} else {
					return "Unsuccessful";
				}
			} else {
				return "Not Deleted";
			}

		} catch (Exception e) {
			e.getMessage();
		}
		return "Failed";
	}
	
	public String getAllShows(String url) {

		try {
			JSONObject jsonObj = new JSONObject(sendHttpGetRequest(url));
			if (jsonObj.has("success")) {
				String success = jsonObj.getString("success");
				Util.getShows.clear();
				if (success.equalsIgnoreCase("1")) {
					JSONArray jsonArray = jsonObj.getJSONArray("shows");
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject jsonObject = jsonArray.getJSONObject(i);
						String showId = jsonObject.getString("id");
						String showName = jsonObject.getString("name");
						String showAddress = jsonObject.getString("address");
						String showTime = jsonObject.getString("time");
						String showContact = jsonObject.getString("show");
						String showDate = jsonObject.getString("date");
						String showImage = jsonObject.getString("image");
						
						Util.getShows.add(new Shows(showId, showName, showAddress, showTime, showContact, showDate, showImage));
					}
					return "Successful";
				} else {
					return "Unsuccessful";
				}
			} else {
				return "Not Deleted";
			}

		} catch (Exception e) {
			e.getMessage();
		}
		return "Failed";
	}
	
	public String addSuppliersToFavorite(String url) {

		try {
			JSONObject jsonObj = new JSONObject(sendHttpGetRequest(url));
			if (jsonObj.has("success")) {
				String success = jsonObj.getString("success");
				if (success.equalsIgnoreCase("1")) {
//					message = jsonObj.getString("message");

					return "Successful";
				} else {
					return "Unsuccessful";
				}
			} else {
				return "Unsuccessful";
			}

		} catch (Exception e) {
			e.getMessage();
		}
		return "Login fail";
	}

}
