package com.vardhaman.wedding;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.vardhaman.wedding.restinteraction.RESTInteraction;
import com.vardhaman.wedding.util.ApiUrl;

public class VouchersActivity extends Activity implements OnClickListener{
	RESTInteraction restInteraction ;
	ImageView offerImage, shareFriends;
	Animation animBlink;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_gift_voucher);
		init();
	}
	
	void init(){
		restInteraction = RESTInteraction.getInstance(this);
		offerImage = (ImageView)findViewById(R.id.offerImage);
		offerImage.setOnClickListener(this);
		shareFriends  = (ImageView)findViewById(R.id.shareFriends);
		shareFriends.setOnClickListener(this);
		animBlink = AnimationUtils.loadAnimation(
				getApplicationContext(), R.anim.blink);
		offerImage.startAnimation(animBlink);
	}
	
	@Override
	protected void onResume() {
		new GetOffersAsync().execute();
		super.onResume();
	} 
	
	public class GetOffersAsync extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(VouchersActivity.this);
			progressDialog.setCancelable(false);
			progressDialog.setMessage("Please wait...");
//			progressDialog.show();

		}

		@Override
		protected String doInBackground(String... params) {
				return restInteraction.getAllOffers(ApiUrl.GetOfferssUrl + "supplier_id=" + "24");
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
		
//			progressDialog.dismiss();
			if (result.equals("Successful")) {
//				for (int j = 0; j < Util.getOfferDetail.size(); j++) {
//					Util.loadImage(offerImage,Util.getOfferDetail.get(j).getImage(), 0);
//				}
			}else if (result.equals("Unsuccessful")) {
//				Toast.makeText(VouchersActivity.this, "No offers are present at this time.", Toast.LENGTH_SHORT)
//				.show();
			}
			else {
//				Toast.makeText(VouchersActivity.this, "Please check internet connection.", Toast.LENGTH_SHORT)
//						.show();
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.shareFriends:
			Intent sharingIntent = new Intent(Intent.ACTION_SEND);
			sharingIntent.setType("text/plain");
			sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT,
					"Hey, I found this new app. Check this.");
			sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
					"Check this new app.");
			startActivity(Intent.createChooser(sharingIntent, "Share using"));
			break;
		case R.id.offerImage:
			offerImage.clearAnimation();
			break;

		default:
			break;
		}
	}

}
