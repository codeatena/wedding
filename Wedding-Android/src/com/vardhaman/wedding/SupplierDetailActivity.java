package com.vardhaman.wedding;

import java.util.Locale;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.vardhaman.wedding.restinteraction.RESTInteraction;
import com.vardhaman.wedding.util.ApiUrl;
import com.vardhaman.wedding.util.Util;

public class SupplierDetailActivity extends Activity implements OnClickListener{
	ImageView backNavigation, previous, next, mainImage;
	String supplierId;
	RESTInteraction restInteraction;
	TextView suppName, suppAddress, suppPh, suppEmail, suppWeb, suppDesc, backNavigationSupplier_1;
	int counter = 0;
	ImageView sendMail, openWeb, makeCall, viewMap;
	String latitude, longitude, address;
	String url_add_to_favorite = "http://www.vardhamaninfotech.com/wedding/add_favorite.php?";
	CheckBox favorite;
	SharedPreferences sharedPreferences;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.supplier_detail_layout);
		init();
	}

	void init(){
		restInteraction = RESTInteraction.getInstance(this);
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		backNavigation = (ImageView)findViewById(R.id.backNavigationSupplier);
		backNavigation.setOnClickListener(this);
		suppName = (TextView)findViewById(R.id.suppName);
		suppAddress = (TextView)findViewById(R.id.suppAddress);
		suppPh = (TextView)findViewById(R.id.telNo);
		suppEmail = (TextView)findViewById(R.id.suppMail);
		suppWeb = (TextView)findViewById(R.id.suppWeb);
		suppDesc = (TextView)findViewById(R.id.desc);
		previous = (ImageView)findViewById(R.id.previousImage);
		next = (ImageView)findViewById(R.id.nextImage);
		backNavigationSupplier_1 = (TextView)findViewById(R.id.backNavigationSupplier_1);
		backNavigationSupplier_1.setOnClickListener(this);
		previous.setOnClickListener(this);
		next.setOnClickListener(this);
		sendMail = (ImageView)findViewById(R.id.sendMail);
		sendMail.setOnClickListener(this);
		openWeb = (ImageView)findViewById(R.id.openWeb);
		openWeb.setOnClickListener(this);
		makeCall = (ImageView)findViewById(R.id.makeCall);
		makeCall.setOnClickListener(this);
		viewMap = (ImageView)findViewById(R.id.viewMap);
		viewMap.setOnClickListener(this);
		mainImage = (ImageView)findViewById(R.id.productImage);
		mainImage.setOnClickListener(this);
		Intent intent = getIntent();
		supplierId = intent.getStringExtra("supplierId");
		favorite =(CheckBox)findViewById(R.id.cb_add_to_favorite);
		if (sharedPreferences.getBoolean(supplierId, false)) {
			favorite.setChecked(true);
		}
		favorite.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					sharedPreferences.edit().putBoolean(supplierId, true).commit();
					new AddToFavoriteAsync(supplierId, "favorite").execute();
				}	else {
					sharedPreferences.edit().putBoolean(supplierId, false).commit();
					new AddToFavoriteAsync(supplierId, "unfavorite").execute();
				}
			}
		});
	}

	@Override
	protected void onResume() {
		new SupplierDetailAsync().execute();
		super.onResume();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.backNavigationSupplier:
			finish();
			break;
		case R.id.backNavigationSupplier_1:
			finish();
			break;
		case R.id.nextImage:
			if (counter<(restInteraction.imagesArray.length-1)) {
				counter++;
				Util.loadImage(mainImage,
						restInteraction.imagesArray[counter],
						0);
			}
			if (counter>0) {
				previous.setVisibility(View.VISIBLE);
			}
			break;
		case R.id.previousImage:
			if (counter>0) {
				counter--;
			}
			if (counter==0) {
				previous.setVisibility(View.INVISIBLE);
			}
			Util.loadImage(mainImage,
					restInteraction.imagesArray[counter],
					0);
			break;
		case R.id.sendMail:
			Intent intent = new Intent(Intent.ACTION_SEND);
			intent.setType("text/html");
			intent.putExtra(Intent.EXTRA_EMAIL, new String[] {restInteraction.email});
			intent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
			intent.putExtra(Intent.EXTRA_TEXT, "I'm email body.");

			startActivity(Intent.createChooser(intent, "Send Email"));
			break;
		case R.id.openWeb:
			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(restInteraction.web));
			startActivity(browserIntent);

			break;
		case R.id.makeCall:
			Intent dial = new Intent();
			dial.setAction(Intent.ACTION_DIAL);
			dial.setData(Uri.parse("tel:" + restInteraction.phoneNo));
			startActivity(dial);

			break;
		case R.id.viewMap:
			System.out.println(latitude + "\n" + longitude + "===========================");
			//			String uri = String.format(Locale.ENGLISH, "geo:%f,%f", latitude, longitude);
			String geoCode = "geo:"+latitude+","+longitude+"?q=" + latitude + ","
					+ longitude + "(" + address + ")";
			Intent intent1 = new Intent(Intent.ACTION_VIEW, Uri.parse(geoCode));
			startActivity(intent1);	

			break;
		case R.id.productImage:
			Dialog dialog = new Dialog(SupplierDetailActivity.this, android.R.style.Theme_Holo_Light);
			dialog.setTitle("Zoom Image");
			dialog.setContentView(R.layout.zoom_image_layout);
			ImageView imageView = (ImageView)dialog.findViewById(R.id.zoomImage);
			Util.loadImage(imageView, restInteraction.imagesArray[counter], 0);
			dialog.show();
			break;

		default:
			break;
		}
	}

	public class SupplierDetailAsync extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(SupplierDetailActivity.this);
			progressDialog.setCancelable(false);
			progressDialog.setMessage("Please wait...");
			progressDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			return restInteraction.fetchProductList(ApiUrl.ProductDetailListURL + "id=" + supplierId);
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			progressDialog.dismiss();
			if (result.equals("Successful")) {
				suppName.setText(restInteraction.suppliername);
				suppAddress.setText(restInteraction.address);
				suppPh.setText("Tel: " + restInteraction.phoneNo);
				suppEmail.setText("Email: " + restInteraction.email);
				suppWeb.setText("Web: " + restInteraction.web);
				suppDesc.setText(restInteraction.description);
				if (restInteraction.imagesArray != null && restInteraction.imagesArray.length>0) {
					Util.loadImage(mainImage,restInteraction.imagesArray[counter], 0);
				}
				new GetLatLongAsync().execute();
			}else if (result.equals("Unsuccessful")) {
				Toast.makeText(SupplierDetailActivity.this, "Oops Sorry..!! No details at that time.", Toast.LENGTH_SHORT)
				.show();
			}
			else {
				Toast.makeText(SupplierDetailActivity.this, "Network Error", Toast.LENGTH_SHORT)
				.show();
			}
		}
	}

	public class GetLatLongAsync extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			return restInteraction.getLatLong(ApiUrl.getAddressUrl + "id=" + supplierId);
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (result.equals("Successful")) {
				latitude = restInteraction.lat;
				longitude = restInteraction.lng;
				address = restInteraction.place;

			}
			else {
//				Toast.makeText(SupplierDetailActivity.this, "Network Error", Toast.LENGTH_SHORT)
//				.show();
			}
		}
	}


	public class AddToFavoriteAsync extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;
		String supplierId, status;


		public AddToFavoriteAsync(String supplierId, String status) {
			this.supplierId = supplierId;
			this.status = status;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(SupplierDetailActivity.this);
			progressDialog.setCancelable(false);
			progressDialog.setMessage("Please wait...");
			progressDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			return restInteraction.addSuppliersToFavorite(url_add_to_favorite + "user_id=" + Util.loginData.getUserId()
					+ "&supplier_id=" + supplierId + "&status=" + status);
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			progressDialog.dismiss();
			if (result.equals("Successful")) {
				if (status.equalsIgnoreCase("favorite")) {
					Toast.makeText(SupplierDetailActivity.this, "Supplier added to Favorite List.", Toast.LENGTH_SHORT)
					.show();
				}else if(status.equalsIgnoreCase("unfavorite")) {
					Toast.makeText(SupplierDetailActivity.this, "Supplier removed from Favorite List.", Toast.LENGTH_SHORT)
					.show();
				}

			}
			else {
				Toast.makeText(SupplierDetailActivity.this, "Network Error", Toast.LENGTH_SHORT)
				.show();
			}
		}
	}
}
