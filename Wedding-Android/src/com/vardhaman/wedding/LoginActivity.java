package com.vardhaman.wedding;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.vardhaman.wedding.restinteraction.RESTInteraction;
import com.vardhaman.wedding.util.ApiUrl;

public class LoginActivity extends Activity implements OnClickListener{
	private RESTInteraction restInteraction;
	private Button btnSignUp;
	private Button btnLogin, btnFacebookLogin;
	EditText etLoginEmail, etLoginPwd;
	private Facebook facebook;
	SharedPreferences sharedPreferences;
	String userEmail, userPassword;
	CheckBox cbRememberLogin;
	boolean status = false;
	boolean checkboxStatus;
	TextView forgotPwd;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		init();
	}
	public void init()
	{
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		userEmail = sharedPreferences.getString("email", "");
		userPassword = sharedPreferences.getString("password", "");
		checkboxStatus = sharedPreferences.getBoolean("checkbox", false);
		restInteraction = RESTInteraction.getInstance(LoginActivity.this);
		facebook = new Facebook(getResources().getString(R.string.app_id));
		btnSignUp = (Button)findViewById(R.id.btnSignUp);
		btnLogin=(Button)findViewById(R.id.btnLogin);
		etLoginEmail = (EditText)findViewById(R.id.etLoginEmail);
		etLoginPwd = (EditText)findViewById(R.id.etLoginPwd);

		btnFacebookLogin = (Button)findViewById(R.id.btnFacebookLogin);
		btnFacebookLogin.setOnClickListener(this);
		btnLogin.setOnClickListener(this);
		btnSignUp.setOnClickListener(this);
		forgotPwd = (TextView)findViewById(R.id.forgotPwd);
		forgotPwd.setOnClickListener(this);
		cbRememberLogin = (CheckBox)findViewById(R.id.cbRememberLogin);
		cbRememberLogin.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					status = true;
					sharedPreferences.edit()
					.putBoolean("checkbox", status)
					.commit();
				}else {
					status = false;
					sharedPreferences.edit()
					.putBoolean("checkbox", status)
					.commit();
				}
			}
		});
		if (checkboxStatus) {
			etLoginEmail.setText(userEmail);
			etLoginPwd.setText(userPassword);
			cbRememberLogin.setChecked(true);
		}else {
			etLoginEmail.setText("");
			etLoginPwd.setText("");
			cbRememberLogin.setChecked(false);
		}
		
	}
	private boolean eTextValidation() {
		boolean isEmpty=false;
		if(!isEmailValid(etLoginEmail.getText().toString().trim())){
			isEmpty=true;
			etLoginEmail.setError("Enter valid email!");
		}
		if (etLoginPwd.getText().toString().trim().length()<1) {
			isEmpty=true;
			etLoginPwd.setError("Enter valid password!");
		}
		return isEmpty;
	}	
	boolean isEmailValid(CharSequence email) {
		return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
	}
	private class LoginAsync extends AsyncTask<String, Void, String> {
		ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(LoginActivity.this);
			progressDialog.setCancelable(false);
			progressDialog.setMessage("Please wait...");
			progressDialog.show();

		}

		@Override
		protected String doInBackground(String... params) {

			return restInteraction.sendLoginRequest(ApiUrl.loginURL
					+ "email=" + etLoginEmail.getText().toString().trim()
					+ "&password=" + etLoginPwd.getText().toString().trim());
		}

		@Override
		protected void onPostExecute(String result) {
			progressDialog.dismiss();
			if (result.equals("Successful")) {
				sharedPreferences.edit()
				.putString("email", etLoginEmail.getText().toString().trim())
				.putString("password", etLoginPwd.getText().toString().trim())
				.commit();
				Toast.makeText(LoginActivity.this, "Login Successfull", Toast.LENGTH_SHORT)
				.show();
				startActivity(new Intent(LoginActivity.this, Dashboard.class));
			}else if (result.equalsIgnoreCase("Unsuccessful")) {
				Toast.makeText(LoginActivity.this, "Please enter valid email or password.", Toast.LENGTH_SHORT)
				.show();
			} 
			else {
				Toast.makeText(LoginActivity.this, "Internet connection is slow. Please try again later.", Toast.LENGTH_SHORT).show();
			}
		}
	}
	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.btnSignUp:
			startActivity(new Intent(LoginActivity.this,SignUpActivity.class));

			break;
		case R.id.btnLogin:
			if(!eTextValidation()){
				new LoginAsync().execute(etLoginEmail.getText().toString(),etLoginPwd.getText().toString());
			}
			break;

		case R.id.btnFacebookLogin:
			loginToFb();
			break;
		case R.id.forgotPwd:
			final Dialog dialog = new Dialog(LoginActivity.this);
			dialog.setContentView(R.layout.forgot_pwd_dialog);
			dialog.setTitle("Forgot Password");
			final EditText enterMail;
			Button submit, cancel;
			enterMail = (EditText)dialog.findViewById(R.id.sendMail);
			submit = (Button)dialog.findViewById(R.id.btnSendMail);
			cancel = (Button)dialog.findViewById(R.id.btnCancel);
			submit.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					String getEmail = enterMail.getText().toString();
					
				
						if (getEmail.length()!=0 ){
							if (getEmail.matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+"))
							{
								new ForgotPasswordAsync(getEmail).execute();
								dialog.dismiss();
							}
						}else {
							enterMail.setError("Please enter valid email address.");
						}

					}

			});

			cancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					dialog.dismiss();
				}
			});

			dialog.show();

			break;
		default:
		}

	}

	private class GetfbInfoAsync extends AsyncTask<String, Void, String> 
	{		
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(LoginActivity.this);
			progressDialog.setCancelable(false);
			progressDialog.setMessage("Please wait..");
			progressDialog.show();
		}

		@Override
		protected String doInBackground(String... params) {
			String jsonUser = null;
			try {
				jsonUser = facebook.request("me");
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return jsonUser;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			progressDialog.dismiss();
			if(result != null){
				JSONObject obj;
				try {
					obj = new JSONObject(result);
					String userId = obj.getString("id");
					String fbFirstName = obj.getString("first_name");
					String fbLastName = obj.getString("last_name");
					String email = obj.getString("email");
					Intent intent = new Intent(LoginActivity.this, Dashboard.class);
					intent.putExtra("firstName", fbFirstName);
					intent.putExtra("lastName", fbLastName);
					intent.putExtra("email", email);
					intent.putExtra("userId", email);
					startActivity(intent);
				} catch (JSONException e) {
					e.printStackTrace();
					Toast.makeText(LoginActivity.this, "Error in login with facebook", Toast.LENGTH_SHORT).show();
				}				
			} else {
				Toast.makeText(LoginActivity.this, "Error in login with facebook", Toast.LENGTH_SHORT).show();
			}			
		}
	}

	private void loginToFb(){
		facebook.authorize(LoginActivity.this, new String[]{"email"},Facebook.FORCE_DIALOG_AUTH, new DialogListener() {

			@Override
			public void onFacebookError(FacebookError e) {
				e.printStackTrace();
			}

			@Override
			public void onError(DialogError e) {
				e.printStackTrace();
			}

			@Override
			public void onComplete(Bundle values) {
				String fbToken = values.getString("access_token");
				System.out.println(values);		
				new GetfbInfoAsync().execute();

			}
			@Override
			public void onCancel() {						
			}
		});
	}
	
	class ForgotPasswordAsync extends AsyncTask<String, String, String> {
		JSONArray jsonArray;
		ProgressDialog pDialog;
		String getEmail;
		public ForgotPasswordAsync(String getEmail) {
			this.getEmail = getEmail;
		}
		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(LoginActivity.this);
			pDialog.setMessage("Loading. Please wait...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(false);
						pDialog.show();
		}
		/**
		 * getting All products from url
		 * */
		protected String doInBackground(String... args) {
			return restInteraction.forgetPassword(ApiUrl.fogotPassword + "email="+ URLEncoder.encode(getEmail));
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String result) {
			pDialog.dismiss();
			if (result.equals("Successful")) {

				Toast.makeText(LoginActivity.this, restInteraction.message, Toast.LENGTH_SHORT).show();
			}else {
				Toast.makeText(LoginActivity.this, "Please try again", Toast.LENGTH_SHORT).show();
			}
		}
	}


}
