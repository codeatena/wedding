package com.vardhaman.wedding;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.vardhaman.wedding.adapter.ImageCustomAdapter;
import com.vardhaman.wedding.fragment.CategoryListFragment;
import com.vardhaman.wedding.restinteraction.RESTInteraction;
import com.vardhaman.wedding.util.ApiUrl;
import com.vardhaman.wedding.util.Util;

public class SupplierActivity extends Activity {
	GridView grid;
	public RESTInteraction restInteraction;
	String[] txtId = { "Google", "Github", "Instagram", "Facebook", "Flickr",
			"Pinterest", "Quora", "Twitter", "Vimeo", "WordPress", "Youtube",
			"Stumbleupon", "SoundCloud", "Reddit", "Blogger" };
	int[] imageId = { R.drawable.ic_launcher, R.drawable.ic_launcher,
			R.drawable.ic_launcher, R.drawable.ic_launcher,
			R.drawable.ic_launcher, R.drawable.ic_launcher,
			R.drawable.ic_launcher, R.drawable.ic_launcher,
			R.drawable.ic_launcher, R.drawable.ic_launcher,
			R.drawable.ic_launcher, R.drawable.ic_launcher,
			R.drawable.ic_launcher, R.drawable.ic_launcher,
			R.drawable.ic_launcher };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_suppliers);
		initAdapter();
		restInteraction=RESTInteraction.getInstance(this);
	}

	public void initAdapter() {
		
		grid = (GridView) findViewById(R.id.gridViewSupImages);
		
		grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Toast.makeText(SupplierActivity.this,
						"You Clicked at " + txtId[position], Toast.LENGTH_SHORT)
						.show();
				startActivity(new Intent(SupplierActivity.this, CategoryListFragment.class).putExtra("categoryId", Util.getCategoryDetail.get(position).getId()));
			}
		});

	}
	
	@Override
	protected void onResume() {
		new SupplierAsync().execute();
		super.onResume();
	}
	private class SupplierAsync extends AsyncTask<String, Void, String>
	{
		private ProgressDialog progressDialog;
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog=new ProgressDialog(SupplierActivity.this);
			progressDialog.setCancelable(false);
			progressDialog.setMessage("Please wait...");
			progressDialog.show();
		}
		@SuppressWarnings("deprecation")
		@Override
		protected String doInBackground(String... params) {
		
			return restInteraction.getSupplierList(ApiUrl.SupplierListURL
				
					);
		}
		@Override
		protected void onPostExecute(String result) {
			
			super.onPostExecute(result);
			progressDialog.dismiss();
			if(result!=null)
			{
				if(result=="Successful")
				{
					grid.setAdapter(new ImageCustomAdapter(SupplierActivity.this, R.layout.grid_single, Util.getCategoryList));
					
					//startActivity(new Intent(SignUpActivity.this,LoginActivity.class));
				}
				else {
					Toast.makeText(SupplierActivity.this, "Internet connection is slow. Please try again later.",Toast.LENGTH_SHORT).show();
				}
			}
			else {
				Toast.makeText(SupplierActivity.this, "Network Error", Toast.LENGTH_SHORT).show();
			}
		}
	
	}


}
