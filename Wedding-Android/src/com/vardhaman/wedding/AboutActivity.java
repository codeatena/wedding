package com.vardhaman.wedding;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class AboutActivity extends Activity{
	ImageView shareFriends_about;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
		shareFriends_about = (ImageView)findViewById(R.id.shareFriends_about);
		shareFriends_about.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Intent sharingIntent = new Intent(Intent.ACTION_SEND);
				sharingIntent.setType("text/plain");
				sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT,
						"Hey, I found this new app. Check this.");
				sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
						"Check this new app.");
				startActivity(Intent.createChooser(sharingIntent, "Share using"));
			}
		});
	}

}
