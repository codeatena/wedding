package com.vardhaman.wedding.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.TimeZone;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;
import android.util.Log;

public class MySQLiteHelper extends SQLiteOpenHelper {
	public static String DB_NAME = "wedding.db";
	private final Context myContext;
	public SQLiteDatabase myDataBase;
	public static String DB_PATH = "/data/data/";
	public static String userId;
	private final static int DB_VERSION = 1;
	private boolean isDBUpgraded = false;
	static SharedPreferences sharedPrefs;
	private static MySQLiteHelper instanceDBHelper;
	public  final static String KEY_ID = "id";
	public  final static String KEY_USERID = "user_id";
	public  final static String KEY_GUEST_ID = "guest_id";
	public final static String KEY_GUEST_FIRSTNAME = "guest_firstname";
	public  final static String KEY_GUEST_LASTNAME = "guest_lastname";
	public  final static String KEY_GUEST_ADDRESS = "guest_address";
	public  final static String KEY_GUEST_RELATION = "guest__relation";
	public  final static String KEY_GUEST_CONTACT = "guest__contact";
	public  final static String KEY_GUEST_EMAIL = "guest_email";
	public  final static String KEY_GUEST_PROFILE_PIC = "guest_pic";
	public  final static String KEY_FAVOURITE = "favourite";
	public final static String KEY_STATUS = "status";
	
	// Table name

	public static final String TABLE_GUEST = "guest";
	// Constructor Method
	public MySQLiteHelper( Context context )
	{
		super(context, DB_NAME, null, DB_VERSION);
		this.myContext = context;
		DB_PATH = "/data/data/"+myContext.getPackageName()+"/databases/";
		Log.d("MySQLiteHelper", "DB_PATH"+DB_PATH);
		sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		//			try {
		//				createDataBase();
		//			} catch (IOException e) {
		//				// TODO Auto-generated catch block
		//				e.printStackTrace();
		//			}
	}

	// Singleton Method.
	public static MySQLiteHelper getInstance(Context context)
	{


		if (instanceDBHelper == null)
		{
			instanceDBHelper = new MySQLiteHelper(context);

		}
		return instanceDBHelper;
	}
	public SQLiteDatabase getDatabaseInstance(){
		return myDataBase;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_GUEST= "CREATE TABLE " +TABLE_GUEST + "("
				+ KEY_ID + " INTEGER PRIMARY KEY,"
				+ KEY_USERID + " TEXT,"
				+ KEY_GUEST_ID + " TEXT,"
				+ KEY_GUEST_FIRSTNAME + " TEXT, "
				+ KEY_GUEST_LASTNAME + " TEXT, "
				+ KEY_GUEST_ADDRESS + " TEXT, "
				+ KEY_GUEST_RELATION + " TEXT, "
				+ KEY_GUEST_CONTACT + " TEXT,"
				+ KEY_GUEST_EMAIL + " TEXT,"
				+ KEY_GUEST_PROFILE_PIC + " TEXT,"
				+ KEY_FAVOURITE + " TEXT,"
				+ KEY_STATUS + " TEXT" +")";
		db.execSQL(CREATE_GUEST);
	}

	public long addAllGuestToDB(String userId, ContentValues values) {
		SQLiteDatabase database = this.getWritableDatabase();
		Cursor cursor = database.query(TABLE_GUEST,
				new String[] { KEY_GUEST_ID },
				KEY_GUEST_ID + " =? ", new String[] { userId }, null, null, null);
		if (cursor.getCount() > 0 && cursor.moveToFirst()) {
			return database.update(TABLE_GUEST, values,
					KEY_GUEST_ID + " =? ", new String[] { userId });
		} else {
			// If not found then create new one//
			return database.insert(TABLE_GUEST, null, values);
		}
	}
	
	public Cursor getAllGuest(){
		userId = Util.loginData.getUserId();
		SQLiteDatabase database = this.getWritableDatabase();
		return database.query(TABLE_GUEST, null, KEY_USERID + " =? ", new String[] { userId }, null, null, null);
	}
	
	public Cursor getArrivedGuest(){
		userId = Util.loginData.getUserId();
		SQLiteDatabase database = this.getWritableDatabase();
		return database.query(TABLE_GUEST, null, KEY_USERID + " =? " + " AND " + KEY_STATUS + " =?", new String[] { userId, "true" }, null, null, null);
	}
	
	public void createDataBase() throws IOException
	{
		Boolean dbExists = databaseExist();
		if (!dbExists)
		{
			try
			{
				copyDataBase();
			}
			catch (IOException e)
			{
				throw new Error("Error copying database: " + e.getMessage());
			}
		}
	}


	// check if database exists
	public boolean databaseExist()
	{
		File dbFile = new File(DB_PATH + DB_NAME);
		return dbFile.exists();
	}

	// Copy database from assets
	public void copyDataBase() throws IOException
	{
		Log.d("MySQLiteHelper", "copyDatabase Called");
		try
		{
			InputStream myInput = this.myContext.getAssets().open(DB_NAME);
			String outFileName = DB_PATH + DB_NAME;
			OutputStream myOutput = new FileOutputStream(outFileName);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = myInput.read(buffer)) > 0)
			{
				myOutput.write(buffer, 0, length);
			}

			myOutput.flush();
			myOutput.close();
			myInput.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	// open database for querying
	public synchronized void openDataBase() throws SQLException
	{
		try
		{
			String myPath = DB_PATH + DB_NAME;
			if (myDataBase == null || !myDataBase.isOpen())
			{
				myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

				Log.d("MYSQLiteHelper", "DBVersion = "+myDataBase.getVersion());
				if(isDBUpgraded){
					myDataBase.setVersion(DB_VERSION);
				}
				if(myDataBase.getVersion()<DB_VERSION){
					onUpgrade(myDataBase, myDataBase.getVersion(), DB_VERSION);
					//Reopen
					myDataBase.close();
					myDataBase = null;
					openDataBase();
				}							
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {		  
		Log.w(MySQLiteHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		//deleting old database
		//TODO
		File dbFile = new File(DB_PATH + DB_NAME);
		if(dbFile.exists()){
			isDBUpgraded = dbFile.delete();
			Log.d("MySQLiteHelper", "deletion of old DB file, result = "+isDBUpgraded);	    	
		}
		//creating new one
		//	    try {
		//			createDataBase();
		//		} catch (IOException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}
	}

}
