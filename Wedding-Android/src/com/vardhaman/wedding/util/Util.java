package com.vardhaman.wedding.util;

import java.util.ArrayList;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.koushikdutta.urlimageviewhelper.UrlImageViewCallback;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.vardhaman.wedding.model.CategoryModel;
import com.vardhaman.wedding.model.FavModel;
import com.vardhaman.wedding.model.GiftListModel;
import com.vardhaman.wedding.model.GuestList;
import com.vardhaman.wedding.model.LoginData;
import com.vardhaman.wedding.model.OffersModel;
import com.vardhaman.wedding.model.Shows;
import com.vardhaman.wedding.model.SupplierModel;

public class Util {
	public static LoginData loginData = new LoginData();
	public static ArrayList<GuestList> guestArrayList = new ArrayList<GuestList>();
	public static ArrayList<SupplierModel> getCategoryList = new ArrayList<SupplierModel>();
	public static ArrayList<CategoryModel> getCategoryDetail=new  ArrayList<CategoryModel>();
	public static ArrayList<GiftListModel> getGiftDetail=new  ArrayList<GiftListModel>();
	public static ArrayList<OffersModel> getOfferDetail=new  ArrayList<OffersModel>();
	public static ArrayList<Shows> getShows=new  ArrayList<Shows>();
	public static ArrayList<FavModel> getFavouriteList=new  ArrayList<FavModel>();

//	
	 public static void loadImage(ImageView imgView,String url,int defaultResource){
	  UrlImageViewHelper.setUrlDrawable(imgView,url,defaultResource, new UrlImageViewCallback() {
	            @Override
	            public void onLoaded(ImageView imageView, Bitmap loadedBitmap, String url, boolean loadedFromCache) {
	                if (!loadedFromCache) {
	                }
	            }
	        });
	 }
	
}
