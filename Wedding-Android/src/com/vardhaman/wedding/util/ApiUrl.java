package com.vardhaman.wedding.util;

public class ApiUrl{
	public static final String baseURL = "http://theweddingguide.co.uk/wedding-app-admin/";
	public static final String registraionURL = baseURL + "create_user.php?";
	public static final String loginURL = baseURL + "login.php?";
	public static final String addGuestURL = baseURL + "guest_user.php?";
	public static final String imageDownloadURL = baseURL + "files/";
	public static final String fetchGuestURL = baseURL + "get_guest_user.php?";
	public static final String checkStatusURL = baseURL + "update_guest_user.php?";
	public static final String weddingTimeTableURL = baseURL + "wedding.php?";
	public static final String SupplierListURL = baseURL + "get_suppler_category.php?";
	public static final String SupplierDetailListURL = baseURL + "get_suppler.php?";
	public static final String ProductDetailListURL = baseURL + "single_suplier.php?";
	public static final String AddGiftUrl = baseURL + "add_grift_user.php?";
	public static final String GetGiftsUrl = baseURL + "get_gift.php?";
	public static final String GetOfferssUrl = baseURL + "get_inventory.php?";
	public static final String fogotPassword = baseURL + "forgetpassword.php?";
	public static final String getAddressUrl = baseURL + "get_address.php?";
	public static final String addWeddingShows = baseURL + "add_shows.php?";
	public static final String deleteGiftUrl = baseURL + "delete_gift.php?";
	public static final String getShowsUrl = baseURL + "get_shows.php?";
	public static final String getAllGuestUrl = baseURL + "get_guest.php?";
	public static final String getFavouritesUrl = baseURL + "get_user_favorite.php?";
}
