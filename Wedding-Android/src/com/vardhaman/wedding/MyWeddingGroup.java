package com.vardhaman.wedding;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Window;

import com.vardhaman.wedding.fragment.AddGiftFragment;
import com.vardhaman.wedding.fragment.AddGuestFragment;
import com.vardhaman.wedding.fragment.DayAndNotesFragment;
import com.vardhaman.wedding.fragment.FavouriteGuestFragment;
import com.vardhaman.wedding.fragment.GiftListFragment;
import com.vardhaman.wedding.fragment.GuestListFragment;
import com.vardhaman.wedding.fragment.MyWeddingFragment;
import com.vardhaman.wedding.fragment.TimeTableDetailsFragment;
import com.vardhaman.wedding.fragment.WeddingBudgetFragment;
import com.vardhaman.wedding.fragment.WeddingTimeTableFragment;


public class MyWeddingGroup extends FragmentActivity{
	FragmentManager fManager;
	public MyWeddingFragment myWeddingFragment;
	public GiftListFragment giftListFragment;
	public GuestListFragment guestListFragment;
	public AddGuestFragment addGuestFragment;
	public WeddingTimeTableFragment weddingTimeTableFragment;
	public DayAndNotesFragment dayAndNotesFragment;
	public FavouriteGuestFragment favGuestFragment;
	public TimeTableDetailsFragment tableDetailsFragment;
	public WeddingBudgetFragment weddingBudgetFragment;
	public AddGiftFragment addGiftFragment;
	public static int lVPosition;
	@SuppressLint("NewApi") @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.follow_group);
		
		//		actionBar.setCustomView(R.layout.custom_action_bar);
		lVPosition = 0;
		fManager = getSupportFragmentManager();

		myWeddingFragment = new MyWeddingFragment();
		Bundle bundle = new Bundle();
		//actionBar = getActionBar();

		bundle.putInt("page", 1);
		myWeddingFragment.setArguments(bundle);
		loadFragment(R.id.fmLayout, myWeddingFragment);

	}
	
	public void loadFragment(int resourceId,Fragment fragment){
		FragmentTransaction ft = fManager.beginTransaction();
		ft.setTransition(android.R.anim.bounce_interpolator);
		ft.add(resourceId, fragment);
		ft.commit();		
	}

	public void unLoadFragment1(){
		fManager.beginTransaction().hide(myWeddingFragment).commit();
		fManager.beginTransaction().remove(myWeddingFragment).commit();
	}
	public void unLoadFragment2(){
		fManager.beginTransaction().hide(giftListFragment).commit();
		fManager.beginTransaction().remove(giftListFragment).commit();
	}
	public void unLoadFragment3(){
		fManager.beginTransaction().hide(guestListFragment).commit();
		fManager.beginTransaction().remove(guestListFragment).commit();
	}
	public void unLoadFragment4(){
		fManager.beginTransaction().hide(addGuestFragment).commit();
		fManager.beginTransaction().remove(addGuestFragment).commit();
	}
	public void unLoadFragment5(){
		fManager.beginTransaction().hide(weddingTimeTableFragment).commit();
		fManager.beginTransaction().remove(weddingTimeTableFragment).commit();
	}
	public void unLoadFragment6(){
		fManager.beginTransaction().hide(dayAndNotesFragment).commit();
		fManager.beginTransaction().remove(dayAndNotesFragment).commit();
	}
	public void unLoadFragment7(){
		fManager.beginTransaction().hide(favGuestFragment).commit();
		fManager.beginTransaction().remove(favGuestFragment).commit();
	}
	public void unLoadFragment8(){
		fManager.beginTransaction().hide(tableDetailsFragment).commit();
		fManager.beginTransaction().remove(tableDetailsFragment).commit();
	}
	public void unLoadFragment9(){
		fManager.beginTransaction().hide(weddingBudgetFragment).commit();
		fManager.beginTransaction().remove(weddingBudgetFragment).commit();
	}
	public void unLoadFragment10(){
		fManager.beginTransaction().hide(addGiftFragment).commit();
		fManager.beginTransaction().remove(addGiftFragment).commit();
	}


	@Override
	public void onBackPressed() {
		super.onBackPressed();
		this.getParent().onBackPressed();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		//		addGuestFragment.onActivityResult(requestCode, resultCode, data);
		super.onActivityResult(requestCode, resultCode, data);
	}


}