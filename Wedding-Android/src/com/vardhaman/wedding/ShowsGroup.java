package com.vardhaman.wedding;

import com.vardhaman.wedding.fragment.ShowsDetailFragment;
import com.vardhaman.wedding.fragment.WeddingShowsFragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;


public class ShowsGroup extends FragmentActivity{
	public static int lVPosition;
	FragmentManager fManager;
	public WeddingShowsFragment weddingShowsFragment;
	public ShowsDetailFragment showsDetailFragment;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.follow_group);
		lVPosition = 0;
		fManager = getSupportFragmentManager();
		weddingShowsFragment = new WeddingShowsFragment();
		loadFragment(R.id.fmLayout, weddingShowsFragment);
	}
	
	public void loadFragment(int resourceId,Fragment fragment){
		FragmentTransaction ft = fManager.beginTransaction();
		ft.setTransition(android.R.anim.bounce_interpolator);
		ft.add(resourceId, fragment);
		ft.commit();		
	}
	public void unLoadFragment1(){
		fManager.beginTransaction().remove(weddingShowsFragment).commit();
	}
	public void unLoadFragment2(){
		fManager.beginTransaction().remove(showsDetailFragment).commit();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		this.getParent().onBackPressed();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}

}
