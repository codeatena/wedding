package com.vardhaman.wedding.model;

import android.util.Log;

public class SupplierModel {
	String id;
	String categoryName;
	String categoryImage;
	
	public SupplierModel(String id, String categoryName, String categoryImage) {
		super();
		this.id = id;
		this.categoryName = categoryName;
		this.categoryImage = categoryImage;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getCategoryImage() {
		
//		String path = categoryImage.substring(0, 54);
		String filename = categoryImage.substring(54);
		return "http://theweddingguide.co.uk/wedding-app-admin/admin/upload/" + filename;
	}
	public void setCategoryImage(String categoryImage) {
		this.categoryImage = categoryImage;
	}
	

}
