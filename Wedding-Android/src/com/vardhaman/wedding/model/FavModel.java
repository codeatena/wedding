package com.vardhaman.wedding.model;

public class FavModel {
	String id;
	String supplierName; 
	String supplierCategoryId;
	String supplierImage;
	
	public FavModel(String id, String supplierName, String supplierCategoryId,
			String supplierImage) {
		super();
		this.id = id;
		this.supplierName = supplierName;
		this.supplierCategoryId = supplierCategoryId;
		this.supplierImage = supplierImage;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public String getSupplierCategoryId() {
		return supplierCategoryId;
	}
	public void setSupplierCategoryId(String supplierCategoryId) {
		this.supplierCategoryId = supplierCategoryId;
	}
	public String getSupplierImage() {
		return supplierImage;
	}
	public void setSupplierImage(String supplierImage) {
		this.supplierImage = supplierImage;
	}
	
}
