package com.vardhaman.wedding.model;

public class GiftListModel {
	String giftId;
	String id;
	String firstName;
	String lastName;
	String gift;
	String relation;
	String profilePic;
	String favourite;
	public GiftListModel(String giftId, String id, String firstName, String lastName,
			String gift, String relation, String profilePic, String favourite) {
		super();
		this.giftId = giftId;
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gift = gift;
		this.relation = relation;
		this.profilePic = profilePic;
		this.favourite = favourite;
	}
	public String getgiftId() {
		return giftId;
	}
	public void setgiftId(String giftId) {
		this.giftId = giftId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getGift() {
		return gift;
	}
	public void setGift(String gift) {
		this.gift = gift;
	}
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	public String getProfilePic() {
		return profilePic;
	}
	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}
	public String getFavourite() {
		return favourite;
	}
	public void setFavourite(String favourite) {
		this.favourite = favourite;
	}


}
