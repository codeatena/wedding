package com.vardhaman.wedding.model;

public class Shows {
	String id;
	String showName;
	String showAddress;
	String showTime;
	String showContact;
	String showDate;
	String showImage;
	public Shows(String id, String showName, String showAddress,
			String showTime, String showContact, String showDate,
			String showImage) {
		super();
		this.id = id;
		this.showName = showName;
		this.showAddress = showAddress;
		this.showTime = showTime;
		this.showContact = showContact;
		this.showDate = showDate;
		this.showImage = showImage;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getShowName() {
		return showName;
	}
	public void setShowName(String showName) {
		this.showName = showName;
	}
	public String getShowAddress() {
		return showAddress;
	}
	public void setShowAddress(String showAddress) {
		this.showAddress = showAddress;
	}
	public String getShowTime() {
		return showTime;
	}
	public void setShowTime(String showTime) {
		this.showTime = showTime;
	}
	public String getShowContact() {
		return showContact;
	}
	public void setShowContact(String showContact) {
		this.showContact = showContact;
	}
	public String getShowDate() {
		return showDate;
	}
	public void setShowDate(String showDate) {
		this.showDate = showDate;
	}
	public String getShowImage() {
		return showImage;
	}
	public void setShowImage(String showImage) {
		this.showImage = showImage;
	}
	
}
