package com.vardhaman.wedding.model;

public class GuestList {
	
	 private String id;
	 private String fistname;
	 private String lastname;
	 private String relation;
	 private String contact_no;
	 private String email;
	 private String address;
	 private String profile_pic;
	 private String favouite;
	 private String status;
	
	 
	public GuestList(String id, String fistname, String lastname,
			String relation, String contact_no, String email, String address,
			String profile_pic, String favouite, String status) {
		this.id = id;
		this.fistname = fistname;
		this.lastname = lastname;
		this.relation = relation;
		this.contact_no = contact_no;
		this.email = email;
		this.address = address;
		this.profile_pic = profile_pic;
		this.favouite = favouite;
		this.status = status;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getFistname() {
		return fistname;
	}
	public void setFistname(String fistname) {
		this.fistname = fistname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	public String getContact_no() {
		return contact_no;
	}
	public void setContact_no(String contact_no) {
		this.contact_no = contact_no;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getProfile_pic() {
		return profile_pic;
	}
	public void setProfile_pic(String profile_pic) {
		this.profile_pic = profile_pic;
	}
	public String getFavouite() {
		return favouite;
	}
	public void setFavouite(String favouite) {
		this.favouite = favouite;
	}


}

