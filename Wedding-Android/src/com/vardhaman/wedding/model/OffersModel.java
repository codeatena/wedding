package com.vardhaman.wedding.model;

public class OffersModel {
	String id;
	String name;
	String supplierId;
	String image;
	public OffersModel(String id, String name, String supplierId, String image) {
		super();
		this.id = id;
		this.name = name;
		this.supplierId = supplierId;
		this.image = image;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	
	

}
