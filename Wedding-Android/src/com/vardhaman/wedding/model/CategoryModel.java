package com.vardhaman.wedding.model;

public class CategoryModel {
	String id;
	String supplerName;
	String image;
	String city;
	int offers;
	String supplierId;
	public CategoryModel(String id, String supplerName,
			String image, String city, int offers, String supplierId) {
		super();
		this.id = id;
		this.supplerName = supplerName;
		this.image = image;
		this.city = city;
		this.offers = offers;
		this.supplierId = supplierId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSupplerName() {
		return supplerName;
	}
	public void setSupplerName(String supplerName) {
		this.supplerName = supplerName;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public int getOffers() {
		return offers;
	}
	public void setOffers(int offers) {
		this.offers = offers;
	}
	public String getsupplierId() {
		return supplierId;
	}
	public void setsupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

}
